angular.module('spa-SE')
 .controller('homeCtrl', function ($scope) {
    $scope.message = "Home";
});
 
angular.module('spa-SE')
 .controller('personaCtrl', function ($scope) {
    $scope.message = "Registro Persona";
});
 
angular.module('spa-SE')
 .controller('contactoCtrl', function ($scope) {
    $scope.message = "Contacto";
});

angular.module('spa-SE')
 .controller('formCtrl', function ($scope) {
    $scope.formData = {};
    $scope.formBasico = {};
    $scope.formNivel = {};
    $scope.formCurso = {};
    $scope.formEmpresa = {};
    $scope.formReferido = {};
    $scope.formPreferido = {};
    $scope.formAptitud = {};

    $scope.dynamicPopover = {
	    content: 'Hola Prueba',
	    title: 'Titulo'
	};

    $scope.patternTexto=/^[a-zA-Z]*$/;
    $scope.patternAlpha=/^[a-zA-Z0-9]*$/;
    $scope.patternFecha=/^(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d$/;
    $scope.patternTelefono=/^\+?\d{3}?[-]?\(?(?:\d{3})\)?[\s]?\d\d\d\d$/;

    $scope.mostrarNombre = false;
    $scope.mostrarDatos = false;
    $scope.mostrarEstudios = false;
    $scope.mostrarOtros = false;
    $scope.mostrarLaboral = false;
    $scope.mostrarSkills = false;
    $scope.mostrarPreferencias = false;

    $scope.submitForm = function (formData) {
	    $scope.mostrarNombre = true;
	    $scope.mostrarDatos = true;
	};

    $scope.submitFormBasico = function(formBasico){
        $scope.mostrarEstudios = true;
        $scope.mostrarOtros = true;
        $scope.mostrarLaboral = true;
        $scope.mostrarSkills = true;
        $scope.mostrarPreferencias = true;
    }
});