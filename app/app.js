'use strict';

var app = angular.module('zonatrabajoApp', ['ngRoute', 'ngSanitize', 'ui.select', 'angular-icheck', 'Alertify', 'ngFileUpload', 'perfect_scrollbar', 'ui.bootstrap', 'angularMoment', 'angucomplete-alt', 'duScroll'])
                 .config(function ($routeProvider, $locationProvider) {
                  $routeProvider
                    .when('/', {
                      templateUrl: 'app/views/home.html'
                    })
                    .when('/empleos', {
                      templateUrl: 'app/views/jobs.html'
                    })
                    .when('/empleos/:vacante/:id', {
                      templateUrl: 'app/views/job-detail.html'
                    })
                    .when('/planes', {
                      templateUrl: 'app/views/plans.html'
                    })
                    .when('/contacto', {
                      templateUrl: 'app/views/contact.html'
                    })
                    .otherwise({
                      redirectTo: '/'
                    });

                    // use the HTML5 History API
                    $locationProvider.html5Mode(true);
                });