'use strict';

  app.controller('jobsController', ['$window', '$scope', '$http', '$location', '$anchorScroll', '$document', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper',
                          function($window, $scope, $http, $location, $anchorScroll, $document, Session, commons, Alertify, Upload, $timeout, Redir, Person, Company, Activities, Offer, Helper) {

    var job = $scope;
    job.itemsPerPage = 10;
 	  job.currentPage = 0;
 	  job.ofertas = [];
 	  job.offerCustomFilter = {};
    job.user_data = JSON.parse(localStorage.getItem("user_data"));
    job.is_logged = false;
    job.searchTerm = ''
    job.showDropdown = true
    job.mobile_filters = ($window.innerWidth > 700) ? 'collapse in' : 'collapse';

    job.cargaInicial = function(){

        if (Session.check(job.user_data, 'person')){
            job.is_logged = true;            

            Person.profile(job.user_data._id).then(function (response) {
                 
                job.person = response.data.person;
                job.user = response.data.user;
                job.member_since = new Date(response.data.user.member_since.date);

            });
        }

        
        Offer.listAll().then(function (response) {
          
          job.ofertas = response.data.offers;

        });

        Offer.filterBy("cities").then(function (response) {
          
          job.offerCities = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("state").then(function (response) {
          
          job.offerStates = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("country").then(function (response) {
          
          job.offerCountry = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("education").then(function (response) {
          
          job.offerLevels = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("contract").then(function (response) {
          
          job.offerContratcs = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("schedule").then(function (response) {
          
          job.offerSchedules = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("area").then(function (response) {
          
          job.offerAreas = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("currency").then(function (response) {
          
          job.offerCurrencies = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

    }

    job.goTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    };    

    job.offerSelected= function(selected) {
      if (selected) {
        job.goToUrl(selected.originalObject.title, selected.originalObject._id);
      }
    };

    job.$watch('$viewContentLoaded', function ($evt, data) {
             
        Offer.countAll()
             .then(function (response) {
          
          job.num_offers = response.data.num_offers;
          job.title = 'Vacantes de empleo disponibles';

        });

        Offer.mostrecent()
             .then(function (response) {
          
          job.ofertas = response.data.offers;

        });
    });    

    job.removeFilter = function(filter){
      delete job.offerCustomFilter[filter];
      job.select_area = ''
    }

    job.areasFilter = function(){
      job.addFilter('area', job.select_area._id)
      $document.scrollToElement(angular.element(document.getElementById('content')), 100, 2000);
    }

    job.addFilter = function(index, term){
    	job.offerCustomFilter[index] = term;
    }

	  job.goToUrl = function (job, id) {

			var tmp_this = job.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for (var i = 0; i < arr_busca.length; i++) {
                tmp_this = tmp_this.replace(arr_busca[i], arr_reemplaza[i]);
            }
            job = tmp_this.replace(/[^\\s\w]/g, "-");

            window.location = "/empleos"+ "/" + job + "/" + id;
    }

    job.range = function() {
      var rangeSize = 4;
      var ps = [];
      var start = job.currentPage;
      if (start > job.pageCount()-rangeSize ) {
        start = job.pageCount()-rangeSize+1;
      }
   
     for (var i=start; i<start+rangeSize; i++) {
        if(i>=0) 
          ps.push(i);
      }
      return ps;
    };
   
    job.prevPage = function() {
      if (job.currentPage > 0) {
        job.currentPage--;
      }
    };
   
    job.DisablePrevPage = function() {
      return job.currentPage === 0 ? "disabled" : "";
    };
   
    job.pageCount = function() {
      return Math.ceil(job.ofertas.length/job.itemsPerPage)-1;
    };
   
    job.nextPage = function() {
      if (job.currentPage < job.pageCount()) {
        job.currentPage++;
      }
    };
   
    job.DisableNextPage = function() {
      return job.currentPage === job.pageCount() ? "disabled" : "";
    };
   
    job.setPage = function(n) {
      job.currentPage = n;
    };    

    job.viewDetail = function(item, itemName, url){

      RedirService.detail(item, itemName, url);

    }
    
}]);