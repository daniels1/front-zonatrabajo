'use strict';

app.controller('jobsDetailController', ['$scope', '$http', 'Session', 'commons', 'Alertify', '$timeout', 'Redir', '$routeParams', 'Person', 'Company', 'Activities', 'Offer', 'Helper',
                                function($scope, $http, Session, commons, Alertify, $timeout, Redir, $routeParams, Person, Company, Activities, Offer, Helper) {

    var jdc = $scope;
    jdc.user_data = JSON.parse(localStorage.getItem("user_data"));
    jdc.postulateSuccess = false;
    jdc.postulateDuplicate = false;
    jdc.offerURL = 'https://zonatrabajo.com/empleos/'+$routeParams.vacante+'/'+$routeParams.id;

    jdc.cargaInicial = function(){
        
        jdc.offer_id = ($routeParams.id) ? $routeParams.id : ''

        Offer.show(jdc.offer_id).then(function(response) {
            
            jdc.oferta = response.data.offer;
                
        });

    }    

    jdc.viewDetail = function(item, itemName, url){

      if (Session.check(jdc.user_data, 'person')){
        Redir.detail(item, itemName, url);
      }else{
        Redir.detail(item, itemName, 'usuarios/entrar');
      }

    }
    
}]);