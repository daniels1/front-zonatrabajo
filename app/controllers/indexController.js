'use strict';

app.controller('indexController', ['$scope', '$http', '$location', '$anchorScroll', 'Session', 'commons', 'Alertify', '$timeout', 'Redir', '$routeParams', 'Person', 'Company', 'Activities', 'Offer', 'Helper',
                                function($scope, $http, $location, $anchorScroll, Session, commons, Alertify, $timeout, Redir, $routeParams, Person, Company, Activities, Offer, Helper) {

    var idx = $scope;
    idx.user_data = JSON.parse(localStorage.getItem("user_data"));
    idx.is_logged = false;
    idx.searchTerm = ''
    idx.showDropdown = true

    idx.cargaInicial = function(){

        if (Session.check(idx.user_data, 'person')){
            idx.is_logged = true;            

            Person.profile(idx.user_data._id).then(function (response) {
                 
                idx.person = response.data.person;
                idx.user = response.data.user;
                idx.member_since = new Date(response.data.user.member_since.date);

            });
        }

        Offer.filterBy("area").then(function (response) {
          
          idx.offerAreas = response.data.offer;

        });

    }

    idx.goTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    };    

    idx.offerSelected= function(selected) {
      if (selected) {
        idx.goToUrl(selected.originalObject.title, selected.originalObject._id);
      }
    };


    idx.$watch('$viewContentLoaded', function ($evt, data) {
             
        Offer.countAll()
             .then(function (response) {
          
          idx.num_offers = response.data.num_offers;
          idx.title = 'Vacantes de empleo disponibles';

        });

        Offer.mostrecent()
             .then(function (response) {
          
          idx.ofertas = response.data.offers;

        });
    });


    idx.goToUrl = function (job, id) {

            var tmp_this = job.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for (var i = 0; i < arr_busca.length; i++) {
                tmp_this = tmp_this.replace(arr_busca[i], arr_reemplaza[i]);
            }
            job = tmp_this.replace(/[^\\s\w]/g, "-");

            window.location = "/empleos"+ "/" + job + "/" + id;
    }
    
}]);