<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Portal de empleos y bolsa de trabajo - ZonaTrabajo</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <base href="/">

    <? include('header.php');?>

    <script src="app/server.js"></script>
    <script src="app/app.js"></script>
    <script src="app/filters/paginationFilter.js"></script>

    <script src="usuarios/app/services/ActivitiesService.js"></script>
    <script src="usuarios/app/services/CompanyService.js"></script>
    <script src="usuarios/app/services/HelperService.js"></script>
    <script src="usuarios/app/services/PersonService.js"></script>
    <script src="usuarios/app/services/OfferService.js"></script>
    <script src="usuarios/app/services/RedirService.js"></script>
    <script src="usuarios/app/services/SessionService.js"></script>
    <script src="usuarios/app/constants/commonsConstants.js"></script>

    <script src="app/controllers/indexController.js"></script>
    <script src="app/controllers/jobsController.js"></script>
    <script src="app/controllers/jobsDetailController.js"></script>

  </head>
  <body ng-app="zonatrabajoApp">


<!-- Wrap -->
<div id="wrap"> 
    
  <!-- header -->
  <header data-ng-controller="indexController" data-ng-init="cargaInicial()" data-ng-cloak>

    <div class="top-bar">
      <div class="top-info">
        <div class="container">
          <ul class="personal-info">
            <li ng-hide="is_logged">
              <a target="_self" href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77krhttq9amnl8&scope=r_basicprofile+r_emailaddress&state=57743c851c26d5.25098013&redirect_uri=https://api.zonatrabajo.com/linkedin/userdata/"> Ingresar con LinkedIn <i class="fa fa-linkedin"></i> </a>
            </li>
            <li ng-show="is_logged">
              <a target="_self" href="/usuarios/persona">
                <img ng-if="user.avatar != ''" ng-src="https://api.zonatrabajo.com/profile/{{user.avatar}}" class="user-image img-circle" alt="{{person.first_name}} {{person.last_name}}">
                <img ng-if="user.avatar == ''" ng-src="dist/img/avatar.png" class="img-circle" alt="{{person.first_name}} {{person.last_name}}">
                {{person.first_name}} {{person.last_name}}
              </a>
            </li>
          </ul>
          
          <!-- Right Sec -->
          <div class="right-sec"> 
            
              <ul class="personal-info">

                <li>
                  <div class="dropdown">
                    <a class="dropdown-toggle" type="button" data-toggle="dropdown">Registrarme
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a target="_self" href="/usuarios/nuevapersona">Persona</a></li>
                      <li role="separator" class="divider" style="width: 100%"></li>
                      <li><a target="_self" href="/usuarios/nuevaempresa">Empresa</a></li>
                    </ul>
                  </div>
                </li>


                <li><a target="_self" href="/usuarios/entrar">Acceder</a></li>
              </ul>



          </div>
        </div>
      </div>
    </div>
    
    <!-- Navigation -->
    <nav class="navbar">
      <div class="sticky">
        <div class="container"> 
          
          <!-- LOGO -->
          <div class="logo"> <a href="/"><img style="margin-top: -10px" class="img-responsive" src="assets/images/logo_zona_dark_min.png" alt=""> </a> </div>
          
          <!-- Nav -->
          <ul class="nav ownmenu">
            <li> <a href="/">Inicio </a> </li>
            <li> <a href="/contacto">Contacto </a> </li>
          </ul>
          
        </div>
      </div>
    </nav>
  </header>

  <div ng-view=""></div>


  <!-- FOOTER -->
  <footer>
    <div class="container">
      <div class="row"> 
        
        <!-- ABOUT -->
        <div class="col-md-3"> 
          <img src="assets/images/logo-tuniversia-white-footer.png" class="img-responsive" width="50%">
          <div class="about-foot">
            <ul>
              <li>
                <p><i class="fa fa-envelope"></i> contacto@zonatrabajo.com</p>
              </li>
            </ul>
          </div>
        </div>
        
        <!-- Categories -->
        <!-- <div class="col-md-3 col-md-offset-6">
          <h6>Categorías</h6>
          <ul class="tags">
            <li><a href="#.">User Experience</a></li>
            <li><a href="#.">html 5</a></li>
            <li><a href="#.">Css 3</a></li>
            <li><a href="#.">Web Design</a></li>
            <li><a href="#.">Social Media</a></li>
            <li><a href="#.">Web Development</a></li>
            <li><a href="#.">Print Design</a></li>
            <li><a href="#.">e-commerce</a></li>
            <li><a href="#.">Java script</a></li>
          </ul>
        </div> -->
      </div>
    </div>
  </footer>
  
  <!-- RIGHTS -->
  <div class="rights">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p> © Todos los Derechos Reservados <span class="primary-color">Zona</span>trabajo.com</p>
        </div>
        <div class="col-md-6 text-right"> <a href="#.">Politica de Privacidad</a> <a href="#.">Terminos y Condiciones</a> </div>
      </div>
    </div>
  </div>

  
</div>  
     <script>
       !function(A,n,g,u,l,a,r){A.GoogleAnalyticsObject=l,A[l]=A[l]||function(){
       (A[l].q=A[l].q||[]).push(arguments)},A[l].l=+new Date,a=n.createElement(g),
       r=n.getElementsByTagName(g)[0],a.src=u,r.parentNode.insertBefore(a,r)
       }(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

       ga('create', 'UA-XXXXX-X');
       ga('send', 'pageview');

      window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
          if (d.getElementById(id)) return t;
          js = d.createElement(s);
          js.id = id;
          js.src = "https://platform.twitter.com/widgets.js";
          fjs.parentNode.insertBefore(js, fjs);

          t._e = [];
          t.ready = function(f) {
            t._e.push(f);
          };

          return t;
      }(document, "script", "twitter-wjs"));
    </script>

</body>
</html>
