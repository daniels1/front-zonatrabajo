<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/resumenPersonaController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="resumenPersonaController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Resumen            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="persona">Resumen</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->

          <div class="row"> 
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>Ver</h3>
                  <p>Mi Resumen</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-home-outline"></i>
                </div>
                <a href="persona" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>Ver</h3>
                  <p>Mis Postulaciones</p>
                </div>
                <div class="icon">
                  <i class="ion ion-clipboard"></i>
                </div>
                <a href="postulaciones" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
                <div class="inner">
                  <h3>Ver</h3>
                  <p>Mi Curr&iacute;culum</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-contact"></i>
                </div>
                <a href="perfil_persona" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-blue">
                <div class="inner">
                  <h3>Buscar</h3>
                  <p>Ofertas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-list-outline"></i>
                </div>
                <a href="buscar_ofertas" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
          </div>

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">

              <div class="row">
                <div class="col-md-6">

              <!-- Widget: user widget style 1 -->
              <div class="box box-widget widget-user-2" ng-cloak>
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="pull-right no-padding">
                  <a class="btn btn-info btn-xs" href="perfil_persona"><i class="fa fa-pencil"></i> Editar Perfil</a>
                </div>
                <div class="widget-user-header bg-light-blue">
                  <div class="widget-user-image">
                    <img data-ng-if="user.avatar != ''" class="img-circle" data-ng-src="https://api.zonatrabajo.com/profile/{{user.avatar}}" alt="{{person.first_name}} {{person.last_name}}">
                    <img data-ng-if="user.avatar == ''" data-ng-src="dist/img/avatar.png" class="img-circle" alt="{{person.first_name}} {{person.last_name}}">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{person.first_name}} {{person.last_name}}</h3>
                  <h5 class="widget-user-desc">{{(person.title) ? person.title : '-----'}}</h5>                  
                </div>
                <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#"><strong>Perfil completado al</strong>

                          <span ng-class="{'bg-green': percent >= 90, 'bg-yellow': percent >= 60 && percent < 90,  'bg-red': percent < 60}" class="pull-right badge ng-binding">{{percent | number : 2}}%</span>
                      
                      </a></li>
                    <li><a href="#"><strong>Estado de tus postulaciones</strong></a></li>
                    <li><a href="#">Postulado <span class="pull-right badge bg-teal">{{(person.postulations.resumeStatus.postulado) ? person.postulations.resumeStatus.postulado : 0}}</span></a></li>
                    <li><a href="#">Favorito <span class="pull-right badge bg-green">{{(person.postulations.resumeStatus.favorito) ? person.postulations.resumeStatus.favorito : 0}}</span></a></li>
                    <li><a href="#">Descartado <span class="pull-right badge bg-red">{{(person.postulations.resumeStatus.descartado) ? person.postulations.resumeStatus.descartado : 0}}</span></a></li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->

                </div><!-- /.col -->

                <div class="col-md-6">
                  <!-- USERS LIST -->
                  <div class="box box-info box-solid" ng-cloak>
                    <div class="box-header with-border">
                      <h3 class="box-title">Empresas Registradas</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                        <li data-ng-repeat="company in companies" ng-click="viewDetail(company, 'company', 'detalle_empresa')">
                          <img data-ng-if="company.logo != null" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" alt="{{company.social_name}}">
                          <img data-ng-if="company.logo == null" src="dist/img/company.png" alt="{{company.social_name}}">
                          <a class="users-list-name">{{company.social_name}}</a>
                        </li>                        
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                      <a href="lista_empresas" class="uppercase">Ver Empresas</a>
                    </div><!-- /.box-footer -->
                  </div><!--/.box -->
                </div><!-- /.col -->
              </div><!-- /.row -->

            </div><!-- /.col -->

            <div class="col-md-4" ng-cloak>

              <!-- PRODUCT LIST -->
              <div class="box box-success box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Últimas Ofertas Agregadas</h3>
                  <div class="box-tools pull-right">
                    <a href="buscar_ofertas" class="btn btn-default btn-xs"><i class="fa fa-search"></i> Buscar Ofertas</a>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                    <li class="item" data-ng-repeat="offer in ofertas" ng-click="viewDetail(offer, 'offer', 'detalle_oferta')">
                      <div class="product-img">
                        <img ng-if="offer.company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{offer.company.logo}}" class="img-circle" alt="{{offer.company.name}}">
                        <img ng-if="!offer.company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{offer.company.name}}">
                      </div>
                      <div class="product-info">
                        <a href="detalle_oferta" class="product-title">{{offer.title}} </a>
                        <div class="pull-right">
                          <span class="label label-info" ng-show="offer.showSalary"> {{offer.salary | currency : '' : 2}} {{offer.salaryCurrency}}</span>
                          <span class="label label-info" ng-show="!offer.showSalary"> A Convenir </span>
                        </div>
                        <span class="product-description">
                          {{offer.description | limitTo: 50}}
                        </span>
                        {{offer.company.name}} - {{offer.city}} 
                      </div>
                    </li><!-- /.item -->                    
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="buscar_ofertas" class="uppercase">Ver Todas las Ofertas</a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include('copy.php');
         include('control_bar_persona.php'); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
