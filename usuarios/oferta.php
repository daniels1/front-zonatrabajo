<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
      <?
        include('header.php');
      ?>
    <script src="app/controllers/ofertasController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="ofertasController" data-ng-init="cargaInicial()">

      <? include("top_empresa.php"); ?>
      <? include("menu_empresa.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Oferta            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="ofertas">Lista de Ofertas</a></li>
            <li><a href="perfil_empresa">Oferta</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>

          <div class="row">
            <div class="col-md-5">

              <div class="box box-widget widget-user">              
                
                <div class="widget-user-header bg-light-blue">
                  <h3 class="widget-user-username">{{company.name}}</h3>
                  <h5 class="widget-user-desc">{{(company.slogan) ? company.slogan : '-----'}}</h5>
                </div>
                <div class="widget-user-image">
                  <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                  <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-12 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{(company.description) ? '¿Quienes Somos?' : ''}}</h5>
                        <span class="description-text">{{company.description}} <p><b>{{company.sector}}</b></p></span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->                    
                  </div><!-- /.row -->
                </div>
              </div>

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h2 class="box-title">{{offer.title}}</h2>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="col-sm-12">
                    <strong><i class="fa fa-newspaper-o margin-r-5"></i> Descripción</strong>
                    <p class="text-muted">{{offer.description}}</p>
                  </div>
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-sitemap margin-r-5"></i> Area de la Oferta</strong>
                      <p>{{offer.area}}</p>
                  </div>
                  
                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>
                      <p>{{offer.city}} {{(offer.state) ? ' '+offer.state : ''}} {{(offer.country) ? ' '+offer.country : ''}}</p>
                  </div>

                  <div class="col-sm-6">
                    <strong><i class="fa fa-money margin-r-5"></i> Salario</strong>
                      <p class="text-muted" ng-show="offer.showSalary">{{offer.salary | currency : '' : 2}} {{offer.salaryCurrency}}</p>
                      <p class="text-muted" ng-show="!offer.showSalary">A Convenir</p>
                  </div>                                                          

                  <div class="col-sm-4 border-right">
                    <strong><i class="fa fa-clock-o margin-r-5"></i> Tipo de Jornada</strong>
                    <p class="text-muted">{{offer.typeSchedule}}</p>
                  </div>

                  <div class="col-sm-4 border-right">
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Tipo de Contrato</strong>
                    <p class="text-muted">{{offer.typeContract}}</p>
                  </div>

                  <div class="col-sm-4">
                    <strong><i class="fa fa-users margin-r-5"></i> Vacantes</strong>
                    <p class="text-muted">{{offer.jobsAvaiables}}</p>
                  </div>

                  <div class="col-sm-12">
                    <h4>Requisitos</h4>
                    <hr>
                  </div>  
                  
                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-graduation-cap margin-r-5"></i> Educación:</strong>
                      <p>{{offer.educationLevel}}</p>
                  </div>

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-calendar margin-r-5"></i> Años de Experiencia:</strong>
                      <p>{{(offer.experience) ? offer.experience+' años' : 'Sin Experiencia'}}</p>
                  </div>
                 
                  <div class="col-sm-12" ng-show="offer.favCarrers">
                    <strong><i class="fa fa-graduation-cap margin-r-5"></i> Carreras Preferidas</strong> <br>
                    <small class="label label-offers bg-green" ng-repeat="carrer in offer.carrers">{{carrer.description}}</small>
                  </div>
                 
                  <div class="col-sm-12" ng-show="offer.skills.length > 0">
                    <br><strong><i class="fa fa-star margin-r-5"></i> Habilidades</strong> <br>
                    <small class="label label-offers bg-light-blue" ng-repeat="skill in offer.skills">{{skill.description}}</small>
                  </div>

                  <div class="col-sm-12" ng-show="offer.languages.length > 0">
                    <br><strong><i class="fa fa-globe margin-r-5"></i> Idiomas</strong> <br>
                    <small class="label label-offers bg-blue" ng-repeat="lang in offer.languages">{{lang.description}}</small>
                  </div>                  

                  <div class="col-sm-12" ng-show="offer.softwares.length > 0">
                    <br><strong><i class="fa fa-laptop margin-r-5"></i> Software</strong> <br>
                    <small class="label label-offers bg-purple" ng-repeat="soft in offer.softwares">{{soft.description}}</small>
                  </div>
                  <div class="col-sm-12 margin-top-10">
                    <h5 ng-show="offer.avaiableTravel"> Disponibilidad Para Viajar <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.avaiableChangeHome"> Disponibilidad de Cambio de Residencia <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.ownCar"> Vehículo Propio <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.driveLicense"> Posee Licencia de Conducir <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.usVisa"> Visa Americana <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.eurVisa"> Visa Comunidad Europea <i class="fa fa-check"></i></h5>
                  </div>                 

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-7">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#offer" data-toggle="tab"> Datos de la Oferta </a></li>
                </ul>
                <div class="tab-content">

                    <div class="active tab-pane" id="offer">
                      <div class="btn-group pull-left" ng-show="offer._id">
                        <a ng-show="offer.status == 'pause'" ng-click="playOffer(offer)" class="btn btn-success"> <i class="fa fa-play"></i> Activar</a>
                        <a ng-show="offer.status == 'active'" ng-click="pauseOffer(offer)" class="btn btn-warning"> <i class="fa fa-pause"></i> Pausar</a>
                        <a class="btn btn-danger" ng-click="deleteOffer(offer)""> <i class="fa fa-trash"></i> Borrar</a>
                      </div>
                      <form class="form-horizontal" name="frmNewOffer">                        

                        <a class="btn btn-success pull-right" ng-click="saveOfferData()" ng-disabled="!frmNewOffer.$valid && offer.type"> <i class="fa fa-save"></i> Guardar Cambios</a><br><br>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="box box-primary box-solid">
                              <div class="box-header with-border">
                                <h3 class="box-title"> <i class="fa fa-newspaper-o"></i> Datos de la Oferta</h3>
                                <div class="box-tools pull-right">
                                  <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                                </div>
                              </div><!-- /.box-header -->
                            
                              <div class="box-body">
                                <div class="form-group">
                                  <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">
                                     <a href="#" class="btn btn-danger" role="button" ng-click="setOfferType('urgent')">
                                        Urgente <i ng-if="offer.type == 'urgent'" class="fa fa-check"></i>  
                                      </a>
                                     <a href="#" class="btn btn-primary" role="button" ng-click="setOfferType('important')">
                                        Destacada <i ng-if="offer.type == 'important'" class="fa fa-check"></i>  
                                      </a>
                                     <a href="#" class="btn btn-default" role="button" ng-click="setOfferType('normal')">
                                        Normal <i ng-if="offer.type == 'normal'" class="fa fa-check"></i>  
                                      </a> 
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-sm-12">                                  
                                      <input type="text" class="form-control input-sm" name="title" ng-model="offer.title" placeholder="Cargo Ofertado" required>
                                      <span class="text-danger" ng-show="frmNewOffer.title.$touched && frmNewOffer.title.$invalid">Campo Obligatorio</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-sm" name="description" ng-model="offer.description" placeholder="Descripción del Cargo" required></textarea>
                                    <span class="text-danger" ng-show="frmNewOffer.description.$touched && frmNewOffer.description.$invalid">Campo Obligatorio</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12">
                                    <ui-select ng-model="$parent.offer.area">
                                      <ui-select-match class="ui-select-match" placeholder="Area de la Oferta">{{$select.selected.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="area.description as area in listAreas | filter: $select.search">
                                        <div ng-bind-html="area.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                </div>    
                                <div class="form-group">
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.offer.country" on-select="fnStates($item)">
                                      <ui-select-match class="ui-select-match" placeholder="País">{{$select.selected.name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="country.name as country in listCountries | filter: $select.search">
                                      <div ng-bind-html="country.name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.offer.state" on-select="fnCities($item)" ng-disabled="listStates.length == 0">
                                      <ui-select-match class="ui-select-match" placeholder="Estado">{{$select.selected.name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="est.name as est in listStates | filter: $select.search">
                                        <div ng-bind-html="est.name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  

                                  </div>
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.offer.city" ng-disabled="listCities.length == 0">
                                      <ui-select-match class="ui-select-match" placeholder="Ciudad">{{$select.selected.city_name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="city.city_name as city in listCities | filter: $select.search">
                                        <div ng-bind-html="city.city_name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                </div>
                                <div class="form-group">                                
                                  <div class="col-sm-4">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                      <input type="text" class="form-control input-sm" name="salary" ng-model="offer.salary" placeholder="Salarío">
                                    </div>
                                  </div>                                  
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.offer.salaryCurrency">
                                      <ui-select-match class="ui-select-match" placeholder="Moneda">{{$select.selected.name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="curr.siglas as curr in currencies | filter: $select.search">
                                        <div ng-bind-html="curr.name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                  <div class="col-sm-4">
                                      <i-check ng-model="offer.showSalary">Mostrar Salario</i-check>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.offer.typeSchedule">
                                      <ui-select-match class="ui-select-match" placeholder="Tipo de Jornada">{{$select.selected}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="sch in schedules | filter: $select.search">
                                        <div ng-bind-html="sch | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.offer.typeContract">
                                      <ui-select-match class="ui-select-match" placeholder="Tipo de Contrato">{{$select.selected}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="contract in contracts | filter: $select.search">
                                        <div ng-bind-html="contract | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                  <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" name="jobsAvaiables" ng-model="offer.jobsAvaiables" placeholder="Cantidad de Vacantes">
                                  </div>
                                </div>  
                              </div>

                            </div>
                          </div>  
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="box box-primary box-solid">
                              <div class="box-header with-border">
                                <h3 class="box-title"> <i class="fa fa-user"></i> Requisitos del Candidato</h3>
                                <div class="box-tools pull-right">
                                  <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                                </div>
                              </div><!-- /.box-header -->
                            
                              <div class="box-body">
                                <div class="form-group">
                                  <div class="col-sm-6">
                                    <ui-select ng-model="$parent.offer.educationLevel">
                                      <ui-select-match class="ui-select-match" placeholder="Nivel Educativo">{{$select.selected}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="level in levels | filter: $select.search">
                                        <div ng-bind-html="level | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                  <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" name="experience" ng-model="offer.experience" placeholder="Años de Experiencia">
                                  </div>
                                </div>  
                                <div class="form-group">
                                  <div class="col-sm-12">
                                      <i-check ng-model="offer.favCarrers" ng-click="checkOfferCarrers()">Preferencia de Carreras</i-check>
                                  </div>
                                </div>  
                                <div class="form-group">                                
                                  <div class="col-xs-12">
                                    <ui-select multiple ng-model="$parent.offer.carrers" ng-disabled="!offer.favCarrers" close-on-select="false">
                                      <ui-select-match class="ui-select-match" placeholder="Profesión">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="prof in professions | filter: $select.search" minimum-input-length="3">
                                        <div ng-bind-html="prof.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-xs-2"><label>Habilidades:</label></div>
                                  <div class="col-xs-10">
                                    <ui-select multiple ng-model="$parent.offer.skills" close-on-select="false">
                                      <ui-select-match class="ui-select-match">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="skill in listskills | filter: $select.search">
                                        <div ng-bind-html="skill.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-2"><label>Idiomas:</label></div>
                                  <div class="col-xs-10">
                                    <ui-select multiple ng-model="$parent.offer.languages" close-on-select="false">
                                      <ui-select-match class="ui-select-match">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="lang in listlanguages | filter: $select.search">
                                        <div ng-bind-html="lang.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-2"><label>Softwares:</label></div>
                                  <div class="col-xs-10">
                                    <ui-select multiple ng-model="$parent.offer.softwares" close-on-select="false">
                                      <ui-select-match class="ui-select-match">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="soft in listsoftwares | filter: $select.search">
                                        <div ng-bind-html="soft.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>                                  
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-sm-6">
                                      <i-check ng-model="offer.avaiableTravel">Disponibilidad Para Viajar</i-check>
                                  </div>
                                  <div class="col-sm-6">
                                      <i-check ng-model="offer.avaiableChangeHome">Disponibilidad de Cambio de Residencia</i-check>
                                  </div>
                                </div>  
                                <div class="form-group">                                  
                                  <div class="col-sm-6">
                                      <i-check ng-model="offer.driveLicense"> Posee Licencia de Conducir </i-check>
                                  </div>
                                  <div class="col-sm-6">
                                      <i-check ng-model="offer.ownCar"> Vehículo Propio </i-check>
                                  </div>
                                </div>  
                                <div class="form-group">
                                  <div class="col-sm-6">
                                      <i-check ng-model="offer.usVisa">Visa Americana</i-check>
                                  </div>
                                  <div class="col-sm-6">
                                      <i-check ng-model="offer.eurVisa">Visa Comunidad Europea</i-check>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>  
                        </div>


                      </form>
                    </div>

                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include("copy.php"); ?>
      <? include("control_bar_empresa.php"); ?>

    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>
