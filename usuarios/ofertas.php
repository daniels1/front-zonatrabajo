<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
      <?
        include('header.php');
      ?>
    <script src="app/controllers/ofertasController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="ofertasController" data-ng-init="cargaInicial()">

      <? include("top_empresa.php"); ?>
      <? include("menu_empresa.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mis Ofertas
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="ofertas">Lista de Ofertas</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>
          
              <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Listado de Ofertas</h3>
                  <div class="box-tools-list-offers">
                    <div class="form-group">
                      <div class="input-group-list-offers">
                        <select name="offerStatus" class="form-control" ng-model="offerStatus" placeholder="Estatus" ng-disabled="ofertas.length == 0">
                          <option value="">Todas</option>
                          <option value="active">Activas</option>
                          <option value="pause">Pausadas</option>
                          <option value="finish">Finalizadas</option>
                        </select>
                        <select name="offerType" class="form-control" ng-model="offerType" placeholder="Tipo de Oferta" ng-disabled="ofertas.length == 0">
                          <option value="">Todas</option>
                          <option value="urgent">Urgente</option>
                          <option value="important">Importante</option>
                          <option value="normal">Normal</option>
                        </select>
                        <input type="text" class="form-control" ng-model="offerFilter" placeholder="Buscar" ng-disabled="ofertas.length == 0">
                        <div class="input-group-btn">
                          <a ng-click="newOffer('offer', 'oferta')" class="btn btn-success" ng-disabled="company.status != 'verified'"><i class="fa fa-plus"></i> Crear nueva</a>
                        </div><!-- /btn-group -->
                      </div>
                    </div>  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="alert alert-info alert-dismissable"  ng-show="company.status == 'pending'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      Su empresa esta en proceso de Verificación, una vez la información sea verificada procederemos a informarle y podrá usar todos nuestros servicios.
                  </div>

                  <div class="alert alert-warning alert-dismissable"  ng-show="company.status == 'no_verified'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-exclamation-triangle"></i> Aviso</h4>
                      Su empresa <b>NO ESTA VERIFICADA</b>, dirijase a <a href="perfil_empresa"><b>Perfil de Empresa</b></a>, en la pestaña de Validación y adjunte los documentos solicitados. Mientrás no suministre los documentos requeridos no podrá publicar ofertas ni contactar candidatos.
                  </div>

                  <div class="alert alert-info alert-dismissable" ng-show="ofertas.length == 0 && company.status == 'verified'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      En este momento no tienes ofertas cargadas en nuestro sistema. Crea una Oferta haciendo Click <a href="nueva_oferta"><b>AQUÍ</b></a>
                  </div>


                  <div class="table-responsive no-padding">
                    <table class="table table-hover" ng-show="ofertas.length > 0">
                      <thead>
                        <tr>
                          <th>Cargo</th>
                          <th>Fecha de Creación</th>
                          <th>Estatus</th>
                          <th>Tipo de Oferta</th>
                          <th>Descripción</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>  
                        <tr ng-repeat="offer in ofertas | filter: offerType | filter: offerFilter | filter: offerStatus | orderBy: offer.create_at">
                          <td>{{offer.title}}</td>
                          <td>{{offer.created_at.date | amDateFormat:"DD/MM/YYYY"}}</td>
                          <td>
                            <span ng-show="offer.status == 'active'" class="label label-success">Activa <i class="fa fa-check"></i></span>
                            <span ng-show="offer.status == 'pause'" class="label label-warning">Pausada <i class="fa fa-pause"></i></span>
                            <span ng-show="offer.status == 'finish'" class="label bg-gray">Finalizada <i class="fa fa-ban"></i></span>
                          </td>
                          <td>
                            <span ng-show="offer.type == 'urgent'" class="label label-danger">Urgente</span>
                            <span ng-show="offer.type == 'important'" class="label label-primary">Importante</span>
                            <span ng-show="offer.type == 'normal'" class="label label-default">Normal</span>
                          </td>
                          <td>{{offer.description | limitTo: 50}}</td>
                          <td>
                            <a ng-show="offer.status == 'pause'" class="btn btn-xs btn-success" ng-click="playOffer(offer)"> <i class="fa fa-play"></i> Activar</a>
                            <a ng-show="offer.status == 'active'" class="btn btn-xs btn-warning" ng-click="pauseOffer(offer)"> <i class="fa fa-pause"></i> Pausar</a>
                            <a class="btn btn-xs btn-danger" ng-click="deleteOffer(offer)"> <i class="fa fa-trash"></i> Borrar</a>
                            <a  ng-show="offer.status != 'finish'" class="btn btn-xs btn-info" ng-click="viewDetail(offer, 'offer', 'oferta')"> <i class="fa fa-pencil"></i> Editar</a>
                            <a  ng-show="offer.status != 'finish'" class="btn btn-xs btn-primary" ng-click="viewDetail(offer, 'offer', 'candidatos')"> <i class="fa fa-users"></i> Ver Candidatos</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include("copy.php"); ?>
      <? include("control_bar_empresa.php"); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
