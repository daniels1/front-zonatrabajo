<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/detallePersonaController.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="detallePersonaController" data-ng-init="cargaInicial()">

      <?
        include('top_empresa.php');
        include('menu_empresa.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Resumen            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="candidatos"> Candidatos</a></li>
            <li><a href="detalle_persona">Detalle Persona</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-5">

              <div class="box box-widget widget-user" ng-cloak>
                <!-- Add the bg color to the header using any of the bg-* classes -->

                <div class="widget-user-header bg-light-blue">
                  <h3 class="widget-user-username">{{person.first_name}} {{person.last_name}}</h3>
                  <h5 class="widget-user-desc">{{(person.title) ? person.title : '-----'}}</h5>
                </div>
                <div class="widget-user-image">
                  <img data-ng-if="user.avatar" class="img-circle" data-ng-src="https://api.zonatrabajo.com/profile/{{user.avatar}}" alt="User Avatar">
                    <img data-ng-if="!user.avatar" data-ng-src="dist/img/avatar.png" class="img-circle" alt="{{person.first_name}} {{person.last_name}}">
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-12 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{(person.resume) ? '¿Quién Soy yo?' : ''}}</h5>
                        <span class="description-text">{{person.resume}}</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->                    
                  </div><!-- /.row -->
                  <div class="row">
                    <div class="col-sm-6 border-right">
                      <div class="description-block">
                        <h5 class="description-header"><i class="fa fa-envelope margin-r-5"></i> Email</h5>
                        <span class="description-text">{{user.email}}</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3 border-right">
                      <div class="description-block">
                        <h5 class="description-header"><i class="fa fa-phone margin-r-5"></i> Teléfono</h5>
                        <span class="description-text">{{person.phone}}</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                    <div class="col-sm-3">
                      <div class="description-block">
                        <h5 class="description-header"><i class="fa fa-mobile margin-r-5"></i> Móvil</h5>
                        <span class="description-text">{{person.mobile}}</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->
                  </div>
                </div>
              </div>

              <!-- About Me Box -->
              <div class="box box-primary" ng-cloak>
                <div class="box-header with-border">
                  <h3 class="box-title">Perfil</h3>
                  <a target="_blank" ng-if="person.cv" ng-href="https://zonatrabajo.com/hojasdevida/{{person.cv}}" class="btn btn-primary pull-right"> <i class="fa fa-file-pdf-o"></i> Ver CV  </a>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-birthday-cake margin-r-5"></i> Fecha de Nacimiento</strong>
                    <p class="text-muted">{{person.birthday}}</p>
                  </div>
                  <div class="col-sm-6">
                    <strong><i class="fa fa-flag-o margin-r-5"></i> Nacionalidad</strong>
                    <p class="text-muted">{{person.nationality}}</p>
                  </div>
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Dirección</strong>
                    <p class="text-muted">{{person.address}} {{person.city}} {{(person.state) ? '- '+person.state : ''}} {{(person.country) ? '- '+person.country : ''}}</p>
                  </div>  

                  <div class="col-sm-12 skills">
                    <strong><i class="fa fa-star margin-r-5"></i> Habilidades</strong> <br>
                    <small class="label bg-light-blue" ng-repeat="skill in person.skills">{{skill.description}}</small>
                  </div>                  

                  <div class="col-sm-12 langs">
                    <strong><i class="fa fa-globe margin-r-5"></i> Idiomas</strong> <br>
                    <small class="label bg-blue" ng-repeat="lang in person.languages">{{lang.description}}</small>
                  </div>                  

                  <div class="col-sm-12 softs">
                    <strong><i class="fa fa-laptop margin-r-5"></i> Software</strong> <br>
                    <small class="label bg-purple" ng-repeat="soft in person.softwares">{{soft.description}}</small>
                  </div>
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Intereses</strong>
                    <p>{{person.interest}}</p>
                  </div>  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-7">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#resumen" data-toggle="tab"><span class="glyphicon glyphicon-file"></span>Resumen</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="resumen" ng-cloak>

                    <perfect-scrollbar class="scroller" wheel-propagation="true" wheel-speed="3" min-scrollbar-length="20">
                      <ul class="timeline timeline-inverse">
                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-blue">
                            Experiencia Laboral
                          </span>
                        </li>
                        <!-- /.timeline-label -->
                        
                        <li ng-show="!person.experiences">
                          <i class="fa fa-warning"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header">Aviso</h3>
                            <div class="timeline-body">
                              No posee experiencia laboral.
                            </div>  
                          </div>
                        </li>

                        <!-- timeline item -->
                        <li ng-repeat="exp in person.experiences track by $index">
                          <i class="fa fa-building bg-blue"></i>
                          <div class="timeline-item">
                            <span class="time"><i class="fa fa-calendar"></i> {{exp.startMonth}} {{exp.startYear}} – {{exp.actual ? 'Actualmente': exp.endMonth+' '+exp.endYear}}</span>
                            <h3 class="timeline-header"><a href="#">{{exp.position}}</a> <br> {{exp.company}}</h3>
                            <div class="timeline-body">
                              {{exp.resume}}
                              <br>
                              <small>{{exp.sector}} - {{exp.area}}</small>
                            </div>                            
                          </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-green">
                            Educación
                          </span>
                        </li>

                        <li ng-show="!person.studies">
                          <i class="fa fa-warning"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header">Aviso</h3>
                            <div class="timeline-body">
                              No posee estudios realizados.
                            </div>                          
                          </div>
                        </li>

                        <!-- timeline item -->
                        <li ng-repeat="study in person.studies track by $index">
                          <i class="fa fa-graduation-cap bg-green"></i>
                          <div class="timeline-item">
                            <span class="time"><i class="fa fa-calendar"></i> {{study.startMonth}} {{study.startYear}} – {{(study.cursando) ? 'Actualmente': study.endMonth+' '+study.endYear}}</span>
                            <h3 class="timeline-header"><a class="text-success" href="#">{{study.profesion}}</a> </h3>
                            <div class="timeline-body">
                              {{study.institute}}
                              <br>
                              <small> {{study.city}} {{(study.state) ? '- '+study.state : ''}} {{(study.country) ? '- '+study.country : ''}}</small>
                            </div>                            
                          </div>
                        </li>
                        <!-- END timeline item -->

                        <li>
                          <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                      </ul>
                    </perfect-scrollbar>  
                  </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     <? include('copy.php');
         include('control_bar_empresa.php'); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>
