<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/perfilPersonaController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="perfilPersonaController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Currículum            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="persona">Currículum</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-5">

              <div class="box box-widget widget-user" ng-cloak>
                <!-- Add the bg color to the header using any of the bg-* classes -->
                
                <div class="widget-user-header bg-light-blue">
                  <h3 class="widget-user-username">{{person.first_name}} {{person.last_name}}</h3>
                  <h5 class="widget-user-desc">{{(person.title) ? person.title : '-----'}}</h5>
                </div>
                <div class="widget-user-image">
                  <img data-ng-if="user.avatar != ''" class="img-circle" data-ng-src="https://api.zonatrabajo.com/profile/{{user.avatar}}" alt="User Avatar">
                  <img data-ng-if="user.avatar == ''" data-ng-src="dist/img/avatar.png" class="img-circle" alt="{{person.first_name}} {{person.last_name}}">
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-12 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{(person.resume) ? '¿Quién Soy yo?' : ''}}</h5>
                        <span class="description-text">{{person.resume}}</span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->                    
                  </div><!-- /.row -->
                </div>
              </div>

              <!-- About Me Box -->
              <div class="box box-primary" ng-cloak>
                <div class="box-header with-border">
                  <h3 class="box-title">Perfil</h3>
                  <a target="_blank" ng-if="person.cv" ng-href="https://zonatrabajo.com/hojasdevida/{{person.cv}}"" class="btn btn-success pull-right"> <i class="fa fa-file-pdf-o"></i> Descargar Hoja de Vida  </a>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-birthday-cake margin-r-5"></i> Fecha de Nacimiento</strong>
                    <p class="text-muted">{{person.birthday}}</p>
                  </div>
                  <div class="col-sm-6">
                    <strong><i class="fa fa-flag-o margin-r-5"></i> Nacionalidad</strong>
                    <p class="text-muted">{{person.nationality}}</p>
                  </div>
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Dirección</strong>
                    <p class="text-muted">{{person.address}} {{person.city}} {{(person.state) ? ', '+person.state : ''}} {{(person.country) ? ', '+person.country : ''}}</p>
                  </div>  

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted">{{user.email}}</p>
                  </div>
                  <div class="col-sm-3">
                    <strong><i class="fa fa-phone margin-r-5"></i> Teléfono</strong>
                    <p class="text-muted">{{person.phone}}</p>
                  </div>
                  <div class="col-sm-3">
                    <strong><i class="fa fa-mobile margin-r-5"></i> Móvil</strong>
                    <p class="text-muted">{{person.mobile}}</p>
                  </div>

                  <div class="col-sm-12 skills">
                    <strong><i class="fa fa-star margin-r-5"></i> Habilidades</strong> <br>
                    <small class="label bg-light-blue" ng-repeat="skill in person.skills">{{skill.description}}</small>
                  </div>                  

                  <div class="col-sm-12 langs">
                    <strong><i class="fa fa-globe margin-r-5"></i> Idiomas</strong> <br>
                    <small class="label bg-blue" ng-repeat="lang in person.languages">{{lang.description}}</small>
                  </div>                  

                  <div class="col-sm-12 softs">
                    <strong><i class="fa fa-laptop margin-r-5"></i> Software</strong> <br>
                    <small class="label bg-purple" ng-repeat="soft in person.softwares">{{soft.description}}</small>
                  </div>
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Intereses</strong>
                    <p>{{person.interest}}</p>
                  </div>  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-7">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li ng-class="{'active' : person.experiences.length > 0 || person.studies.length > 0 }"><a href="#resumen" data-toggle="tab"><span class="glyphicon glyphicon-file"></span>Resumen</a></li>
                  <li ng-class="{'active' : person.experiences.length == 0 || person.studies.length == 0 }"><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Editar</a></li>
                </ul>
                <div class="tab-content">
                  <div ng-class="{'active' : person.experiences.length > 0 || person.studies.length > 0 }" class="tab-pane" id="resumen" ng-cloak>

                    <div class="row" ng-show="editExperience">
                      <div class="col-sm-12">
                        <div class="box box-info box-solid">
                          <div class="box-header with-border">
                            <h3 class="box-title"> <i class="fa fa-building"></i> Actualizar Experiencia Laboral</h3>                              
                          </div><!-- /.box-header -->
                        
                          <div class="box-body">
                            <form name="frmExperience">
                              <div class="row">
                                <div class="col-xs-12">
                                  <input type="text" class="form-control input-sm" placeholder="Empresa" ng-model="exp.company">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <input type="text" class="form-control input-sm" placeholder="Cargo" ng-model="exp.position">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-6">
                                  <ui-select ng-model="$parent.exp.sector">
                                    <ui-select-match class="ui-select-match" placeholder="Sector">{{$select.selected.description}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="sector.description as sector in sectors | filter: $select.search" minimum-input-length="3">
                                      <div ng-bind-html="sector.description | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>  
                                </div>
                                <div class="col-xs-6">

                                  <ui-select ng-model="$parent.exp.area">
                                    <ui-select-match class="ui-select-match" placeholder="Area">{{$select.selected.description}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="area.description as area in areas | filter: $select.search" minimum-input-length="3">
                                      <div ng-bind-html="area.description | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-4">
                                  <ui-select ng-model="$parent.exp.startMonth">
                                    <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                      <div ng-bind-html="month | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                                <div class="col-xs-2">
                                  <input type="text" class="form-control input-sm" placeholder="Año" ng-model="exp.startYear" mask="9999"> 
                                </div>
                                <div class="col-xs-4">
                                  <ui-select ng-model="$parent.exp.endMonth" ng-disabled="exp.actual">
                                    <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                      <div ng-bind-html="month | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                                <div class="col-xs-2">
                                  <input type="text" class="form-control input-sm" placeholder="Año" ng-model="exp.endYear" mask="9999" ng-disabled="exp.actual">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-6 col-xs-offset-6">
                                    <i-check ng-model="exp.actual">Actualmente</i-check>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <textarea class="form-control input-sm" id="exp.resume" ng-model="exp.resume" placeholder="Proporciona detalles sobre tus logros y proyectos en los que hayas trabajado"></textarea>
                                </div>
                              </div>
                              </form>
                          </div>
                          <div class="box-footer pull-right">
                              <button type="button" class="btn btn-primary btn-xs" ng-click="updateExperience(exp)"> <i class="fa fa-check"></i> </button>
                              <button type="button" class="btn btn-danger btn-xs" ng-click="cancelEditExperience()"> <i class="fa fa-close"></i> </button>
                          </div><!-- /.box-footer -->
                        </div>
                      </div>  
                    </div>

                    <div class="row" ng-show="editStudy">
                      <div class="col-sm-12">
                        <div class="box box-primary box-solid">
                          <div class="box-header with-border">
                            <h3 class="box-title"> <i class="fa fa-graduation-cap"></i> Actualizar Educación</h3>
                          </div><!-- /.box-header -->
                        
                        <div class="box-body">
                          <form name="frmStudy">
                            <div class="row">
                              <div class="col-xs-12">
                                <ui-select ng-model="$parent.study.institute">
                                  <ui-select-match class="ui-select-match"  placeholder="Instituto">{{$select.selected.nombre_institucion}}</ui-select-match>
                                  <ui-select-choices class="ui-select-choices" repeat="inst.nombre_institucion as inst in listinstitutions | filter: $select.search" minimum-input-length="3">
                                    <div ng-bind-html="inst.nombre_institucion | highlight: $select.search"></div>
                                    <small> {{inst.siglas}}</small>
                                  </ui-select-choices>
                                </ui-select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-12">
                                <ui-select ng-model="$parent.study.profesion">
                                  <ui-select-match class="ui-select-match"  placeholder="Profesión">{{$select.selected.description}}</ui-select-match>
                                  <ui-select-choices class="ui-select-choices" repeat="prof.description as prof in professions | filter: $select.search" minimum-input-length="3">
                                    <div ng-bind-html="prof.description | highlight: $select.search"></div>
                                  </ui-select-choices>
                                </ui-select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-4">
                                  <ui-select ng-model="$parent.study.country" on-select="fnStudyStates($item)">
                                    <ui-select-match class="ui-select-match" placeholder="País">{{$select.selected.name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="country.name as country in listCountries | filter: $select.search">
                                      <div ng-bind-html="country.name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>  
                              </div>
                              <div class="col-xs-4">
                                <ui-select ng-model="$parent.study.state" on-select="fnStudyCities($item)" ng-disabled="listStates.length == 0">
                                  <ui-select-match class="ui-select-match" placeholder="Estado">{{$select.selected.name}}</ui-select-match>
                                  <ui-select-choices class="ui-select-choices" repeat="est.name as est in listStates | filter: $select.search">
                                    <div ng-bind-html="est.name | highlight: $select.search"></div>
                                  </ui-select-choices>
                                </ui-select>  
                              </div>
                              <div class="col-sm-4">
                                <ui-select ng-model="$parent.study.city" ng-disabled="listStudyCities.length == 0">
                                  <ui-select-match class="ui-select-match" placeholder="Ciudad">{{$select.selected.city_name}}</ui-select-match>
                                  <ui-select-choices class="ui-select-choices" repeat="city.city_name as city in listStudyCities | filter: $select.search">
                                    <div ng-bind-html="city.city_name | highlight: $select.search"></div>
                                  </ui-select-choices>
                                </ui-select> 
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-4">
                                <ui-select ng-model="$parent.study.startMonth">
                                  <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                  <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                    <div ng-bind-html="month | highlight: $select.search"></div>
                                  </ui-select-choices>
                                </ui-select>                                  
                              </div>
                              <div class="col-xs-2">
                                <input type="text" class="form-control input-sm" placeholder="Año" ng-model="study.startYear" mask="9999"> 
                              </div>
                              <div class="col-xs-4">
                                <ui-select ng-model="$parent.study.endMonth" ng-disabled="study.cursando">
                                  <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                  <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                    <div ng-bind-html="month | highlight: $select.search"></div>
                                  </ui-select-choices>
                                </ui-select>
                              </div>
                              <div class="col-xs-2">
                                <input type="text" class="form-control input-sm" placeholder="Año" ng-disabled="study.cursando" ng-model="study.endYear"  mask="9999"> 
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-xs-6 col-xs-offset-6">
                                 <div class="checkbox icheck">
                                    <i-check ng-model="study.cursando">Cursando</i-check>                                      
                                  </div>
                              </div>
                            </div>                              
                            <div class="row">
                              <div class="col-xs-12">
                                <textarea class="form-control input-sm" id="resume" ng-model="study.resume" placeholder="Explica qué aprendiste en este curso"></textarea>
                              </div>
                            </div>
                            </form>
                        </div>

                          <div class="box-footer pull-right">
                              <button type="button" class="btn btn-primary btn-xs" ng-click="updateStudy(study)"> <i class="fa fa-check"></i> </button>
                              <button type="button" class="btn btn-danger btn-xs" ng-click="cancelEditStudy()"> <i class="fa fa-close"></i> </button>
                          </div><!-- /.box-footer -->
                        </div>
                      </div>  
                    </div>

                    <perfect-scrollbar class="scroller" wheel-propagation="true" wheel-speed="3" min-scrollbar-length="20">
                      <ul class="timeline timeline-inverse" ng-show="(!editStudy && !editExperience)" >
                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-blue">
                            Experiencia Laboral
                          </span>
                        </li>
                        <!-- /.timeline-label -->
                        
                        <li ng-show="person.experiences.length == 0">
                          <i class="fa fa-warning"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header">Aviso</h3>
                            <div class="timeline-body">
                              En este momento no tienes registrada información sobre experiencia laboral.
                            </div>  
                          </div>
                        </li>

                        <!-- timeline item -->
                        <li ng-repeat="exp in person.experiences track by $index">
                          <i class="fa fa-building bg-blue"></i>
                          <div class="timeline-item">
                            <span class="time"><i class="fa fa-calendar"></i> {{exp.startMonth}} {{exp.startYear}} – {{exp.actual ? 'Actualmente': exp.endMonth+' '+exp.endYear}}</span>
                            <h3 class="timeline-header"><a href="#">{{exp.position}}</a> <br> {{exp.company}}</h3>
                            <div class="timeline-body">
                              {{exp.resume}}
                              <br>
                              <small>{{exp.sector}} - {{exp.area}}</small>
                            </div>
                            <div class="timeline-footer pull-right">
                              <a ng-click="fnEditExperience($index)" class="btn btn-primary btn-xs"> <i class="fa fa-pencil"></i> </a>
                              <a ng-click="removeExperience(exp)" class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </a>
                            </div>
                          </div>
                        </li>
                        <!-- END timeline item -->
                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-green">
                            Educación
                          </span>
                        </li>

                        <li ng-show="person.studies.length == 0">
                          <i class="fa fa-warning"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header">Aviso</h3>
                            <div class="timeline-body">
                              En este momento no tienes registrada información sobre los estudios realizados.
                            </div>                          
                          </div>
                        </li>

                        <!-- timeline item -->
                        <li ng-repeat="study in person.studies track by $index">
                          <i class="fa fa-graduation-cap bg-green"></i>
                          <div class="timeline-item">
                            <span class="time"><i class="fa fa-calendar"></i> {{study.startMonth}} {{study.startYear}} – {{(study.cursando) ? 'Actualmente': study.endMonth+' '+study.endYear}}</span>
                            <h3 class="timeline-header"><a class="text-success" href="#">{{study.profesion}}</a> </h3>
                            <div class="timeline-body">
                              {{study.institute}}
                              <br>
                              <small> {{study.city}} {{(study.state) ? '- '+study.state : ''}} {{(study.country) ? '- '+study.country : ''}}</small>
                            </div>
                            <div class="timeline-footer pull-right">
                              <a ng-click="fnEditStudy($index)" class="btn btn-primary btn-xs"> <i class="fa fa-pencil"></i> </a>
                              <a ng-click="removeStudy(study)" class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </a>
                            </div>
                          </div>
                        </li>
                        <!-- END timeline item -->

                        <li>
                          <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                      </ul>
                    </perfect-scrollbar>  
                  </div><!-- /.tab-pane -->

                  <div ng-class="{'active' : person.experiences.length == 0 || person.studies.length == 0 }" class="tab-pane" id="profile">
                    <form class="form-horizontal" name="frmPerfilPersona" ng-submit="savePersonData()">

                      <button type="submit" class="btn btn-success pull-right" ng-disabled="!frmPerfilPersona.$valid"> <i class="fa fa-save"></i> Guardar Cambios</button><br><br>
                      
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                              <h3 class="box-title"> <i class="fa fa-user"></i> Datos Personales</h3>
                              <div class="box-tools pull-right">
                                <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                              </div>
                            </div><!-- /.box-header -->
                          
                            <div class="box-body">

                                <div class="col-sm-9">
                                  <div class="form-group">
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" ng-model="person.first_name" name="first_name" placeholder="Nombres" required>
                                      <span class="text-danger" ng-show="frmPerfilPersona.first_name.$touched && frmPerfilPersona.first_name.$invalid">Campo Obligatorio</span>
                                    </div>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control input-sm" ng-model="person.last_name" placeholder="Apellidos" required>
                                      <span class="text-danger" ng-show="frmPerfilPersona.last_name.$touched && frmPerfilPersona.last_name.$invalid">Campo Obligatorio</span>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control input-sm" ng-model="person.birthday" name="birthday" placeholder="Fecha de Nacimiento" mask="39/19/2999">
                                      <span class="text-danger" ng-show="frmPerfilPersona.birthday.$touched && frmPerfilPersona.birthday.$invalid">Campo Obligatorio</span>
                                    </div>
                                    <div class="col-sm-8">
                                      <ui-select ng-model="$parent.person.nationality">
                                        <ui-select-match class="ui-select-match" placeholder="Nacionalidad">{{$select.selected}}</ui-select-match>
                                        <ui-select-choices class="ui-select-choices" repeat="nat in nationalities | filter: $select.search">
                                          <div ng-bind-html="nat | highlight: $select.search"></div>
                                        </ui-select-choices>
                                      </ui-select>  
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <input type="text" class="form-control input-sm" ng-model="person.title" id="title" placeholder="Título">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <textarea class="form-control input-sm" id="resume" ng-model="person.resume" placeholder="Escribe una reseña sobre tu perfil profesional para que las personas te conozcan un poco mejor"></textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-sm-3">
                                    <div ngf-drop ngf-select ng-model="file" class="drop-box" ngf-drag-over-class="'dragover'" ngf-multiple="false" ngf-allow-dir="true"
                                          accept="image/*" ngf-pattern="'image/*'">Arrastra la foto aquí o haga click para actualizar </div>
                                    <div ngf-no-file-drop>Recomendamos Actualizar su navegador</div>                                  
                                </div>


                            </div>
                            
                          </div>
                        </div>  
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="box box-primary box-solid collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title"> <i class="fa fa-home"></i> Datos de Ubicación</h3>
                              <div class="box-tools pull-right">
                                <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                              </div>
                            </div><!-- /.box-header -->
                          
                            <div class="box-body">
                              <div class="form-group">
                                <div class="col-sm-12">
                                  <textarea class="form-control input-sm" id="address" ng-model="person.address" placeholder="Dirección Residencial"></textarea>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-4">
                                  <ui-select ng-model="$parent.person.country" on-select="fnHomeStates($item)">
                                    <ui-select-match class="ui-select-match" placeholder="País">{{$select.selected.name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="country.name as country in listCountries | filter: $select.search">
                                      <div ng-bind-html="country.name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>  

                                </div>
                                <div class="col-sm-4">
                                  <ui-select ng-model="$parent.person.state" on-select="fnHomeCities($item)" ng-disabled="listStates.length == 0">
                                    <ui-select-match class="ui-select-match" placeholder="Estado">{{$select.selected.name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="est.name as est in listStates | filter: $select.search">
                                      <div ng-bind-html="est.name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>  

                                </div>
                                <div class="col-sm-4">
                                  <ui-select ng-model="$parent.person.city" ng-disabled="listCities.length == 0">
                                    <ui-select-match class="ui-select-match" placeholder="Ciudad">{{$select.selected.city_name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="city.city_name as city in listCities | filter: $select.search">
                                      <div ng-bind-html="city.city_name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="col-sm-6">
                                  <input type="email" class="form-control input-sm" name="email" ng-model="user.email" placeholder="Email" required>
                                  <span class="text-danger" ng-show="frmPerfilPersona.email.$touched && frmPerfilPersona.email.$invalid">Campo Obligatorio</span>
                                </div>
                                <div class="col-sm-3">
                                  <input type="text" class="form-control input-sm" id="phone" ng-model="person.phone" placeholder="Teléfono" mask="(9999) 999-9999">
                                  <span class="text-danger" ng-show="frmPerfilPersona.phone.$touched && frmPerfilPersona.phone.$invalid">Campo Obligatorio</span>
                                </div>
                                <div class="col-sm-3">
                                  <input type="text" class="form-control input-sm" id="mobile" ng-model="person.mobile" placeholder="Móvil" mask="(9999) 999-9999">
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>  
                      </div>
                      
                      <div class="row" ng-show="!pp.editExperience">
                        <div class="col-sm-12">
                          <div class="box box-info box-solid collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title"> <i class="fa fa-building"></i> Experiencia Laboral</h3>
                              <div class="box-tools pull-right">
                                <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                              </div>
                            </div><!-- /.box-header -->
                          
                            <div class="box-body">
                              <form name="frmExperience">
                                <div class="row">
                                  <div class="col-xs-12">
                                    <input type="text" class="form-control input-sm" placeholder="Empresa" ng-model="exp.company">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-12">
                                    <input type="text" class="form-control input-sm" placeholder="Cargo" ng-model="exp.position">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-6">
                                    <ui-select ng-model="$parent.exp.sector">
                                      <ui-select-match class="ui-select-match" placeholder="Sector">{{$select.selected.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="sector.description as sector in sectors | filter: $select.search">
                                        <div ng-bind-html="sector.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                  <div class="col-xs-6">

                                    <ui-select ng-model="$parent.exp.area">
                                      <ui-select-match class="ui-select-match" placeholder="Area">{{$select.selected.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="area.description as area in areas | filter: $select.search">
                                        <div ng-bind-html="area.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-4">
                                    <ui-select ng-model="$parent.exp.startMonth">
                                      <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                        <div ng-bind-html="month | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                  <div class="col-xs-2">
                                    <input type="text" class="form-control input-sm" placeholder="Año" ng-model="exp.startYear" mask="9999"> 
                                  </div>
                                  <div class="col-xs-4">
                                    <ui-select ng-model="$parent.exp.endMonth" ng-disabled="exp.actual">
                                      <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                        <div ng-bind-html="month | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                  <div class="col-xs-2">
                                    <input type="text" class="form-control input-sm" placeholder="Año" ng-model="exp.endYear" mask="9999" ng-disabled="exp.actual">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-6 col-xs-offset-6">
                                      <i-check ng-model="exp.actual">Actualmente</i-check>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-12">
                                    <textarea class="form-control input-sm" id="exp.resume" ng-model="exp.resume" placeholder="Proporciona detalles sobre tus logros y proyectos en los que hayas trabajado"></textarea>
                                  </div>
                                </div>
                                </form>
                            </div>
                            <div class="box-footer text-center">
                              <button type="button" class="btn btn-info pull-right btn-xs" ng-click="addExperience()"> <i class="fa fa-check"></i> Agregar</button>
                            </div><!-- /.box-footer -->
                          </div>
                        </div>  
                      </div>

                      <div class="row" ng-show="!pp.editStudy">
                        <div class="col-sm-12">
                          <div class="box box-primary box-solid collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title"> <i class="fa fa-graduation-cap"></i> Educación</h3>
                              <div class="box-tools pull-right">
                                <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                              </div>
                            </div><!-- /.box-header -->
                          
                          <div class="box-body">
                            <form name="frmStudy">
                              <div class="row">
                                <div class="col-xs-12">
                                  <ui-select ng-model="$parent.study.institute">
                                    <ui-select-match class="ui-select-match"  placeholder="Instituto">{{$select.selected.nombre_institucion}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="inst.nombre_institucion as inst in listinstitutions | filter: $select.search" minimum-input-length="3">
                                      <div ng-bind-html="inst.nombre_institucion | highlight: $select.search"></div>
                                      <small> {{inst.siglas}}</small>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <ui-select ng-model="$parent.study.profesion">
                                    <ui-select-match class="ui-select-match"  placeholder="Profesión">{{$select.selected.description}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="prof.description as prof in professions | filter: $select.search" minimum-input-length="3">
                                      <div ng-bind-html="prof.description | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-4">
                                  <ui-select ng-model="$parent.study.country" on-select="fnStudyStates($item)">
                                    <ui-select-match class="ui-select-match" placeholder="País">{{$select.selected.name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="country.name as country in listCountries | filter: $select.search">
                                      <div ng-bind-html="country.name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>  
                                </div>
                                <div class="col-xs-4">
                                  <ui-select ng-model="$parent.study.state" on-select="fnStudyCities($item)">
                                    <ui-select-match class="ui-select-match" placeholder="Estado">{{$select.selected.name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="est.name as est in listStates | filter: $select.search">
                                      <div ng-bind-html="est.name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>  
                                </div>
                                <div class="col-sm-4">
                                  <ui-select ng-model="$parent.study.city">
                                    <ui-select-match class="ui-select-match" placeholder="Ciudad">{{$select.selected.city_name}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="city.city_name as city in listStudyCities | filter: $select.search">
                                      <div ng-bind-html="city.city_name | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select> 
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-4">
                                  <ui-select ng-model="$parent.study.startMonth">
                                    <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                      <div ng-bind-html="month | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>                                  
                                </div>
                                <div class="col-xs-2">
                                  <input type="text" class="form-control input-sm" placeholder="Año" ng-model="study.startYear" mask="9999"> 
                                </div>
                                <div class="col-xs-4">
                                  <ui-select ng-model="$parent.study.endMonth" ng-disabled="study.cursando">
                                    <ui-select-match class="ui-select-match" placeholder="Meses">{{$select.selected}}</ui-select-match>
                                    <ui-select-choices class="ui-select-choices" repeat="month in months | filter: $select.search">
                                      <div ng-bind-html="month | highlight: $select.search"></div>
                                    </ui-select-choices>
                                  </ui-select>
                                </div>
                                <div class="col-xs-2">
                                  <input type="text" class="form-control input-sm" placeholder="Año" ng-disabled="study.cursando" ng-model="study.endYear"  mask="9999"> 
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-6 col-xs-offset-6">
                                   <div class="checkbox icheck">
                                      <i-check ng-model="study.cursando">Cursando</i-check>                                      
                                    </div>
                                </div>
                              </div>                              
                              <div class="row">
                                <div class="col-xs-12">
                                  <textarea class="form-control input-sm" id="resume" ng-model="study.resume" placeholder="Explica qué aprendiste en este curso"></textarea>
                                </div>
                              </div>
                              </form>
                          </div>

                            <div class="box-footer text-center">
                              <button type="button" class="btn btn-info pull-right btn-xs" ng-click="addStudy()"> <i class="fa fa-check"></i> Agregar</button>
                            </div><!-- /.box-footer -->
                          </div>
                        </div>  
                      </div>

                      <div class="row">
                        <div class="col-sm-12">
                          <div class="box box-success box-solid collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title"> <i class="fa fa-star"></i> Habilidades / Idiomas / Software / Intereses</h3>
                              <div class="box-tools pull-right">
                                <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                              </div>
                            </div><!-- /.box-header -->
                          
                            <div class="box-body">

                                <div class="form-group">
                                  <div class="col-xs-2"><label>Habilidades:</label></div>
                                  <div class="col-xs-10">
                                    <ui-select multiple ng-model="$parent.person.skills" close-on-select="false" theme="bootstrap">
                                      <ui-select-match class="ui-select-match">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="skill in listskills | filter: $select.search">
                                        <div ng-bind-html="skill.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-2"><label>Idiomas:</label></div>
                                  <div class="col-xs-10">
                                    <ui-select multiple ng-model="$parent.person.languages" close-on-select="false" theme="bootstrap">
                                      <ui-select-match class="ui-select-match">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="lang in listlanguages | filter: $select.search">
                                        <div ng-bind-html="lang.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-xs-2"><label>Softwares:</label></div>
                                  <div class="col-xs-10">
                                    <ui-select multiple ng-model="$parent.person.softwares" close-on-select="false" theme="bootstrap">
                                      <ui-select-match class="ui-select-match">{{$item.description}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="soft in listsoftwares | filter: $select.search">
                                        <div ng-bind-html="soft.description | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>                                  
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="interest" class="col-xs-2 control-label">Intereses</label>
                                  <div class="col-xs-10">
                                    <textarea class="form-control input-sm" id="interest" ng-model="person.interest" placeholder="Hablanos un poco acerca de ti las cosas que te gustan hacer, pasatiempos, que haces en tu tiempo libre"></textarea>
                                  </div>    
                                </div>
                            </div>

                          </div>
                        </div>  
                      </div>


                      <div class="row">
                        <div class="col-sm-12">
                          <div class="box box-primary box-solid collapsed-box">
                            <div class="box-header with-border">
                              <h3 class="box-title"> <i class="fa fa-file-text-o"></i> Hoja de Vida </h3>
                              <div class="box-tools pull-right">
                                <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                              </div>
                            </div><!-- /.box-header -->
                          

                            <div class="box-body">
                              <div class="col-sm-12">                                  
                                  <h4>Adjunta tu Hoja de Vida, para que las empresas que te encuentren puedan tener una copia.</h4>
                              </div>

                              <div class="col-sm-12">
                                  <div ngf-drop ngf-select ng-model="cv_file" class="drop-box" ngf-drag-over-class="'dragover'" ngf-multiple="false" ngf-allow-dir="true"
                                        accept="application/pdf" ngf-pattern="application/pdf"> <i class="fa fa-file-pdf-o fa-2x"></i> Arrastra el Documento PDF aquí o haz click para actualizar</div>
                                  <div ngf-no-file-drop>Recomendamos Actualizar su navegador</div>                                  
                              </div>                              
                            </div>

                          </div>
                        </div>  
                      </div>


                    </form>
                  </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <? include('copy.php');
         include('control_bar_persona.php'); ?>
    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>
