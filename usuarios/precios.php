<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/resumenEmpresaController.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="resumenEmpresaController" data-ng-init="cargaInicial()">

      <?
        include('top_empresa.php');
        include('menu_empresa.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Contratar            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="empresa">Contratar</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>
          <!-- Info boxes -->

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Servicios</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12 col-sm-12 col-xs-12">

              <div class="pricing-tables compress">

                  <div class="col-sm-4 col-md-3 col-xs-12 ">

                      <div class="price-pack recommended">

                          <div class="head">
                              <h3>Contratación Anual</h3>
                          </div>  

                          <ul class="item-list list-unstyled">
                              <li><strong>30</strong> Anuncios Normales</li>
                              <li><strong>10 </strong>Anuncios Destacados</li>
                              <li><strong>5</strong> Anuncios Urgentes</li>
                              <li><strong>100</strong> Búsquedas Mensuales</li>

                          </ul>

                          <div class="price">
                              <h4><span class="symbol">$</span>449.000</h4>
                          </div>

                          <button type="button" class="btn btn-lg btn-orange">Contratar</button>
                      </div>

                  </div>
                  <div class="col-sm-8 col-md-9 col-xs-12 ">
                    
                  </div>


              </div>
                <!-- end -->
            </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer-->
      </div>

                        </section></div>



        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include('copy.php');
         include('control_bar_empresa.php'); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script>
      $(function(){
          $('#candidatos, #ofertas').slimScroll({
              height: '280px'
          });
      });
    </script>

  </body>
</html>
