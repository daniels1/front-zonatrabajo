<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon-16x16.png">
    <link rel="manifest" href="../assets/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <meta name="msapplication-TileImage" content="../assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-bussines">
    <div class="login-box">
      <div class="login-logo" style="background-color: #fff;">
        <a href="../"><img width="173" src="../assets/images/logo_zona_dark_min.png"></a> <br> <small> <b> Empresas</b></small>  
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Introduce tus datos para abrir sesión</p>
        <form id="frmLogin" name="frmLogin">
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <input name="email" id="email" type="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input name="password" id="password" type="password" class="form-control" placeholder="Clave">
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="button" id="btnLogIn" name="btnLogIn" class="btn btn-primary btn-block btn-flat">Entrar</button>
            </div>
            <div class="col-xs-4 pull-right">
              <a class="btn btn-success btn-block btn-flat" href="personas"></span>Personas</span></a>
            </div><!-- /.col -->
          </div>
        </form>
        <p></p>
        <a href="nuevaempresa" class="text-center">Registrarme</a>
        <a href="recover" class="text-center pull-right">¿Recuperar contraseña?</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- Publicidad Tuniversia -->
    <!-- Daniel, este modulo de publicidad va incluido en las vistas de Registro y Personas. Hay que hacerle unos cambios al formulario de registro y de Iniciar Sesión pero eso lo hablaras con Manuel directamente.-->
    <!-- IMPORTANTE: Tuve que incluir el codigo css desde aquí porque desde style.css no lo reconoce y no comprendo por qué, y lo peor es que es solo esta página.-->
    <div class="publicidad-tuniversia" style="width: 60%; margin: 3%; float: left; position: relative; top: 360px;">
    <div class="col-xs-12 col-md-8">
      <div class="publicidad" style="background-color: white; background-repeat: no-repeat; padding: 20px; border-top: 0; color: #666; border-radius: 50px 0px 0px 0px;-moz-border-radius: 50px 0px 0px 0px;-webkit-border-radius: 50px 0px 0px 0px;border: 0px solid #000000;">
        <div class="row">
        <div class="col-sm-12">
          <img src="/assets/images/logo-tuniversia.png"> <small>EDUCACIÓN EN LÍNEA</small>

          <div class="row">
            <div class="publicidad-out" style="margin-top: 5px;">
              <div class="col-xs-8 col-sm-6">
              <h4 style="font-size: 22px;">Programas de formación <i>Online</i> que mejoran tu perfil laboral, reconocidos por las empresas.</h4>
              </div>
            <div class="col-xs-4 col-sm-6">
              <strong style="font-size: 20px;">Escuelas disponibles</strong>
              <ul class="list-inline">
                <li><img src="/assets/images/idiomas-icon.png">
                <p>Idiomas</p></li>
                <li><img src="/assets/images/tecnologia-icon.png">
                <p>Tecnología</p></li>
                <li><img src="/assets/images/gerencia-icon.png">
                <p>Negocios</p></li>
              </ul>
            </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>  
    </div>


    <? include('footer.php'); ?>
    <script src="app/empresas.js"></script>
  </body>
</html>
