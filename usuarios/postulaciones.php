<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/postulacionesController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="postulacionesController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Postulaciones            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="persona">Postulaciones</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>
          
           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div class="box-tools-list-candidates">
                    <div class="form-group">
                      <div class="input-group-list-offers">
                        <select name="offerStatus" class="form-control" ng-model="postulationStatus" placeholder="Estatus" ng-disabled="postulations.length == 0">
                          <option value="">Todas</option>
                          <option value="postulado">Postulado</option>
                          <option value="favorito">Favorito</option>
                          <option value="descartado">Descartado</option>
                        </select>
                        <input type="text" class="form-control" ng-model="postulationFilter" placeholder="Buscar" ng-disabled="postulations.length == 0">                        
                      </div>
                    </div>  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="alert alert-info alert-dismissable" ng-show="postulations.length == 0">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      En este momento no tienes postulaciones. Busca ofertas de empleo haciendo Click <a href="buscar_ofertas"><b>AQUÍ</b></a>
                  </div>


                  <div class="table-responsive no-padding">
                    <table class="table table-hover" ng-show="postulations.length > 0">
                      <thead>
                        <tr>
                          <th colspan="2">Empresa</th>
                          <th>Cargo</th>
                          <th>Fecha de Postulación</th>
                          <th>Estatus</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>  
                        <tr ng-repeat="postulation in postulations | filter: postulationFilter | filter: postulationStatus | orderBy: postulation.postulation.created_at" ng-show="postulation.offer.title">
                          <td width="10%" align="center">
                            <img style="width:60%" ng-if="postulation.offer.companies.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{postulation.offer.companies.logo}}" class="img-circle" alt="{{postulation.offer.companies.name}}">
                            <img style="width:60%" ng-if="!postulation.offer.companies.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{postulation.offer.companies.name}}">
                          </td>
                          <td width="20%">{{postulation.offer.companies.name}}</td>
                          <td width="20%">{{postulation.offer.title}}</td>
                          <td width="15%">{{postulation.postulation.created_at | amDateFormat:"DD/MM/YYYY"}}</td>
                          <td width="10%">
                            <span ng-show="postulation.postulation.status[postulation.postulation.status.length - 1].status == 'postulado'" class="label label-info">Postulado <i class="fa fa-check"></i></span>
                            <span ng-show="postulation.postulation.status[postulation.postulation.status.length - 1].status == 'favorito'" class="label label-success">Favorito <i class="fa fa-star"></i></span>
                            <span ng-show="postulation.postulation.status[postulation.postulation.status.length - 1].status == 'descartado'" class="label label-danger">Descartado <i class="fa fa-times-circle"></i></span>
                          </td>
                          <td align="center">
                            <a class="btn btn-xs btn-info" ng-click="viewDetail(postulation.offer.companies, 'company', 'detalle_empresa')"> <i class="fa fa-building"></i> Ver Empresa</a>
                            <a ng-disabled="postulation.postulation.status[postulation.postulation.status.length - 1].status == 'descartado'" class="btn btn-xs btn-success" ng-click="viewDetail(postulation.offer, 'offer', 'detalle_oferta')"> <i class="fa fa-users"></i> Ver Oferta</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

     <? include('copy.php');
         include('control_bar_persona.php'); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
