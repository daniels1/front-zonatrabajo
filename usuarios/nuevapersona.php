<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon-16x16.png">
    <link rel="manifest" href="../assets/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <meta name="msapplication-TileImage" content="../assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <!-- <link rel="stylesheet" href="../assets/css/style.css"> -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- CSS file -->
    <link rel="stylesheet" href="plugins/easyAutocomplete/easy-autocomplete.min.css"> 
    <link rel="stylesheet" href="plugins/easyAutocomplete/easy-autocomplete.themes.min.css"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-person">
    <div class="container">
    <div class="login-box" style="float: left;">
      <div class="login-logo">
        <a href="../"><img width="173" src="../assets/images/logo-zonatrabajo-grand-dark.png" alt="" > </a> <br> <small> <b> Personas</b></small>  
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Registrate como Nuevo Usuario</p>
        <form id="frmRegPerson" name="frmRegPerson">
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            <input id="first_name" name="first_name" type="text" class="form-control" placeholder="Nombres">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            <input id="last_name" name="last_name" type="text" class="form-control" placeholder="Apellidos">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
            <input id="profesion" name="profesion" type="text" class="form-control" placeholder="Profesión">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-thumbs-up form-control-feedback"></span>
            <input id="interest" name="interest" type="text" class="form-control" placeholder="Áreas de Interés">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            <input id="phone" name="phone" type="text" class="form-control" placeholder="Teléfono">
          </div>

          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-globe form-control-feedback"></span>
            <input id="country" name="country" type="text" class="form-control" placeholder="País">
          </div>

          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <input id="email" name="email" type="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input id="password" name="password" type="password" class="form-control" placeholder="Clave">
          </div>
          <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input id="repass" name="repass" type="password" class="form-control" placeholder="Repetir Clave">
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input id="terms" name="terms" type="checkbox"> Acepto los <a href="#">terminos</a>
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <input type="hidden" name="type_user" id="type_user" value="person">
              <button id="btnRegPerson" name="btnRegPerson" type="button" class="btn btn-primary btn-block btn-flat">Registrar</button>
            </div><!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          <p>- O -</p>
          <a target="_self" href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=77krhttq9amnl8&scope=r_basicprofile+r_emailaddress&state=57743c851c26d5.25098013&redirect_uri=https://api.zonatrabajo.com/linkedin/userdata/" class="btn btn-block btn-social btn-primary btn-flat"><i class="fa fa-linkedin"></i> Entrar con LinkedIn</a>
        </div><!-- /.social-auth-links -->

        <a href="entrar" class="text-center">Ya tengo Usuario</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->
    </div>

    <? include('footer.php'); ?>
    <script src="app/personas.js"></script>
  </body>
</html>
