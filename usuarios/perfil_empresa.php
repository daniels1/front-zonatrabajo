<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/perfilEmpresaController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="perfilEmpresaController" data-ng-init="cargaInicial()">

      <?
        include('top_empresa.php');
        include('menu_empresa.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Perfil de Empresa            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="perfil_empresa">Perfil de Empresa</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>

          <div class="row">
            <div class="col-md-5">

              <div class="box box-widget widget-user">
                
                <div class="pull-right no-padding">
                  <a class="btn btn-success btn-xs" ng-show="company.status == 'verified'" >
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-certificate fa-stack-2x"></i>
                          <i class="fa fa-check fa-stack-1x text-success"></i>
                        </span>
                        <b>Verificada.</b> 
                  </a>
                  <a class="btn btn-info btn-xs" ng-show="company.status == 'pending'" title="Su empresa esta en proceso de Verificación, una vez la información sea verificada procederemos a informarle y podrá usar todos nuestros servicios.">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-exclamation fa-stack-1x text-info"></i>
                        </span>
                        <b>Verificando.</b> 
                  </a>
                  <a href="#validate" data-toggle="tab" class="btn btn-warning btn-xs" ng-show="company.status == 'no_verified'" title="Su empresa No esta Verificada, dirijase a la pestaña de Validación y adjunte los documentos solicitados. Mientrás no suministre los documentos requeridos no podrá publicar ofertas ni contactar candidatos.">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-exclamation-triangle fa-stack-1x text-warning"></i>
                        </span>
                        <b>No Verificada.</b> 
                  </a>
                </div>                
                
                <div class="widget-user-header bg-light-blue">
                  <h3 class="widget-user-username">{{company.name}}</h3>
                  <h5 class="widget-user-desc">{{(company.slogan) ? company.slogan : '-----'}}</h5>
                </div>
                <div class="widget-user-image">
                  <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                  <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-12 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{(company.description) ? '¿Quienes Somos?' : ''}}</h5>
                        <span class="description-text">{{company.description}} <p><b>{{company.sector}}</b></p></span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->                    
                  </div><!-- /.row -->
                </div>
              </div>

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Perfil</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="col-sm-8 border-right">
                    <strong><i class="fa fa-briefcase margin-r-5"></i> Razón Social</strong>
                    <p class="text-muted">{{company.social_name}}</p>
                  </div>                
                  <div class="col-sm-4">
                    <strong><i class="fa fa-newspaper-o margin-r-5"></i> Nro. Registro</strong>
                    <p class="text-muted">{{company.nro_doc}}</p>
                  </div>                  
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Dirección</strong>
                    <p class="text-muted">{{company.address}} {{company.city}} {{(company.state) ? ', '+company.state : ''}} {{(company.country) ? ', '+company.country : ''}}</p>
                  </div>                  
                  
                  <div class="col-sm-6">
                    <strong><i class="fa fa-globe margin-r-5"></i> Sitio Web</strong>
                    <a ng-href="http://{{company.website}}">
                      <p>{{(company.website) ? company.website : ''}}</p>
                    </a>  
                  </div>                    

                  <div class="col-sm-8 border-right">
                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted">{{company.email}}</p>
                  </div>
                  <div class="col-sm-4">
                    <strong><i class="fa fa-phone margin-r-5"></i> Teléfono</strong>
                    <p class="text-muted">{{company.phone}}</p>
                  </div>                  
                  <div class="col-sm-12">
                    <h4>Redes Sociales</h4>
                    <hr>
                  </div>  
                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-facebook margin-r-5"></i> Facebook</strong>
                    <a ng-href="https://www.facebook.com/{{company.facebook}}">
                      <p>{{(company.facebook) ? 'facebook.com/'+company.facebook : ''}}</p>
                    </a>  
                  </div>  
                  <div class="col-sm-6">
                    <strong><i class="fa fa-twitter margin-r-5"></i> Twitter</strong>
                    <a ng-href="https://twitter.com/{{company.twitter}}">
                      <p>{{(company.twitter) ? '@'+company.twitter : ''}}</p>
                    </a>  
                  </div>  

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-instagram margin-r-5"></i> Instagram</strong>
                    <a ng-href="https://www.instagram.com/{{company.instagram}}">
                      <p>{{(company.instagram) ? '@'+company.instagram : ''}}</p>
                    </a>                        
                  </div>  
                  <div class="col-sm-6">
                    <strong><i class="fa fa-linkedin-square margin-r-5"></i> LinkedIn</strong>
                    <a ng-href="https://www.linkedin.com/company/{{company.linkedin}}">
                      <p>{{(company.linkedin) ? '@'+company.linkedin : ''}}</p>
                    </a>                        
                  </div>  


                  <div class="col-sm-12">
                  <h4>Persona de Contacto</h4>
                  <hr>
                    <strong><i class="fa fa-user margin-r-5"></i> Nombre Completo</strong>
                    <p class="text-muted">{{company.contact[0].contact_name}}</p>
                  </div>               
                  <div class="col-sm-12">
                    <strong><i class="fa fa-briefcase margin-r-5"></i> Cargo en la Empresa</strong>
                    <p class="text-muted">{{company.contact[0].position}}</p>
                  </div>               
                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted">{{company.contact[0].email}}</p>
                  </div>
                  <div class="col-sm-4">
                    <strong><i class="fa fa-phone margin-r-5"></i> Teléfono</strong>
                    <p class="text-muted">{{company.contact[0].phone}}</p>
                  </div>                  
                  <div class="col-sm-2">
                    <strong><i class="fa fa-phone margin-r-5"></i> Ext.</strong>
                    <p class="text-muted">{{company.contact[0].ext}}</p>
                  </div>                  

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-7">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li ng-class="{'active': validateStart}" class="active"><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Editar</a></li>
                  <li ng-show="company.status == 'no_verified'"><a href="#validate" data-toggle="tab"><span class="glyphicon glyphicon-thumbs-up"></span> Validación</a></li>
                </ul>
                <div class="tab-content">

                    <div ng-class="{'active': validateStart}" class="active tab-pane" id="profile">
                      <form class="form-horizontal" name="frmPerfilCompany" ng-submit="saveCompanyData()">

                        <button type="submit" class="btn btn-success pull-right" ng-disabled="!frmPerfilCompany.$valid"> <i class="fa fa-save"></i> Guardar Cambios</button><br><br>
                        
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="box box-primary box-solid">
                              <div class="box-header with-border">
                                <h3 class="box-title"> <i class="fa fa-briefcase"></i> Datos de la Empresa</h3>
                                <div class="box-tools pull-right">
                                  <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                                </div>
                              </div><!-- /.box-header -->
                            
                              <div class="box-body">

                                  <div class="col-sm-9">
                                    <div class="form-group">
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" ng-model="company.name" name="name" placeholder="Nombre de la Empresa" required>
                                        <span class="text-danger" ng-show="frmPerfilCompany.name.$touched && frmPerfilCompany.name.$invalid">Campo Obligatorio</span>
                                      </div>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" ng-model="company.social_name" name="social_name" placeholder="Razón Social" required>
                                        <span class="text-danger" ng-show="frmPerfilCompany.social_name.$touched && frmPerfilCompany.social_name.$invalid">Campo Obligatorio</span>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" ng-model="company.nro_doc" id="nro_doc" placeholder="Nro. de Registro" required="">
                                        <span class="text-danger" ng-show="frmPerfilCompany.nro_doc.$touched && frmPerfilCompany.nro_doc.$invalid">Campo Obligatorio</span>
                                      </div>
                                      <div class="col-sm-6">
                                        <input type="text" class="form-control input-sm" ng-model="company.slogan" id="slogan" placeholder="Slogan">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="col-sm-12">
                                        <textarea class="form-control input-sm" id="description" ng-model="company.description" placeholder="Escribe una reseña sobre tu empresa para que las personas la conozcan un poco mejor"></textarea>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="col-xs-12">
                                        <ui-select ng-model="$parent.company.sector">
                                          <ui-select-match class="ui-select-match" placeholder="Sector">{{$select.selected.description}}</ui-select-match>
                                          <ui-select-choices class="ui-select-choices" repeat="sector.description as sector in sectors | filter: $select.search" minimum-input-length="3">
                                            <div ng-bind-html="sector.description | highlight: $select.search"></div>
                                          </ui-select-choices>
                                        </ui-select>  
                                      </div>
                                    </div>  
                                  </div>

                                  <div class="col-sm-3">
                                      <div ngf-drop ngf-select ng-model="files" class="drop-box" ngf-drag-over-class="'dragover'" ngf-multiple="true" ngf-allow-dir="true"
                                            accept="image/*" ngf-pattern="'image/*'">Arrastra el logo aquí o haz click para actualizar</div>
                                      <div ngf-no-file-drop>Recomendamos Actualizar su navegador</div>                                  
                                  </div>


                              </div>
                              
                            </div>
                          </div>  
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="box box-primary box-solid collapsed-box">
                              <div class="box-header with-border">
                                <h3 class="box-title"> <i class="fa fa-building"></i> Datos de Ubicación y Contacto</h3>
                                <div class="box-tools pull-right">
                                  <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                                </div>
                              </div><!-- /.box-header -->
                            
                              <div class="box-body">
                                <div class="form-group">
                                  <div class="col-sm-12">
                                    <textarea class="form-control input-sm" ng-model="company.address" name="address" placeholder="Dirección Comercial" required></textarea>
                                    <span class="text-danger" ng-show="frmPerfilCompany.address.$touched && frmPerfilCompany.address.$invalid">Campo Obligatorio</span>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.company.country" on-select="fnStates($item)">
                                      <ui-select-match class="ui-select-match" placeholder="País">{{$select.selected.name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="country.name as country in listCountries | filter: $select.search">
                                      <div ng-bind-html="country.name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.company.state" on-select="fnCities($item)" ng-disabled="listStates.length == 0">
                                      <ui-select-match class="ui-select-match" placeholder="Estado">{{$select.selected.name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="est.name as est in listStates | filter: $select.search">
                                        <div ng-bind-html="est.name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>  
                                  </div>
                                  <div class="col-sm-4">
                                    <ui-select ng-model="$parent.company.city" ng-disabled="listCities.length == 0">
                                      <ui-select-match class="ui-select-match" placeholder="Ciudad">{{$select.selected.city_name}}</ui-select-match>
                                      <ui-select-choices class="ui-select-choices" repeat="city.city_name as city in listCities | filter: $select.search">
                                        <div ng-bind-html="city.city_name | highlight: $select.search"></div>
                                      </ui-select-choices>
                                    </ui-select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-8">                                  
                                    <div class="input-group">
                                      <span class="input-group-addon">www.</span>
                                      <input type="text" class="form-control input-sm" name="website" ng-model="company.website" placeholder="Sitio Web">
                                    </div>
                                  </div>
                                  <div class="col-sm-4">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                      <input type="text" class="form-control input-sm" id="phone" ng-model="company.phone" placeholder="Teléfono" mask="(9999) 999-9999" required>
                                      <span class="text-danger" ng-show="frmPerfilCompany.phone.$touched && frmPerfilCompany.phone.$invalid">Campo Obligatorio</span>
                                    </div>                                  
                                  </div>                                
                                </div>                  
                                <div class="form-group">
                                  <div class="col-sm-12">
                                    <h4>Redes Sociales</h4>
                                  </div>  
                                  <div class="col-sm-3">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                      <input type="text" class="form-control input-sm" name="facebook" ng-model="company.facebook" placeholder="Facebook">
                                    </div>
                                  </div>                                
                                  <div class="col-sm-3">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                      <input type="text" class="form-control input-sm" name="twitter" ng-model="company.twitter" placeholder="Twitter">
                                    </div>
                                  </div>
                                  <div class="col-sm-3">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                      <input type="text" class="form-control input-sm" name="instagram" ng-model="company.instagram" placeholder="Instagram">
                                    </div>
                                  </div>                                
                                  <div class="col-sm-3">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-linkedin-square"></i></span>
                                      <input type="text" class="form-control input-sm" name="linkedin" ng-model="company.linkedin" placeholder="LinkedIn">
                                    </div>
                                  </div>                                
                                </div>
                              </div>

                            </div>
                          </div>  
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="box box-primary box-solid collapsed-box">
                              <div class="box-header with-border">
                                <h3 class="box-title"> <i class="fa fa-user"></i> Persona de Contacto</h3>
                                <div class="box-tools pull-right">
                                  <a class="btn btn-default btn-xs" data-widget="collapse"><i class="fa fa-pencil"></i> Editar</a>
                                </div>
                              </div><!-- /.box-header -->
                            
                              <div class="box-body">
                                <div class="form-group">
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" ng-model="company.contact[0].contact_name" name="contact_name" placeholder="Nombre Completo" required>
                                    <span class="text-danger" ng-show="frmPerfilCompany.contact_name.$touched && frmPerfilCompany.contact_name.$invalid">Campo Obligatorio</span>
                                  </div>                                  
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-12">
                                    <input type="text" class="form-control input-sm" ng-model="company.contact[0].position" name="contact_position" placeholder="Cargo en la Empresa" required>
                                    <span class="text-danger" ng-show="frmPerfilCompany.contact_position.$touched && frmPerfilCompany.contact_position.$invalid">Campo Obligatorio</span>
                                  </div>                                
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-6">
                                    <input type="email" class="form-control input-sm" ng-model="company.contact[0].email" name="contact_email" placeholder="Email">
                                  </div>
                                  <div class="col-sm-3">
                                    <input type="text" class="form-control input-sm" ng-model="company.contact[0].phone" name="contact_phone" placeholder="Teléfono" mask="(9999) 999-9999" required>
                                    <span class="text-danger" ng-show="frmPerfilCompany.contact_phone.$touched && frmPerfilCompany.contact_phone.$invalid">Campo Obligatorio</span>
                                  </div>
                                  <div class="col-sm-3">
                                    <input type="text" class="form-control input-sm" id="mobile" ng-model="company.contact[0].ext" placeholder="Extensión">
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>  
                        </div>


                      </form>
                    </div><!-- /.tab-pane -->
                  
                    <div class="tab-pane" id="validate" ng-show="company.status == 'no_verified'">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="box box-primary box-solid">
                              <div class="box-header with-border">
                                <h3 class="box-title"> <i class="fa fa-briefcase"></i> Validación de Datos de la Empresa</h3>                                
                              </div><!-- /.box-header -->
                            
                              <div class="box-body">
                                  <div class="col-sm-12">
                                    <h4>En <a href="https://zonatrabajo.com">Zonatrabajo.com</a> nos tomamos muy en serio la seguridad de nuestros usuarios, es por eso que para poder aceptarte como empresa requerimos adjuntes una imagen escaneada del documento donde se muestre la información de registro de tu empresa ante los entes gubernamentales de tu país (RIF, RUT, Registro Fiscal). El proceso es bastante sencillo y será realizado solo una vez con la intención de validarte como una empresa y poder generar una mayor confianza en los usuarios que se postulan a tus ofertas de empleo. </h4>
                                  </div>

                                  <div class="col-sm-12">
                                      <div ngf-drop ngf-select ng-model="docs" class="drop-box" ngf-drag-over-class="'dragover'" ngf-multiple="true" ngf-allow-dir="true"
                                            accept="image/*" ngf-pattern="'image/*'">Arrastra la imagen aquí o haz click para actualizar</div>
                                      <div ngf-no-file-drop>Recomendamos Actualizar su navegador</div>                                  
                                  </div>

                              </div>
                              
                            </div>
                          </div>  
                        </div>

                    </div>

                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <? include('copy.php');
         include('control_bar_empresa.php'); ?>
    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>
