      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel" ng-cloak>
            <div class="pull-left image">
              <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
              <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
            </div>
            <div class="pull-left info">
              <p>{{company.name}}</p>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Menú</li>

            <li>
              <a href="empresa">
                <i class="fa fa-home"></i> <span>Resumen</span>
              </a>
            </li>
            <li>
              <a href="perfil_empresa">
                <i class="fa fa-briefcase"></i> <span>Perfil de Empresa</span>
              </a>
            </li>
            <li>
              <a href="ofertas">
                <i class="fa fa-file-text"></i> <span>Ofertas</span>
              </a>
            </li>
            <li>
              <a href="candidatos">
                <i class="fa fa-user"></i> <span>Candidatos</span>
              </a>
            </li>
            <li>
              <a href="buscar_personas">
                <i class="fa fa-search"></i> <span>Buscar</span>
              </a>
            </li>
            <li>
              <a href="precios">
                <i class="fa fa-money"></i> <span>Contratar</span>
              </a>
            </li>
            <li>
              <a data-ng-click="logout()">
                <i class="fa fa-sign-out"></i> <span>Salir</span>
              </a>
            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>