<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/detalleEmpresaController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="detalleEmpresaController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            404 Error
          </h1>
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li class="active">404 error</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Oops! Página no encontrada.</h3>
              <p>
                No sabemos como has llegado aquí, sin embargo te aseguramos que no es un lugar donde quieras estar
                tal vez quieras regresar al <a href="persona">Resumen</a>.
              </p>
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     <? include('copy.php');
         include('control_bar_persona.php'); ?>
    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>