<header class="main-header">

        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="../assets/images/favicon-32x32.png"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="../assets/images/favicon-32x32.png"> <b>Zona</b>Trabajo.com</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

          <div class="cursos-container">
            <a href="http://tuniversia.com/" target="_blank" class="cursos">
              <i class="fa fa-desktop"></i> Educación Online - Tuniversia.com
            </a>
          </div>
          
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <li class="dropdown notifications-menu">
                <a href="empresa" title="Resumen">
                  <i class="fa fa-home"></i>
                </a>
              </li>

              <li class="dropdown notifications-menu">
                <a href="perfil_empresa" title="Perfil de Empresa">
                  <i class="fa fa-briefcase"></i>
                </a>
              </li>

              <li class="dropdown notifications-menu">
                <a href="ofertas" title="Ofertas">
                  <i class="fa fa-list-ul"></i>
                </a>
              </li>

              <li class="dropdown notifications-menu">
                <a href="candidatos" title="Candidatos">
                  <i class="fa fa-user"></i>
                </a>
              </li>

              <li class="dropdown notifications-menu">
                <a href="buscar_personas" title="Candidatos">
                    <i class="fa fa-search"></i>
                </a>
              </li>

              <li class="dropdown notifications-menu">
                <a href="precios" title="Contratar">
                    <i class="fa fa-money"></i>
                </a>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu" ng-cloak>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                  <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                  <span class="hidden-xs">{{company.name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                  <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                  <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                    <p>
                      {{company.name}}  {{company.email}}
                      <small>Miembro desde {{company.member_since | date : "MMMM yyyy"}}</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="perfil_empresa" class="btn btn-default btn-flat">Perfil</a>
                    </div>
                    <div class="pull-right">
                      <a data-ng-click="logout()" class="btn btn-default btn-flat">Salir</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-calendar"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>