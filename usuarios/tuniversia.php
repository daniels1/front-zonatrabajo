<div class="publicidad-tuniversia" style="width: 41%; float: left; position: relative; top: 440px;margin-left: -80px;margin-top: -50px;">
  <div class="col-xs-12">
    <div class="publicidad" style="background-color: white; background-repeat: no-repeat; padding: 20px; border-top: 0; color: #666; border-radius: 0px 50px 0px 0px;-moz-border-radius: 0px 50px 0px 0px;-webkit-border-radius: 50px 0px 0px 0px;border: 0px solid #000000;">
      <div class="row">
      <div class="col-sm-12">
        <a href="https://tuniversia.com/" target="_blank">
          <img src="/assets/images/logo-tuniversia.png" width="50%"> <small>EDUCACIÓN EN LÍNEA</small>

          <div class="row">
            <div class="publicidad-out" style="margin-top: 5px;">
              <div class="col-xs-6 col-sm-5">
              <h4>Programas de formación <i>Online</i> que mejoran tu perfil laboral, reconocidos por las empresas.</h4>
              </div>
            <div class="col-xs-6 col-sm-7">
              <strong>Escuelas disponibles</strong>
              <ul class="list-inline">
                <li><img class="center-block" src="/assets/images/idiomas-icon.png" width="36px">
                <p>Idiomas</p></li>
                <li><img class="center-block" src="/assets/images/tecnologia-icon.png" width="36px">
                <p>Tecnología</p></li>
                <li><img class="center-block" src="/assets/images/gerencia-icon.png" width="36px">
                <p>Negocios</p></li>
              </ul>
            </div>
            </div>
          </div>
        </a>
      </div>
    </div>
    </div>
  </div>  
</div>