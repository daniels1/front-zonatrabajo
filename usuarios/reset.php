<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon-16x16.png">
    <link rel="manifest" href="../assets/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <meta name="msapplication-TileImage" content="../assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-person">

      <div class="container">
        <div class="login-box">
          <div class="login-logo" style="background-color: #fff;">
            <a href="../"><img width="173" src="../assets/images/logo_zona_dark_min.png" alt="" > </a>
          </div><!-- /.login-logo -->
          <div class="login-box-body">
            <p class="login-box-msg">Introduce tu nueva contraseña</p>
            <form id="frmReset" name="frmReset">
              <div class="form-group has-feedback">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <input name="password" id="password" type="password" class="form-control" placeholder="Nueva Contraseña">
              </div>
              <div class="form-group has-feedback">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                <input name="password_confirmation" id="password_confirmation" type="password" class="form-control" placeholder="Nueva Contraseña">
              </div>
              <input type="hidden" name="email" value="<?=$_GET['email']?>">
              <input type="hidden" name="token" value="<?=$_GET['token']?>">
              <div class="row">
                <div class="col-xs-12">
                  <button type="button" id="btnReset" name="btnReset" class="btn btn-primary btn-block btn-flat">Recuperar</button>
                </div>            
              </div>
            </form>

          </div><!-- /.login-box-body -->
        </div>

        <? include ('tuniversia.php'); ?>
      </div>  
    
    <? include('footer.php'); ?>
    <script src="app/recover.js"></script>

  </body>
</html>
