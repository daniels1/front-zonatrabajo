<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/detalleEmpresaController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="detalleEmpresaController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detalle de Empresa            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="perfil_persona">Detalle de Empresa</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>

          <div class="row">
            <div class="col-md-5">

              <div class="box box-widget widget-user">
                
                <div class="pull-right no-padding">
                  <a class="btn btn-success btn-xs" ng-show="company.status == 'verified'" >
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-certificate fa-stack-2x"></i>
                          <i class="fa fa-check fa-stack-1x text-success"></i>
                        </span>
                        <b>Verificada.</b> 
                  </a>
                  <a class="btn btn-info btn-xs" ng-show="company.status == 'pending'">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-exclamation fa-stack-1x text-info"></i>
                        </span>
                        <b>Verificando.</b> 
                  </a>
                  <a href="#validate" data-toggle="tab" class="btn btn-warning btn-xs" ng-show="company.status == 'no_verified'">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-exclamation-triangle fa-stack-1x text-warning"></i>
                        </span>
                        <b>No Verificada.</b> 
                  </a>
                </div>                
                
                <div class="widget-user-header bg-light-blue">
                  <h3 class="widget-user-username">{{company.name}}</h3>
                  <h5 class="widget-user-desc">{{(company.slogan) ? company.slogan : '-----'}}</h5>
                </div>
                <div class="widget-user-image">
                  <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                  <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                </div>
                <div class="box-footer">
                  <div class="row">
                    <div class="col-sm-12 border-right">
                      <div class="description-block">
                        <h5 class="description-header">{{(company.description) ? '¿Quienes Somos?' : ''}}</h5>
                        <span class="description-text">{{company.description}} <p><b>{{company.sector}}</b></p></span>
                      </div><!-- /.description-block -->
                    </div><!-- /.col -->                    
                  </div><!-- /.row -->
                </div>
              </div>

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Perfil</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="col-sm-8 border-right">
                    <strong><i class="fa fa-briefcase margin-r-5"></i> Razón Social</strong>
                    <p class="text-muted">{{company.social_name}}</p>
                  </div>                
                  <div class="col-sm-4">
                    <strong><i class="fa fa-newspaper-o margin-r-5"></i> Nro. Registro</strong>
                    <p class="text-muted">{{company.nro_doc}}</p>
                  </div>                  
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Dirección</strong>
                    <p class="text-muted">{{company.address}} {{company.city}} {{(company.state) ? ', '+company.state : ''}} {{(company.country) ? ', '+company.country : ''}}</p>
                  </div>                  
                  
                  <div class="col-sm-6">
                    <strong><i class="fa fa-globe margin-r-5"></i> Sitio Web</strong>
                    <a ng-href="http://{{company.website}}">
                      <p>{{(company.website) ? 'www.'+company.website : ''}}</p>
                    </a>  
                  </div>                    

                  <div class="col-sm-12">
                    <h4>Redes Sociales</h4>
                    <hr>
                  </div>  
                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-facebook margin-r-5"></i> Facebook</strong>
                    <a ng-href="https://www.facebook.com/{{company.facebook}}">
                      <p>{{(company.facebook) ? 'facebook.com/'+company.facebook : ''}}</p>
                    </a>  
                  </div>  
                  <div class="col-sm-6">
                    <strong><i class="fa fa-twitter margin-r-5"></i> Twitter</strong>
                    <a ng-href="https://twitter.com/{{company.twitter}}">
                      <p>{{(company.twitter) ? '@'+company.twitter : ''}}</p>
                    </a>  
                  </div>  

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-instagram margin-r-5"></i> Instagram</strong>
                    <a ng-href="https://www.instagram.com/{{company.instagram}}">                    
                      <p>{{(company.instagram) ? '@'+company.instagram : ''}}</p>
                    </a>                        
                  </div>  
                  <div class="col-sm-6">
                    <strong><i class="fa fa-linkedin-square margin-r-5"></i> LinkedIn</strong>
                    <a ng-href="https://www.linkedin.com/company/{{company.linkedin}}">                    
                      <p>{{(company.linkedin) ? '@'+company.linkedin : ''}}</p>
                    </a>                        
                  </div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

            <div class="col-md-7">
              <div class="nav-tabs-custom">
                <div class="tab-content">
                  <div class="active tab-pane" id="ofertas">

                    <perfect-scrollbar class="scroller" wheel-propagation="true" wheel-speed="3" min-scrollbar-length="20">
                      <ul class="timeline timeline-inverse">
                        <!-- timeline time label -->
                        <li class="time-label">
                          <span class="bg-blue">
                            Ofertas de Empleo Disponibles en {{company.name}}
                          </span>
                        </li>
                        <!-- /.timeline-label -->
                        
                        <li ng-show="ofertas.length == 0">
                          <i class="fa fa-warning"></i>
                          <div class="timeline-item">
                            <h3 class="timeline-header">Aviso</h3>
                            <div class="timeline-body">
                              La empresa no tiene ofertas de empleo disponibles.
                            </div>  
                          </div>
                        </li>

                        <!-- timeline item -->
                        <li class="ofertas" ng-repeat="offer in ofertas | orderBy: offer.create_at" ng-class="{'hidden': offer.status != 'active'}" ng-click="viewDetail(offer, 'offer', 'detalle_oferta')">
                          <i class="fa fa-briefcase bg-blue"></i>
                          <div class="timeline-item">
                          <span class="time"><i class="fa fa-calendar"></i> {{offer.created_at.date | amDateFormat:"DD/MM/YYYY"}}</span>
                          <h3 class="timeline-header"><a href="#">{{offer.title}}</a> {{offer.city}}-{{offer.state}} </h3>
                          <div class="timeline-body">
                            {{offer.description | limitTo: 100}}
                          </div>
                          <div class="timeline-footer">
                            <span class="label label-primary"><b> <i class="fa fa-sitemap"></i> {{offer.area}}</b></span>
                          </div>
                        </div>
                        </li>
                        <!-- END timeline item -->
                        

                        <li>
                          <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                      </ul>
                    </perfect-scrollbar>  
                  </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->

          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <? include('copy.php');
         include('control_bar_persona.php'); ?>
    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>
