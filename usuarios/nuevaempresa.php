<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon-16x16.png">
    <link rel="manifest" href="../assets/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <meta name="msapplication-TileImage" content="../assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-bussines">
  
    <div class="nav-registro container">
      <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="col-xs-12 col-sm-4">
          <div class="navbar-header">
          <a href="/">
            <img class="center-block" width="173" src="../assets/images/logo-zonatrabajo-grand-dark.png" alt="">
          </a>
        </div>
        </div>
        <div class="col-xs-12 col-sm-8">
          <div class="heading heading-v1">
            <h3 class="text-center">Registro de Empresas</h3>
            <h5 class="text-justify">Ingrese la información para crear su <strong>Cuenta Empresarial</strong></h5>
          </div>
        </div>
      </div>
      </nav>
    </div>
    <div class="content">
      <div id="error"></div>
      <div class="container content">
        <form id="frmRegCompany" class="sky-form">
          <input type="hidden" id="empresas.impulsada" name="readonly">
          <header>Datos de la Empresas</header>
          <fieldset>
            <div class="row">
              <section class="col col-xs-4">
                <label class="label">
                  Nombre de la Empresa
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-building"></i>
                  <input id="company_name" name="company_name" type="text" class="required" placeholder="Especifique un nombre con al menos 3 caracteres" maxlength="50">
                </label>
              </section>
              <section class="col col-xs-4">
                <label class="label">
                  Razón Social
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-building"></i>
                  <input id="social_name" name="social_name" type="text" class="required" placeholder="Especifique la razón social" maxlength="100">
                </label>
              </section>
              <section class="col col-xs-4">
                <label class="label">
                  RIF/RUT
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-asterisk"></i>
                  <input id="nro_doc" name="nro_doc" type="text" class="form-control" placeholder="Número de registro fiscal." maxlength="45">
                </label>
              </section>  
            </div>
            <div class="row">
              <section class="col col-4">
                <label class="label">
                  Dirección Fiscal
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-map-marker"></i>
                  <input id="addressnro_doc" name="address" type="text" class="form-control" placeholder="Indique la dirección fiscal" maxlength="45">
                </label>
              </section>
              <section class="col col-4">
                <label class="label">
                  Telefóno
                  <small class="opcional">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-phone"></i>
                  <input id="phone" type="text" name="phone" class="form-control" placeholder="Número de Telefóno" maxlength="45">
                </label>
              </section>                
              <section class="col col-4">
                <label class="label">
                  Sitio Web
                  <small class="opcional">(opcional)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-globe"></i>
                  <input id="website" type="text" name="website" class="form-control" placeholder="Nombre del Sitio Web." maxlength="45">
                </label>
              </section>
            </div>
            <div class="row">
              <section class="col col-4">
                <label class="label">
                  Tipo de propiedad
                  <small class="opcional">(opcional)</small>
                </label>
                <label class="select">
                  <i class="icon-append fa fa-industry"></i>
                  <select id="capital" name="capital" class="text" placeholder="Indique si su empresa es pública, privada o mixta. (opcional9)">
                    <option selected="selected">Seleccione...</option>
                    <option value="Gobierno">Gobierno</option>
                    <option value="Privado">Privado</option>
                    <option value="Mixto">Mixto</option>
                  </select>
                </label>
              </section>
              <section class="col col-4">
                <label class="label">
                  Industria
                  <small class="requerido">(opcional)</small>
                </label>
                <label class="select">
                  <i class="icon-append fa fa-asterisk"></i>
                  <select id="sector" name="sector" class="text" placeholder="Seleccione una industria">
                    <option>Seleccione...</option>
                    <option value="Aeronaves/Astilleros">Aeronaves/Astilleros</option>
                    <option value="Agrícola/Ganadería">Agrícola/Ganadería</option>
                    <option value="Agroindustria">Agroindustria</option>
                    <option value="Aguas/Obras Sanitarias">Aguas/Obras Sanitarias</option>
                    <option value="Arquitectura/Diseño/Decoración">Arquitectura/Diseño/Decoración</option>
                    <option value="Automotriz">Automotriz</option>
                    <option value="Banca y Servicios Financieros">Banca y Servicios Financieros</option>
                    <option value="Energía (Gas, Petróleo, Electricidad)">Energía (Gas, Petróleo, Electricidad)</option>
                    <option value="Comercio Mayoristas">Comercio Mayoristas</option>
                    <option value="Comercio Minorista">Comercio Minorista</option>
                    <option value="Construcción">Construcción</option>
                    <option value="Consultoría/Asesoría">Consultoría/Asesoría</option>
                    <option value="Consumo Masivo">Consumo Masivo</option>
                    <option value="Defensa">Defensa</option>
                    <option value="Educación/Capacitación">Educación/Capacitación</option>
                    <option value="Entretenimiento">Entretenimiento</option>
                    <option value="Exportación/Importación">Exportación/Importación</option>
                    <option value="Farmacéutica">Farmacéutica</option>
                    <option value="Forestal/Papel/Celulosa">Forestal/Papel/Celulosa</option>
                    <option value="Gobierno">Gobierno</option>
                    <option value="Hotelería/Turismo/Restaurantes">Hotelería/Turismo/Restaurantes</option>
                    <option value="Informática/IT">Informática/IT</option>
                    <option value="Imprenta/Editoriales">Imprenta/Editoriales</option>
                    <option value="Inmobiliarias">Inmobiliarias</option>
                    <option value="Internet">Internet</option>
                    <option value="Inversiones (Sociedades/Holdings)">Inversiones (Sociedades/Holdings)</option>
                    <option value="Jurídica">Jurídica</option>
                    <option value="Logística/Distribución">Logística/Distribución</option>
                    <option value="Manufactura">Manufactura</option>
                    <option value="Materiales de Construcción">Materiales de Construcción</option>
                    <option value="Medios de Comunicación">Medios de Comunicación</option>
                    <option value="Metalmecánica y Siderurgia">Metalmecánica y Siderurgia</option>
                    <option value="Menería">Menería</option>
                    <option value="Navieras">Navieras</option>
                    <option value="Organicaciones sin fines de lucro">Organicaciones sin fines de lucro</option>
                    <option value="Otra">Otra</option>
                    <option value="Pesquera/Cultivos Marinos">Pesquera/Cultivos Marinos</option>
                    <option value="Publicidad/Marketing/RRPP">Publicidad/Marketing/RRPP</option>
                    <option value="Química">Química</option>
                    <option value="Salud">Salud</option>
                    <option value="Seguros/Previsión">Seguros/Previsión</option>
                    <option value="Servicios Varios">Servicios Varios</option>
                    <option value="Telecomunicaciones">Telecomunicaciones</option>
                    <option value="Textil/Confecciones">Textil/Confecciones</option>
                    <option value="Transporte">Transporte</option>
                    <option value="Recursos Humanos">Recursos Humanos</option>
                  </select>                  
                </label>
              </section>                          
              <section class="col col-4">
                <label class="label">
                  Nro. Empleados
                  <small class="opcional">(opcional)</small>
                </label>
                <label class="select">
                  <i class="icon-append fa fa-asterisk"></i>
                  <select id="num_employees" name="num_employees" class="text" placeholder="Indique un rango para el número de empleados. (opcional)">
                    <option selected="selected">Seleccione...</option>
                    <option value="1-10">1-10</option>
                    <option value="11-50">11-50</option>
                    <option value="51-100">51-100</option>
                    <option value="101-500">101-500</option>
                    <option value="501-1000">501-1000</option>
                    <option value="Más de 1000">Más de 1000</option>
                  </select>
                </label>
              </section>
            </div>
          </fieldset>
          <header>Datos de Acceso</header>
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">
                  Email
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-envelope"></i>
                  <input id="email" type="text" name="email" class="text required email" placeholder="Indique el correo electrónico de acceso (requerido)" maxlength="50">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">
                  Clave
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-lock"></i>
                  <input id="password" type="password" name="password" class="text required" minlength="5" placeholder="Indique una clave de al menos 5 caracteres. (requerido)" maxlength="15">
                </label>
              </section>
            </div>
            
          </fieldset>
          <header>Persona de Contacto</header>
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">
                  Nombre
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-user"></i>
                  <input id="contact_name" type="text" name="contact_name" class="text required" minlength="6" placeholder="Especifique un nombre de contácto. (requerido)" maxlength="45">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">
                  Cargo
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-briefcase"></i>
                  <input id="position" type="text" name="position" class="text required" minlength="6" placeholder="Indique el cargo del contácto. (requerido)" maxlength="45">
                </label>
              </section>
            </div>  
            <div class="row">
              <section class="col col-6">
                <label class="label">
                  Email
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-envelope"></i>
                  <input id="contact_email" type="text" name="contact_email" class="text required email" placeholder="Indique el correo electrónico de contácto. (requerido)" maxlength="50">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">
                  Telefóno
                  <small class="opcional">(requerido)</small>
                </label>
                <label class="input">
                  <i class="icon-prepend fa fa-phone"></i>
                  <input id="contact_phone" type="text" name="contact_phone" class="form-control" placeholder="Número de Telefóno" maxlength="45">
                </label>
              </section>                
            </div>
            
          </fieldset>
          <header>Dirección de la Empresa</header>
          <fieldset>
            <div class="row">
              <section class="col col-4">
                <label class="label">
                  País
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="select">
                  <i class="icon-append fa fa-asterisk"></i>
                  <select disabled="disabled" id="country" name="country" class="text required no-map">
                    <option selected="selected">Seleccione...</option>                    
                  </select>                  
                </label>
              </section>
              <section class="col col-4">
                <label class="label">
                  Provincia/Estado/Dpto.
                  <small class="requerido">(requerido)</small>
                </label>
                <label class="select">
                  <i class="icon-append fa fa-asterisk"></i>
                  <select disabled="disabled" id="state" name="state" class="text">
                    <option selected="selected">Seleccione...</option>                    
                  </select>                  
                  <label id="stateLoading" style="display: none;"> <i class="fa fa-spinner fa-spin"></i> Cargando...</label>
                </label>
              </section>
              <section class="col col-4">
                <label class="label">
                  Ciudad
                  <small class="opcional">(opcional)</small>
                </label>
                <label class="select">
                  <i class="icon-append fa fa-asterisk"></i>
                  <select disabled="disabled" id="city" name="city" class="form-control text">
                    <option selected="selected">Seleccione...</option>
                  </select>
                  <label id="citiesLoading" style="display: none;"> <i class="fa fa-spinner fa-spin"></i> Cargando...</label>
                </label>
              </section>
              
            </div>
          </fieldset>
          <fieldset>
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="checkbox icheck">
                  <label>
                    <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="terms" name="terms" type="checkbox" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> Acepto los <a href="#">terminos y condiciones</a> de uso de <a href="https://zonatrajo.com">Zonatrabajo.com</a>
                  </label>
                </div>
              </div><!-- /.col -->
              <div class="col-xs-12 col-sm-4 col-sm-offset-2">
                <input type="hidden" name="type_user" id="type_user" value="company">
                <a id="btnRegCompany" name="btnRegCompany" class="btn btn-primary btn-block btn-flat">Registrar <i class="fa fa-briefcase"></i></a>
              </div><!-- /.col -->
            </div>
          </fieldset>  



          <!-- <fieldset>
            <div class="row">
              <div class="col col-6">
                <label class="label">&nbsp</label>
                <label class="checkbox">
                  <input id="empresas_acepta" name="empresas[acepta]" value="si">
                  <i></i>
                  <a href="...">Términos y Condiciones Legales de nuestro sitio web</a>
                </label>
              </div>
            </div>
          </fieldset>
          <footer>
            <div align="right">
              <input type="hidden" id="path" value="/venezuela">
              <button class="btn-u btn-primary" type="submit" value="Guardar">Registrar Empresas</button>
            </div>
          </footer> -->
        </form>
      </div>
    </div>


    <? include('footer.php'); ?>
    <script src="app/empresas.js"></script>
  </body>
</html>
