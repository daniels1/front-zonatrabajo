<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
      <?
        include('header.php');
      ?>
    <script src="app/controllers/candidatosController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="candidatosController" data-ng-init="cargaInicial()">

      <? include("top_empresa.php"); ?>
      <? include("menu_empresa.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Candidatos
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="ofertas">Candidatos</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>
          
              <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div class="box-tools-list-candidates">
                    <div class="form-group">
                      <div class="input-group-list-offers">
                        <select name="offerStatus" class="form-control" ng-model="offerTitle" ng-disabled="ofertas.length == 0">
                          <option value="">Todas</option>
                          <option ng-repeat="offer in ofertas" value="{{offer._id}}">{{offer.title}}</option>
                        </select>
                        <input type="text" class="form-control" ng-model="candidatesFilter" placeholder="Buscar" ng-disabled="ofertas.length == 0">                        
                      </div>
                    </div>  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="alert alert-info alert-dismissable"  ng-show="company.status == 'pending'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      Su empresa esta en proceso de Verificación, una vez la información sea verificada procederemos a informarle y podrá usar todos nuestros servicios.
                  </div>

                  <div class="alert alert-warning alert-dismissable"  ng-show="company.status == 'no_verified'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-exclamation-triangle"></i> Aviso</h4>
                      Su empresa <b>NO ESTA VERIFICADA</b>, dirijase a <a href="perfil_empresa"><b>Perfil de Empresa</b></a>, en la pestaña de Validación y adjunte los documentos solicitados. Mientrás no suministre los documentos requeridos no podrá publicar ofertas ni contactar candidatos.
                  </div>

                  <div class="alert alert-info alert-dismissable" ng-show="ofertas.length == 0 && company.status == 'verified'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      En este momento no tienes ofertas cargadas en nuestro sistema. Crea una Oferta haciendo Click <a href="nueva_oferta"><b>AQUÍ</b></a>
                  </div>

                    <div class="post" id="{{offer._id}}" class="table table-hover" ng-repeat="offer in ofertas | filter: offerTitle">
                      <div class="user-block">
                        <span class="username">
                          <a>{{offer.title}}</a>
                        </span>
                        <span class="description">Publicada el: {{offer.created_at.date | amDateFormat:"DD/MM/YYYY"}}</span>
                      </div><!-- /.user-block -->
                      <div class="alert alert-warning alert-dismissable" ng-show="offer.candidates.length == 0">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          No hay candidatos postulados para esta oferta.
                      </div>
                      <div masonry>
                            <div class="masonry-brick" ng-repeat="candidate in offer.candidates" ng-if="candidate.postulation.status[candidate.postulation.status.length - 1].status != 'descartado'">
                              <!-- Widget: user widget style 1 -->
                              <div class="box-candidates box-widget widget-user">
                                <div class="pull-right no-padding" ng-show="candidate.postulation.status[candidate.postulation.status.length - 1].status == 'favorito'">
                                  <a class="btn btn-success btn-xs"><i class="fa fa-star"></i> Favorito</a>
                                </div>

                                <div class="widget-user-header bg-aqua-active">
                                  <h4>{{candidate.person.first_name}} {{candidate.person.last_name}}</h4>
                                  <h5>{{candidate.person.title}}</h5>
                                </div>
                                <div class="widget-user-image">
                                  <img style="width:90px" data-ng-if="candidate.user.avatar" class="img-circle" data-ng-src="https://api.zonatrabajo.com/profile/{{candidate.user.avatar}}" alt="User Avatar">
                                    <img style="width:90px" data-ng-if="!candidate.user.avatar" data-ng-src="dist/img/avatar.png" class="img-circle" alt="{{candidate.person.first_name}} {{candidate.person.last_name}}">
                                </div>
                                <div class="box-footer">
                                  <div class="col-sm-12">
                                    <div class="description-block text-center">
                                      <h5 class="description-header ng-binding">Postulado el: {{candidate.postulation.status[candidate.postulation.status.length - 1].date_status | amDateFormat:"DD/MM/YYYY"}}</h5>
                                    </div><!-- /.description-block -->
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-3">
                                      <div class="description-block">
                                        <span class="description-text"><a class="btn btn-xs btn-success" ng-click="markCandidate(candidate.postulation.offer_id, candidate.postulation.person_id, 'favorito')"> <i class="fa fa-star"></i> Favorito</a></span>
                                      </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                    <div class="col-sm-3">
                                      <div class="description-block">
                                        <span class="description-text"><a class="btn btn-xs btn-danger" ng-click="markCandidate(candidate.postulation.offer_id, candidate.postulation.person_id, 'descartado')"> <i class="fa fa-times-circle"></i> Descartar</a></span>
                                      </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                    <div class="col-sm-3">
                                      <div class="description-block">
                                        <span class="description-text"><a class="btn btn-xs btn-info" ng-click="viewDetail(candidate, 'person', 'detalle_persona')"> <i class="fa fa-users"></i> Ver Perfil</a></span>
                                      </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                    <div class="col-sm-3">
                                      <div class="description-block">
                                        <span class="description-text"><a target="_blank" ng-if="candidate.person.cv" ng-href="https://zonatrabajo.com/hojasdevida/{{candidate.person.cv}}" class="btn btn-xs btn-warning"> <i class="fa fa-file-pdf-o"></i> Ver CV</a></span>
                                      </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                  </div><!-- /.row -->
                                </div>
                              </div><!-- /.widget-user -->
                            </div>
                      </div><!-- /.row -->                      
                    </div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include("copy.php"); ?>
      <? include("control_bar_empresa.php"); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
