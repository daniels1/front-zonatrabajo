    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon-16x16.png">
    <link rel="manifest" href="../assets/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <link rel="shortcut icon" href="../assets/images/favicon.ico">
    <meta name="msapplication-TileImage" content="../assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    
    <link rel="stylesheet" href="dist/angular/angular-ui-select/select.css">
    <link rel="stylesheet" href="dist/angular/ng-alertify/ng-alertify.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="dist/angular/angular-perfect-scrollbar/perfect-scrollbar.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="dist/angular/angular-icheck/angular-icheck.css">
    <link rel="stylesheet" href="dist/angular/ng-file-upload/ng-file-upload.css">

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <script src="dist/js/lodash.min.js"></script>    
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/i18n/angular-locale_es-ve.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular-sanitize.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ngMask/3.1.1/ngMask.min.js"></script>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.4/select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.1.3/ui-bootstrap-tpls.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-moment/0.10.3/angular-moment.min.js"></script>

    <script src="dist/angular/angular-perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="dist/angular/angular-perfect-scrollbar/angular-perfect-scrollbar.js"></script>
    <script src="dist/angular/ng-file-upload/ng-file-upload.js"></script>
    <script src="dist/angular/ng-file-upload/ng-file-upload-shim.js"></script>
    <script src="dist/angular/angular-icheck/angular-icheck.js"></script>
    <script src="dist/angular/ng-alertify/ng-alertify.js"></script>

    <!-- masonry -->
    <script src="dist/angular/masonry/jquery-bridget/jquery-bridget.js"></script>
    <script src="dist/angular/masonry/ev-emitter/ev-emitter.js"></script>
    <script src="dist/angular/masonry/desandro-matches-selector/matches-selector.js"></script>
    <script src="dist/angular/masonry/fizzy-ui-utils/utils.js"></script>
    <script src="dist/angular/masonry/get-size/get-size.js"></script>
    <script src="dist/angular/masonry/outlayer/item.js"></script>
    <script src="dist/angular/masonry/outlayer/outlayer.js"></script>
    <script src="dist/angular/masonry/masonry/masonry.js"></script>
    <script src="dist/angular/masonry/imagesloaded/imagesloaded.js"></script>
    <script src="dist/angular/masonry/angular-masonry/angular-masonry.js"></script>    

    <script src="../app/server.js"></script>
    <script src="app/app.js"></script>
    <script src="app/services/ActivitiesService.js"></script>
    <script src="app/services/CompanyService.js"></script>
    <script src="app/services/HelperService.js"></script>
    <script src="app/services/PersonService.js"></script>
    <script src="app/services/OfferService.js"></script>
    <script src="app/services/RedirService.js"></script>
    <script src="app/services/SessionService.js"></script>
    <script src="app/constants/commonsConstants.js"></script>
    <script src="dist/angular/angularjs-filters/filters.js"></script>


     <!--
    IE8 support, see AngularJS Internet Explorer Compatibility http://docs.angularjs.org/guide/ie
    For Firefox 3.6, you will also need to include jQuery and ECMAScript 5 shim
  -->
  <!--[if lt IE 9]>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.2.0/es5-shim.js"></script>
    <script>
      document.createElement('ui-select');
      document.createElement('ui-select-match');
      document.createElement('ui-select-choices');
    </script>
  <![endif]-->