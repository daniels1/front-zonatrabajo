<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
      <?
        include('header.php');
      ?>
    <script src="app/controllers/buscarOfertasController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="buscarOfertasController" data-ng-init="cargaInicial()">

      <? include("top_persona.php"); ?>
      <? include("menu_persona.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Buscar Ofertas            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="buscar_ofertas">Buscar Ofertas</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>

          <div class="row">
            <div class="col-md-3">
              
              <div class="box box-info">
                <div class="box-body">

                  <div class="row">
                    <div class="col-sm-12">
                      <strong><i class="fa fa-search margin-r-5"></i> Buscar</strong>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <input type="text" class="form-control" ng-model="offerTextFilter" id="offerTextFilter" name="offerTextFilter" placeholder="Buscar oferta">
                    </div>
                  </div>
                
                  <h5><strong> Paises:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="country in offerCountry"> 
                      <a ng-click="addFilter('country', country._id)"> 
                        {{(country._id == null) ? 'Otras' : country._id}} ({{country.count}})
                      </a>
                    </li>
                  </ul>

                  <h5><strong> Estados:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="state in offerStates"> 
                      <a ng-click="addFilter('state', state._id)"> 
                        {{(state._id == null) ? 'Otras' : state._id}} ({{state.count}})
                      </a>
                    </li>
                  </ul>

                  <h5><strong> Ciudades:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="city in offerCities"> 
                      <a ng-click="addFilter('city', city._id)"> 
                        {{(city._id == null) ? 'Otras' : city._id}} ({{city.count}})
                      </a>
                    </li>
                  </ul>
                  
                  <!-- By Type-->
                  <h5><strong> Area:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="area in offerAreas"> 
                      <a ng-click="addFilter('area', area._id)"> 
                        {{(area._id == null) ? 'Otras' : area._id}} ({{area.count}})
                      </a>
                    </li>
                  </ul>
                  
                  <!-- By Type-->
                  <h5><strong> Nivel Educativo:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="level in offerLevels"> 
                      <a ng-click="addFilter('educationLevel', level._id)"> 
                        {{(level._id == null) ? 'Otras' : level._id}} ({{level.count}})
                      </a>
                    </li>
                  </ul>
                  
                  <!-- By Type-->
                  <h5><strong> Tipo de Contrato:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="contract in offerContratcs"> 
                      <a ng-click="addFilter('typeContract', contract._id)"> 
                        {{(contract._id == null) ? 'Otras' : contract._id}} ({{contract.count}})
                      </a>
                    </li>
                  </ul>
                  
                  <!-- By Type-->
                  <h5><strong> Tipo de Moneda:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="cur in offerCurrencies"> 
                      <a ng-click="addFilter('salaryCurrency', cur._id)"> 
                        {{(cur._id == null) ? 'Otras' : cur._id}} ({{cur.count}})
                      </a>
                    </li>
                  </ul>
                  
                  <!-- By Type-->
                  <h5><strong> Tipo de Horario:</strong></h5>
                  <ul class="list-unstyled cate result">
                    <li ng-repeat="sch in offerSchedules"> 
                      <a ng-click="addFilter('typeSchedule', sch._id)"> 
                        {{(sch._id == null) ? 'Otras' : sch._id}} ({{sch.count}})
                      </a>
                    </li>
                  </ul>

                </div><!-- /.box-body -->
                
              </div>


            </div><!-- /.col -->

            <div class="col-md-9">
                <div class="box box-info">
                  
                  <div class="box-header" ng-show="showFilters">
                    <h4 class="box-title">Filtros: </h4>
                    <label ng-repeat="(key, val) in offerFilter" class="label-filter label-primary">
                        {{val | join : ", "}}
                        <div class="pull-right">
                          <a class="remove" ng-click="removeFilter(key)"><i class="fa fa-times"></i></a>
                        </div>
                    </label>
                  </div><!-- /.box-header -->
                  <div id="personas" class="box-body">

                    <div class="progress progress-sm active" ng-show="search">
                      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only">Buscando</span>
                      </div>
                    </div>

                    <div masonry>

                        <div class="masonry-brick" ng-repeat="oferta in ofertas | filter: offerFilter | filter: offerTextFilter" style="max-width: 320px;">

                              <div class="box box-widget" ng-click="viewDetail(oferta, 'offer', 'detalle_oferta')">
                                <div class="box-header with-border">
                                  <div class="user-block">

                                    <img ng-if="oferta.company.logo && oferta.company.logo != null" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{oferta.company.logo}}" class="img-responsive img-circle" alt="{{oferta.company.name}}">
                                    <img ng-if="!oferta.company.logo || oferta.company.logo == null " ng-src="dist/img/company.png" class="img-responsive img-circle" alt="{{oferta.company.name}}">

                                    <span class="username"><a href="#">{{oferta.title}}</a></span>
                                    <span class="description">{{oferta.company.name}}</span>
                                  </div><!-- /.user-block -->
                                  
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                  <!-- post text -->
                                  <p> {{oferta.description | camelcase | limitTo: 100}}</p>

                                  <button class="btn btn-primary btn-xs"><i class="fa fa-globe"></i> {{oferta.city}} {{oferta.state}} {{oferta.country}} </button>                                   
                                  <button class="btn btn-success btn-xs" ng-show="oferta.showSalary"><i class="fa fa-money"></i> {{oferta.salary | currency : '' : 2}} {{oferta.salaryCurrency}}</button> 
                                  <button class="btn btn-success btn-xs" ng-show="!oferta.showSalary"><i class="fa fa-money"></i> A Convenir</button> 

                                  <span class="pull-right text-muted"{{oferta.created_at.date | amDateFormat:"DD"}} <span>{{oferta.created_at.date | amDateFormat:"MMM"}}</span>
                                </div>
                              </div>

                        </div>


                    </div>
                  </div>
                </div>    
            </div>
            
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include("copy.php"); ?>
      <? include("control_bar_persona.php"); ?>

    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script>
      $(function(){
          $('#personas').slimScroll({
              height: '725px'
          });
      });
    </script>

  </body>
</html>
