      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-light">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-calendar"></i></a></li>
          <!-- <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li> -->
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Actividad Reciente</h3>

            <perfect-scrollbar class="scroller" wheel-propagation="true" wheel-speed="3" min-scrollbar-length="20">
              <ul class="control-sidebar-menu">
                <li ng-repeat="activity in activities">
                  <a href="#">
                    <i ng-show="activity.type == 'user'" class="menu-icon fa fa-user bg-teal"></i>
                    <i ng-show="activity.type == 'company'" class="menu-icon fa fa-building bg-teal"></i>
                    <i ng-show="activity.type == 'postulations'" class="menu-icon fa fa-briefcase bg-primary"></i>
                    <i ng-show="activity.type == 'offer'" class="menu-icon fa fa-file-text-o bg-success"></i>
                    <div class="menu-info">
                      <h4 class="control-sidebar-subheading">{{activity.description}}</h4>
                      <p>{{activity.created_at | amDateFormat:"DD/MM/YYYY"}}</p>
                    </div>
                  </a>
                </li>
              </ul><!-- /.control-sidebar-menu -->
            </perfect-scrollbar>
          </div><!-- /.tab-pane -->

        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>