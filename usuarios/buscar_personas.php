<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
      <?
        include('header.php');
      ?>
    <script src="app/controllers/buscarPersonasController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="buscarPersonasController" data-ng-init="cargaInicial()">

      <? include("top_empresa.php"); ?>
      <? include("menu_empresa.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Buscar Personas
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="buscar_personas">Buscar Personas</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>


          <div class="row" ng-show="company.status == 'pending' || company.status == 'no_verified'">
            <div class="col-md-12">
                  <div class="alert alert-info alert-dismissable" ng-show="company.status == 'pending'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      Su empresa esta en proceso de Verificación, una vez la información sea verificada procederemos a informarle y podrá usar todos nuestros servicios.
                  </div>

                  <div class="alert alert-warning alert-dismissable" ng-show="company.status == 'no_verified'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-exclamation-triangle"></i> Aviso</h4>
                      Su empresa <b>NO ESTA VERIFICADA</b>, dirijase a <a href="perfil_empresa"><b>Perfil de Empresa</b></a>, en la pestaña de Validación y adjunte los documentos solicitados. Mientrás no suministre los documentos requeridos no podrá publicar ofertas ni contactar candidatos.
                  </div>
            </div>
          </div>

          <div class="row" ng-show="company.status == 'verified'">
            <div class="col-md-3">
              
              <div class="box box-info">
                <div class="box-body">
                
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-id-card margin-r-5"></i> Última Actualización de CV</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12">
                        <ui-select ng-model="$parent.cv_update" on-select="addFilter('Ult. Actualización', $item.name)">
                          <ui-select-match class="ui-select-match" placeholder="Última Actualización">{{$select.selected.name}}</ui-select-match>
                          <ui-select-choices class="ui-select-choices" repeat="upd in cv_updates | filter: $select.search">
                            <div ng-bind-html="upd.name | highlight: $select.search"></div>
                          </ui-select-choices>
                        </ui-select>  
                      </div>
                    </div>
                
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-flag-o margin-r-5"></i> Nacionalidad</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12">
                        <ui-select ng-model="$parent.nationality" on-select="addFilter('nacionalidad', $item)">
                          <ui-select-match class="ui-select-match" placeholder="Nacionalidad">{{$select.selected}}</ui-select-match>
                          <ui-select-choices class="ui-select-choices" repeat="nat in nationalities | filter: $select.search">
                            <div ng-bind-html="nat | highlight: $select.search"></div>
                          </ui-select-choices>
                        </ui-select>  
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>
                      </div>
                    </div>
                    <div class="row">  
                        <div class="col-sm-12">
                          <ui-select ng-model="$parent.country" on-select="fnStates($select.selected); addFilter('país', $item.name)">
                            <ui-select-match class="ui-select-match" placeholder="País">{{$select.selected.name}}</ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="country.name as country in listCountries | filter: $select.search">
                            <div ng-bind-html="country.name | highlight: $select.search"></div>
                            </ui-select-choices>
                          </ui-select>  
                        </div>
                    </div>                        
                    <div class="row">                      
                        <div class="col-sm-12">
                          <ui-select ng-model="$parent.state" on-select="fnCities($select.selected); addFilter('estado', $item.name)" ng-disabled="listStates.length == 0">
                            <ui-select-match class="ui-select-match" placeholder="Estado">{{$select.selected.name}}</ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="est.name as est in listStates | filter: $select.search">
                              <div ng-bind-html="est.name | highlight: $select.search"></div>
                            </ui-select-choices>
                          </ui-select> 
                        </div>
                    </div>                        
                    <div class="row">                          
                        <div class="col-sm-12">
                          <ui-select ng-model="$parent.city" ng-disabled="listCities.length == 0" on-select="addFilter('ciudad', $item.city_name)">
                            <ui-select-match class="ui-select-match" placeholder="Ciudad">{{$select.selected.city_name}}</ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="city.city_name as city in listCities | filter: $select.search">
                              <div ng-bind-html="city.city_name | highlight: $select.search"></div>
                            </ui-select-choices>
                          </ui-select>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-briefcase margin-r-5"></i> Experiencia</strong>
                      </div>
                    </div>  
                    <div class="row">
                        <div class="col-sm-12">
                            <ui-select multiple tagging tagging-label="false" ng-model="$parent.companies" close-on-select="false" on-select="addFilter('empresas', $parent.companies)"  on-remove="removeFilterTerm('empresas', $item.description)">
                              <ui-select-match placeholder="Empresas" class="ui-select-match">{{$item}}</ui-select-match>
                              <ui-select-choices class="ui-select-choices" repeat="comp in listCompExperiences | filter: $select.search">
                                <div ng-bind-html="comp | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>  
                        </div>                 
                    </div>                        
                    <div class="row">  
                        <div class="col-sm-12">
                            <ui-select ng-model="$parent.sector" on-select="addFilter('sector', $item.description)">
                              <ui-select-match class="ui-select-match" placeholder="Sector">{{$select.selected.description}}</ui-select-match>
                              <ui-select-choices class="ui-select-choices" repeat="sector.description as sector in sectors | filter: $select.search" minimum-input-length="3">
                                <div ng-bind-html="sector.description | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>  
                        </div>                    
                    </div>                        
                    <div class="row">                          
                        <div class="col-sm-12">
                          <ui-select ng-model="$parent.area" on-select="addFilter('area', $item.description)">
                            <ui-select-match class="ui-select-match" placeholder="Area">{{$select.selected.description}}</ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="area.description as area in areas | filter: $select.search" minimum-input-length="3">
                              <div ng-bind-html="area.description | highlight: $select.search"></div>
                            </ui-select-choices>
                          </ui-select>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-graduation-cap margin-r-5"></i> Educación</strong>
                      </div>
                    </div>  
                    <div class="row">
                        <div class="col-sm-12">
                            <ui-select ng-model="$parent.institute" multiple on-select="addFilter('universidad', $parent.institute)" on-remove="removeFilterTerm('universidad', $item.nombre_institucion)">
                              <ui-select-match class="ui-select-match"  placeholder="Universidad">{{$item.nombre_institucion}}</ui-select-match>
                              <ui-select-choices class="ui-select-choices" repeat="inst.nombre_institucion as inst in listinstitutions | filter: $select.search" minimum-input-length="3">
                                <div ng-bind-html="inst.nombre_institucion | highlight: $select.search"></div>
                                <small> {{inst.siglas}}</small>
                              </ui-select-choices>
                            </ui-select>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-sm-12">
                            <ui-select ng-model="$parent.profesion" multiple on-select="addFilter('profesion', $parent.profesion)" on-remove="removeFilterTerm('profesion', $item.description)">
                              <ui-select-match class="ui-select-match" placeholder="Profesión">{{$item.description}}</ui-select-match>
                              <ui-select-choices class="ui-select-choices" repeat="prof.description as prof in professions | filter: $select.search" minimum-input-length="3">
                                <div ng-bind-html="prof.description | highlight: $select.search"></div>
                              </ui-select-choices>
                            </ui-select>  
                        </div>
                    </div>  
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-star margin-r-5"></i> Habilidades</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12">
                        <ui-select multiple ng-model="$parent.skills" close-on-select="false" on-select="addFilter('habilidades', $parent.skills)" on-remove="removeFilterTerm('habilidades', $item.description)">
                          <ui-select-match placeholder="Habilidades" class="ui-select-match">{{$item.description}}</ui-select-match>
                          <ui-select-choices class="ui-select-choices" repeat="skill.description as skill in listskills | filter: $select.search">
                            <div ng-bind-html="skill.description | highlight: $select.search"></div>
                          </ui-select-choices>
                        </ui-select>  
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-globe margin-r-5"></i> Idiomas</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12">
                        <ui-select multiple ng-model="$parent.langs" close-on-select="false" on-select="addFilter('idiomas', $parent.langs)" on-remove="removeFilterTerm('idiomas', $item.description)">
                          <ui-select-match placeholder="Idiomas" class="ui-select-match">{{$item.description}}</ui-select-match>
                          <ui-select-choices class="ui-select-choices" repeat="lang.description as lang in listlanguages | filter: $select.search">
                            <div ng-bind-html="lang.description | highlight: $select.search"></div>
                          </ui-select-choices>
                        </ui-select>  
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <strong><i class="fa fa-laptop margin-r-5"></i> Softwares</strong>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12">
                        <ui-select multiple ng-model="$parent.softwares" close-on-select="false" on-select="addFilter('softwares', $parent.softwares)" on-remove="removeFilterTerm('softwares', $item.description)">
                          <ui-select-match placeholder="Softwares" class="ui-select-match">{{$item.description}}</ui-select-match>
                          <ui-select-choices class="ui-select-choices" repeat="soft.description as soft in listsoftwares | filter: $select.search">
                            <div ng-bind-html="soft.description | highlight: $select.search"></div>
                          </ui-select-choices>
                        </ui-select>                                                          
                      </div>
                    </div>

                </div><!-- /.box-body -->
                
              </div>


            </div><!-- /.col -->

            <div class="col-md-9">
                <div class="box box-info">
                  
                  <div class="box-header" ng-show="showFilters">
                    <h4 class="box-title">Filtros: </h4>
                    <label ng-repeat="(key, val) in customFilter" class="label-filter label-primary">
                        {{key | camelcase}}: {{val | join : ", "}}
                        <div class="pull-right">
                          <a class="remove" ng-click="removeFilter(key)"><i class="fa fa-times"></i></a>
                        </div>
                    </label>
                  </div><!-- /.box-header -->
                  <div id="personas" class="box-body">

                    <div class="alert alert-warning alert-dismissable" ng-if="nofound && !search">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4>  <i class="icon fa fa-exclamation-triangle"></i> Aviso</h4>
                        No hay Personas con los parametros ingresados, intenta nuevamente.
                    </div>

                    <div class="progress progress-sm active" ng-show="search">
                      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only">Buscando</span>
                      </div>
                    </div>

                    <div masonry>

                        <div class="masonry-brick" ng-repeat="candidate in candidates" style="max-width: 320px;">

                              <div class="box-candidates box-widget widget-user">
                                
                                <div class="widget-user-header bg-aqua-active">
                                  <h4>{{candidate.first_name}} {{candidate.last_name}}</h4>
                                  <h5 class="truncate">{{candidate.title}}</h5>
                                </div>
                                <div class="widget-user-image">
                                  <img style="width:90px" data-ng-if="candidate.user.avatar != ''" class="img-circle" data-ng-src="https://api.zonatrabajo.com/profile/{{candidate.user.avatar}}" alt="User Avatar">
                                    <img style="width:90px" data-ng-if="candidate.user.avatar == ''" data-ng-src="dist/img/avatar.png" class="img-circle" alt="{{candidate.first_name}} {{candidate.last_name}}">
                                </div>
                                <div class="box-footer">
                                  
                                  <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                      <div class="description-block">
                                        <span class="description-text"><a class="btn btn-xs btn-info" ng-click="viewDetail(candidate, 'person', 'detalle_persona')"> <i class="fa fa-users"></i> Ver Perfil</a></span>
                                      </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                    <div class="col-sm-5 col-sm-offset-1">
                                      <div class="description-block">
                                        <span class="description-text"><a target="_blank" ng-disabled="!candidate.cv" ng-href="https://zonatrabajo.com/hojasdevida/{{candidate.cv}}" class="btn btn-xs btn-primary"> <i class="fa fa-file-pdf-o"></i> Ver CV</a></span>
                                      </div><!-- /.description-block -->
                                    </div><!-- /.col -->
                                  </div><!-- /.row -->
                                </div>
                              </div><!-- /.widget-user -->
                        </div>


                    </div>
                  </div>
                </div>    
            </div>
            
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include("copy.php"); ?>
      <? include("control_bar_empresa.php"); ?>

    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script>
      $(function(){
          $('#personas').slimScroll({
              height: '725px'
          });
      });
    </script>

  </body>
</html>
