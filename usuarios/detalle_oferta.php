<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/detalleOfertaController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="detalleOfertaController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" ng-cloak>
          <h1 class="text-center">
            {{offer.title}}
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="detalle_oferta">Detalle de Oferta</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>

          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="alert alert-success alert-dismissable" ng-show="postulateSuccess && !postulateDuplicate">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-check"></i> Muy Bien!</h4>
                    Te has inscrito en la Oferta como <b>{{offer.title}}</b> en <b>{{company.name}}</b>. <br> Ver mis <a href="postulaciones"><b>POSTULACIONES</b></a>
              </div>

              <div class="alert alert-warning alert-dismissable" ng-show="postulateDuplicate && !postulateSuccess">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-exclamation-triangle"></i> Aviso!</h4>
                    Ya te has postulado a esta oferta. <br> Ver mis <a href="postulaciones"><b>POSTULACIONES</b></a>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-5 col-md-offset-1">

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h4 class="box-title">Publicada el: <b>{{offer.created_at.date | amDateFormat:"DD/MM/YYYY"}}</b></h4>
                  <a ng-click="postulateOffer()" ng-show="!postulateDuplicate" class="btn btn-success pull-right"> <span class="glyphicon glyphicon-log-in"></span> <b>Postularme a esta Oferta</b></a>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="col-sm-12">
                    <strong><i class="fa fa-newspaper-o margin-r-5"></i> Descripción</strong>
                    <p class="text-muted">{{offer.description}}</p>
                  </div>
                  
                  <div class="col-sm-12">
                    <strong><i class="fa fa-sitemap margin-r-5"></i> Area de la Oferta</strong>
                      <p>{{offer.area}}</p>
                  </div>

                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>
                      <p>{{offer.city}} {{(offer.state) ? ' '+offer.state : ''}} {{(offer.country) ? ' '+offer.country : ''}}</p>
                  </div>

                  <div class="col-sm-6">
                    <strong><i class="fa fa-money margin-r-5"></i> Salario</strong>
                      <p class="text-muted" ng-show="offer.showSalary">{{offer.salary | currency : '' : 2}} {{offer.salaryCurrency}}</p>
                      <p class="text-muted" ng-show="!offer.showSalary">A Convenir</p>
                  </div>                                                          

                  <div class="col-sm-4 border-right">
                    <strong><i class="fa fa-clock-o margin-r-5"></i> Tipo de Jornada</strong>
                    <p class="text-muted">{{offer.typeSchedule}}</p>
                  </div>

                  <div class="col-sm-4 border-right">
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Tipo de Contrato</strong>
                    <p class="text-muted">{{offer.typeContract}}</p>
                  </div>

                  <div class="col-sm-4">
                    <strong><i class="fa fa-users margin-r-5"></i> Vacantes</strong>
                    <p class="text-muted">{{offer.jobsAvaiables}}</p>
                  </div>

                  <div class="col-sm-12">
                    <h4>Requisitos</h4>
                    <hr>
                  </div>  
                  
                  <div class="col-sm-6 border-right">
                    <strong><i class="fa fa-graduation-cap margin-r-5"></i> Educación:</strong>
                      <p>{{offer.educationLevel}}</p>
                  </div>

                  <div class="col-sm-6">
                    <strong><i class="fa fa-calendar margin-r-5"></i> Años de Experiencia:</strong>
                      <p>{{(offer.experience) ? offer.experience+' años' : 'Sin Experiencia'}}</p>
                  </div>
                 
                  <div class="col-sm-12" ng-show="offer.favCarrers">
                    <strong><i class="fa fa-graduation-cap margin-r-5"></i> Carreras Preferidas</strong> <br>
                    <small class="label label-requirements bg-green" ng-repeat="carrer in offer.carrers">{{carrer.description}}</small>
                  </div>
                 
                  <div class="col-sm-12" ng-show="offer.skills.length > 0">
                    <br><strong><i class="fa fa-star margin-r-5"></i> Habilidades</strong> <br>
                    <small class="label label-requirements bg-light-blue" ng-repeat="skill in offer.skills">{{skill.description}}</small>
                  </div>

                  <div class="col-sm-12" ng-show="offer.languages.length > 0">
                    <br><strong><i class="fa fa-globe margin-r-5"></i> Idiomas</strong> <br>
                    <small class="label label-requirements bg-blue" ng-repeat="lang in offer.languages">{{lang.description}}</small>
                  </div>                  

                  <div class="col-sm-12" ng-show="offer.softwares.length > 0">
                    <br><strong><i class="fa fa-laptop margin-r-5"></i> Software</strong> <br>
                    <small class="label label-requirements bg-purple" ng-repeat="soft in offer.softwares">{{soft.description}}</small>
                  </div>
                  <div class="col-sm-12 margin-top-10">
                    <h5 ng-show="offer.avaiableTravel"> Disponibilidad Para Viajar <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.avaiableChangeHome"> Disponibilidad de Cambio de Residencia <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.ownCar"> Vehículo Propio <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.driveLicense"> Posee Licencia de Conducir <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.usVisa"> Visa Americana <i class="fa fa-check"></i></h5>
                    <h5 ng-show="offer.eurVisa"> Visa Comunidad Europea <i class="fa fa-check"></i></h5>
                  </div>                 

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-5">
                <div class="box box-widget widget-user" ng-cloak>

                  <div class="pull-right no-padding">
                    <a class="btn btn-success btn-xs" ng-show="company.status == 'verified'" >
                          <span class="fa-stack fa-lg">
                            <i class="fa fa-certificate fa-stack-2x"></i>
                            <i class="fa fa-check fa-stack-1x text-success"></i>
                          </span>
                          <b>Verificada.</b> 
                    </a>
                    <a class="btn btn-info btn-xs" ng-show="company.status == 'pending'">
                          <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-exclamation fa-stack-1x text-info"></i>
                          </span>
                          <b>Verificando.</b> 
                    </a>
                    <a href="#validate" data-toggle="tab" class="btn btn-warning btn-xs" ng-show="company.status == 'no_verified'">
                          <span class="fa-stack fa-lg">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-exclamation-triangle fa-stack-1x text-warning"></i>
                          </span>
                          <b>No Verificada.</b> 
                    </a>
                  </div>
                
                  <div class="widget-user-header bg-light-blue">
                    <h3 class="widget-user-username">{{company.name}}</h3>
                    <h5 class="widget-user-desc">{{(company.slogan) ? company.slogan : '-----'}}</h5>
                  </div>
                  <div class="widget-user-image">
                    <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                    <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                  </div>
                  <div class="box-footer">
                    <div class="row">
                      <div class="col-sm-12 border-right">
                        <div class="description-block">
                          <h5 class="description-header">{{(company.description) ? '¿Quienes Somos?' : ''}}</h5>
                          <span class="description-text">{{company.description}} <p><b>{{company.sector}}</b></p></span>
                        </div><!-- /.description-block -->
                        <a ng-click="viewDetail(company, 'comp', 'detalle_empresa')" class="btn btn-success center-block"><i class="fa fa-building"></i> Perfil de Empresa </a>
                      </div><!-- /.col -->                    
                    </div><!-- /.row -->
                  </div>
              </div>

              <div class="box box-primary box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Ofertas Similares</h3>
                  <div class="box-tools pull-right">
                    <a href="buscar_ofertas" class="btn btn-default btn-xs"><i class="fa fa-search"></i> Buscar Ofertas</a>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="alert alert-warning alert-dismissable" ng-show="ofertas.length == 0">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    No hay ofertas Similares disponibles.
                  </div>

                  <ul class="products-list product-list-in-box">
                    <li class="item" data-ng-repeat="offer in ofertas" ng-click="viewDetail(offer, 'offer', 'detalle_oferta')">
                      <div class="product-img">
                        <img ng-if="offer.company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{offer.company.logo}}" class="img-circle" alt="{{offer.company.name}}">
                        <img ng-if="!offer.company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{offer.company.name}}">
                      </div>
                      <div class="product-info">
                        <a href="detalle_oferta" class="product-title">{{offer.title}} </a>
                        <div class="pull-right">
                          <span class="label label-info" ng-show="offer.showSalary"> {{offer.salary | currency : '' : 2}} {{offer.salaryCurrency}}</span>
                          <span class="label label-info" ng-show="!offer.showSalary"> A Convenir </span>
                        </div>
                        <span class="product-description">
                          {{offer.description | limitTo: 50}}
                        </span>
                        {{offer.company.name}} - {{offer.city}} 
                      </div>
                    </li><!-- /.item -->                    
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="buscar_ofertas" class="uppercase">Ver Todas las Ofertas</a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->


            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include("copy.php"); ?>
      <? include("control_bar_empresa.php"); ?>

    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

  </body>
</html>
