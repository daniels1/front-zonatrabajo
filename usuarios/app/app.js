'use strict';

var app = angular.module('zonaTrabajoApp', ['ngMask', 'ngSanitize', 'ui.select', 'angular-icheck', 'Alertify', 'ngFileUpload', 'perfect_scrollbar', 'ui.bootstrap', 'angularMoment', 'wu.masonry']);