'use strict';

app.service('Helper', function($http, $q) {
    

    this.countries = function(){

        return $http.get(ServerDestino + "countries/all")

    }

    this.states = function(){

        return $http.get(ServerDestino + "states/all")

    }

    this.statesByName = function(name) {
        var defered = $q.defer();

        $http.get(ServerDestino + "states/bycountryname/"+name)
            .success(function(data) {
                defered.resolve(data);
            })
            .error(function(err) {
                defered.reject(err)
            });

        return defered.promise;
    }

    this.cities = function(){

        return $http.get(ServerDestino + "cities/all")

    }

    this.citiesByName = function(name) {
        var defered = $q.defer();

        $http.get(ServerDestino + "cities/bystatename/"+name)
            .success(function(data) {
                defered.resolve(data);
            })
            .error(function(err) {
                defered.reject(err)
            });

        return defered.promise;
    }
    
    this.languages = function(){

        return $http.get(ServerDestino + "languages/all")

    }
    
    this.softwares = function(){

        return $http.get(ServerDestino + "softwares/all")

    }
    
    this.skills = function(){

        return $http.get(ServerDestino + "skills/all")

    }
    
    this.professions = function(){

        return $http.get(ServerDestino + "professions/all")

    }
    
    this.positions = function(){

        return $http.get(ServerDestino + "positions/all")

    }
    
    this.institutions = function(){

        return $http.get(ServerDestino + "institutions/all")

    }

    this.areas = function(){

        return $http.get(ServerDestino + "areapositions/all")

    }

    this.sectors = function(){

        return $http.get(ServerDestino + "sectorscompany/all")

    }

    this.citiesByState = function(id){

        return $http.get(ServerDestino + "cities/bystates/"+id)

    }

    this.statesBycountry = function(id){

        return $http.get(ServerDestino + "states/bycountry/"+id)

    }

    this.companiesExperiences = function(id){

        return $http.get(ServerDestino + "companiesExperiences/all")

    }

});