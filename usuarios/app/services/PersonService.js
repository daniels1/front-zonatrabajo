'use strict';

app.service('Person', function($http) {
    
    this.profile = function(id){

        return $http.get(ServerDestino + "persons/" +id + "/profiledata");

    }

    this.checkPostulations = function(offer, id){

        return $http.get(ServerDestino + "job/check/" + offer + "/" +id);

    }
    
    this.postulate = function(data){

        return $http.post(ServerDestino + "job/apply", data);

    }
    
    this.edit = function(id, data){

        return $http.put(ServerDestino + "persons/" +id, data)

    }
    
    this.search = function(data){

        return $http.post(ServerDestino + "persons/find", data)

    }
 
});