'use strict';

app.service('Redir', function() {
    
    this.detail = function(item, itemName, url){

        localStorage.setItem(itemName, JSON.stringify(item));
        window.location= url;

    }

    this.newItem = function(itemName, url){

        localStorage.removeItem(itemName);
        window.location= url;

    }



});