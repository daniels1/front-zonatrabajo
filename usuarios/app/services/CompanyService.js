'use strict';

app.service('Company', function($http) {
    
    this.companydata = function(id){

        return $http.get(ServerDestino + "companies/" + id + "/companydata");

    }
    
    this.offers = function(id){

        return $http.get(ServerDestino + "offers/bycompany/" + id);

    }
    
    this.listAll = function(){

        return $http.get(ServerDestino + "companies/listar/all/verified");

    }
    
    this.list = function(cant){

        return $http.get(ServerDestino + "companies/list/"+cant);

    }
    
    this.markCandidate = function(data){

        return $http.post(ServerDestino + "job/markCandidate", data);

    }
    
    this.edit = function(id, data){

        return $http.put(ServerDestino + "companies/" +id, data)

    }

});