'use strict';

app.service('Activities', function($http) {
    
    this.userActivities = function(id){

        return $http.get(ServerDestino + "activities/" +id);

    }

});