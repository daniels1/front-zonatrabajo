'use strict';

app.service('Session', function() {
    
    this.check = function(data, type_user){

        if (data !== null)
            return (data.type_user == type_user && data.token != null) ? true : false;
        else
            return false;

    }

    this.logout = function (url) {

        localStorage.removeItem("user_data");
        localStorage.removeItem("offer");
        localStorage.removeItem("company");
        window.location = url;
        
    }

});