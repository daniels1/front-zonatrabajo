app.value('commons', commons());

		function commons() {
		
			return {

				nationalities : ["Angoleño", "Argelino", "Camerunes", "Etíope", "Ecuatoguineano", "Egipcio", "Liberiano", "Libio", "Marroquí", "Namibio", "Nigeriano", "Saharaui", "Senegales", "Sudafricano", "Togoles", "Canadiense", "Estadounidense", "Mexicano", "Beliceño", "Costarricense", "Guatemalteco", "Hondureño", "Nicaragüense", "Panameño", "Salvadoreño", "Cubano", "Arubano", "Bahameño", "Barbadense", "Dominiqueso", "Dominicano", "Haitiano", "Jamaiquino", "Puertorriqueño", "Sancristobaleño", "Santaluciano", "Sanvicentino", "Argentino", "Boliviano", "Brasileño", "Chileno", "Colombiano", "Ecuatoriano", "Guyaneso", "Paraguayo", "Peruano", "Surinames", "Uruguayo", "Venezolano", "Afgano", "Azerbaiyano", "Bangladesí", "Bareiní", "Chino", "Emiratí", "Filipino", "Georgiano", "Hindú", "Indonesio", "Israelí", "Japones", "Libanes", "Mongol", "Norcoreano", "Sirio", "Surcoreano", "Vietnamita", "Albanes", "Aleman", "Andorrano", "Armenio", "Austríaco", "Belga", "Bielorruso", "Bosnio", "Búlgaro", "Checo", "Chiprioto", "Croata", "Danes", "Escoces", "Eslovaco", "Esloveno", "Español", "Estonio", "Finlandeso", "Frances", "Griego", "Holandes", "Húngaro", "Británico", "Irlandes", "Italiano", "Letono", "Lituano", "Luxemburgueso", "Maltes", "Moldavo", "Monegasco", "Montenegrino", "Noruego", "Polaco", "Portugues", "Rumano", "Ruso", "Serbio", "Sueco", "Suizo", "Turco", "Ucraniano", "Australiano", "Neozelandes"],

				months : ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],

				schedules : ["Diurno", "Nocturno", "Rotativo", "Libre"],

				contracts : ["Por Obra", "Contrato Fijo", "Tiempo Definido", "Honorarios Profesionales"],

				levels : ["Básico", "Secundaria", "Universitario", "Post-Grado", "Maestría", "Indiferente"],

				cv_updates : [{name: "Hoy", value: "today"}, {name: "Ayer", value: "yesterday"}, {name: "Últimos 5 días", value: "last5"}, {name: "Últimos 15 días", value: "last15"}, {name: "Último Mes", value: "last30"}, {name: "Últimos 3 Meses", value: "last90"}],

				currencies : [{name: 'Bolivares', siglas: 'Bs'}, {name: 'Doláres Americanos', siglas: 'USD'}, {name: 'Euros', siglas: 'EUR'}, {name: 'Pesos Colombianos', siglas: 'COP'}],

			}
		}