'use strict';

app.controller('perfilEmpresaController', ['$scope', '$http', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Person', 'Company', 'Activities', 'Offer', 'Helper', 
                                   function($scope, $http, Session, commons, Alertify, Upload, $timeout, Person, Company, Activities, Offer, Helper) {

    var pe = $scope;
    pe.user_data = JSON.parse(localStorage.getItem("user_data"));
    pe.listStates = [];
    pe.listCities = [];

    pe.cargaInicial = function(){

        if (!Session.check(pe.user_data, 'company')){
            Session.logout(); return;
        }

        Company.companydata(pe.user_data._id).then(function(response) {
            
            pe.company = response.data.companydata;
            pe.company.email = response.data.user.email;
            pe.company.member_since = new Date(response.data.user.member_since.date)

            Helper.statesByName(pe.company.country).then(function (response) {
             
                pe.listStates = response

            });

            Helper.citiesByName(pe.company.state).then(function (response) {
             
                pe.listCities = response

            });
            
        });

        Activities.userActivities(pe.user_data._id).then(function (response) {
             
            pe.activities = response.data;

        });

        Helper.countries().then(function (response) {
             
            pe.listCountries = response.data

        });

        Helper.sectors().then(function (response) {
             
            pe.sectors = response.data

        });    	

    }

    pe.saveCompanyData = function() {
      
      var objCompany = {name: pe.company.name,
                        logo: pe.company.logo,
                        description: pe.company.description,
                        sector: pe.company.sector,
                        social_name: pe.company.social_name,
                        nro_doc: pe.company.nro_doc,
                        address: pe.company.address,
                        city: pe.company.city,
                        state: pe.company.state,
                        country: pe.company.country,
                        website: pe.company.website,
                        email: pe.company.email,
                        phone: pe.company.phone,
                        facebook: pe.company.facebook,
                        twitter: pe.company.twitter,
                        instagram: pe.company.instagram,
                        linkedin: pe.company.linkedin,
                        contact: {
                        _id: pe.company.contact[0]._id,
                        contact_name: pe.company.contact[0].contact_name,
                        position: pe.company.contact[0].position,
                        email: pe.company.contact[0].email,
                        phone: pe.company.contact[0].phone,
                        ext: pe.company.contact[0].ext}};

      Company.edit(pe.user_data._id, objCompany)
           .success(function(data) {
                if (!data.success) { Alertify.success('Datos Actualizados Correctamente'); }
                else{ Alertify.error('Error al guardar la información'); }
            });

    }

    pe.fnStates = function(country){

        pe.company.state = '';
        pe.company.city = '';
        pe.listStates = [];

        Helper.statesBycountry(country._id).then(function (response) {
             
            pe.listStates = response.data

        });

    }

    pe.fnCities = function(estado){

        pe.company.city = '';
        pe.listCities = [];

        Helper.citiesByState(estado._id).then(function (response) {
             
            pe.listCities = response.data

        });

    }

    pe.logout = function(){
        Session.logout('usuarios/empresas')
    }

    pe.$on('$locationChangeStart', function(event) {

        if ($scope.frmPerfilPersona.$dirty) {
           Alertify.confirm("Desear Abandonar la página? Se perderan los datos no guardados")
                   .then(function onOk() {}, function onCancel() { 
                      event.preventDefault();
                    });
        }
    });

    pe.$watch('files', function () {
        pe.upload(pe.files);
    });
    
    pe.$watch('file', function () {
        pe.upload(pe.file);
    });

    pe.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
              var file = files[i];
              if (!file.$error) {
                Upload.upload({
                    url: ServerDestino + 'companies/'+pe.user_data._id+'/logo',
                    data: {
                      id: pe.user_data._id,
                      file: file
                    }
                }).then(function (resp) {

                    $timeout(function(){ 
                        pe.company.logo = resp.data.nameImage 
                        Alertify.success('Logo de la Empresa Actualizado!!!');
                    });

                }, null, function (evt) {})
              }
            }
        }
    };

    pe.$watch('docs', function () {
        pe.uploadRif(pe.docs);
    });
    
    pe.$watch('doc', function () {
        pe.uploadRif(pe.doc);
    });

    pe.uploadRif = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
              var file = files[i];
              if (!file.$error) {
                Upload.upload({
                    url: ServerDestino + 'companies/'+pe.user_data._id+'/docs',
                    data: {
                      id: pe.user_data._id,
                      file: file
                    }
                }).then(function (resp) {

                    $timeout(function(){ 
                        pe.company.status = resp.data.status
                        pe.validateStart = true;
                        Alertify.success('Documento de Registro adjuntado Correctamente!!!');
                    });

                }, null, function (evt) {})
              }
            }
        }
    };
    
}]);