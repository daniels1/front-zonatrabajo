'use strict';

app.controller('detallePersonaController', ['$scope', '$http', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Company', 'Activities', 
                                    function($scope, $http, Session, commons, Alertify, Upload, $timeout, Company, Activities) {

    var dp = $scope;
    dp.user_data = JSON.parse(localStorage.getItem("user_data"));
    dp.viewContact = false;

    dp.cargaInicial = function(){

        if (!Session.check(dp.user_data, 'company')){
            Session.logout(); return;
        }

        Company.companydata(dp.user_data._id).then(function(response) {
            
            dp.company = response.data.companydata;
            dp.company.email = response.data.user.email;
            dp.company.member_since = new Date(response.data.user.member_since.date)
            
        });

        Activities.userActivities(dp.user_data._id).then(function (response) {
             
            dp.activities = response.data;

        });        
             
        dp.person = (typeof JSON.parse(localStorage.getItem("person")).person == 'undefined') ? JSON.parse(localStorage.getItem("person")) : JSON.parse(localStorage.getItem("person")).person;
        dp.user = JSON.parse(localStorage.getItem("person")).user;

    }

    dp.viewContactData = function() {
        dp.viewContact = true;
    }

    dp.logout = function(){
        Session.logout('usuarios/empresas')
    }         
    
}]);