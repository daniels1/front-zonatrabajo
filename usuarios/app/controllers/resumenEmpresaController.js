'use strict';

app.controller('resumenEmpresaController', ['$scope', '$http', 'Session', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper',
                                    function($scope, $http, Session, Redir, Person, Company, Activities, Offer, Helper) {

    var re = $scope;
    re.user_data = JSON.parse(localStorage.getItem("user_data"));

    re.cargaInicial = function(){

        if (!Session.check(re.user_data, 'company')){
            Session.logout(); return;
        }

        Company.companydata(re.user_data._id).then(function(response) {
            
            re.company = response.data.companydata;
            re.percent = response.data.percent;
            re.company.email = response.data.user.email;
            re.company.member_since = new Date(response.data.user.member_since.date)
            
        });

        Activities.userActivities(re.user_data._id).then(function (response) {
             
            re.activities = response.data;

        });

        Company.offers(re.user_data._id).then(function(response) {
            
            re.ofertas = response.data.offers;

            re.candidates = [];
            _.each(re.ofertas, function(value, key) {
                _.each(value.candidates, function(val, k) {
                        re.candidates.push(val);
                    })
            })

            re.offerStatus = response.data.status;
                
        });

    }

    re.newOffer = function(itemName, url){

        Redir.newItem(itemName, url);

    }    

    re.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    re.logout = function(){
        Session.logout('usuarios/empresas')
    }
    
}]);