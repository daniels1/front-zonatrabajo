'use strict';

app.controller('candidatosController', ['$scope', '$http', 'Session', 'Redir', 'commons', 'Alertify', 'Upload', '$timeout', 'Company', 'Activities',
                                function($scope, $http, Session, Redir, commons, Alertify, Upload, $timeout, Company, Activities) {

    var cc = $scope;
    cc.user_data = JSON.parse(localStorage.getItem("user_data"));
    cc.viewContact = false;

    cc.cargaInicial = function(){

        if (!Session.check(cc.user_data, 'company')){
            Session.logout(); return;
        }

        Company.companydata(cc.user_data._id).then(function(response) {
            
            cc.company = response.data.companydata;
            cc.company.email = response.data.user.email;
            cc.company.member_since = new Date(response.data.user.member_since.date)
            
        });

        Company.offers(cc.user_data._id).then(function(response) {
            
            cc.ofertas = response.data.offers;

            cc.offerStatus = response.data.status;
                
        });

        Activities.userActivities(cc.user_data._id).then(function (response) {
             
            cc.activities = response.data;

        });

    }

    cc.markCandidate = function(offer, person, status) {

        var objCandidate = {offer_id: offer, person_id: person, status: status};

        Alertify.confirm('Desea marcar al candidato como '+status+' ?')
        .then(
            function onOk() {
                Company.markCandidate(objCandidate)
                       .success(function(data) {
                          if (!data.success) { 
                            Alertify.success('Actualizado Correctamente');                    
                            switch (status) {
                                case 'descartado':
                                    var i = _.findIndex(cc.ofertas, {_id:offer});
                                    var j = _.findIndex(cc.ofertas[i].candidates, {_id:person});
                                    cc.ofertas[i].candidates.splice(j, 1);
                                break;
                            }
                          }else{ 
                            Alertify.error('Error al guardar la información'); 
                          }
                      });
            }, 
            function onCancel() {
                return
            }
        );

    }

    cc.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    cc.logout = function(){
        Session.logout('usuarios/empresas')
    }         
    
}]);