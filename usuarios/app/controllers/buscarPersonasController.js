'use strict';

app.controller('buscarPersonasController', ['$scope', '$http', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper', 
                             function($scope, $http, Session, commons, Alertify, Upload, $timeout, Redir, Person, Company, Activities, Offer, Helper) {

    var bp = $scope;
    bp.user_data = JSON.parse(localStorage.getItem("user_data"));
    bp.nationalities = commons.nationalities;
    bp.cv_updates = commons.cv_updates;
    bp.listStates = [];
    bp.listCities = [];
    bp.candidates = {};
    bp.search = false;
    bp.nofound = false;
    bp.showFilters = false;
    bp.customFilter = {};

    bp.cargaInicial = function(){
        
        if (!Session.check(bp.user_data, 'company')){
            Session.logout(); return;
        }

        Company.companydata(bp.user_data._id).then(function(response) {
            
            bp.company = response.data.companydata;
            bp.company.email = response.data.user.email;
            bp.company.member_since = new Date(response.data.user.member_since.date)
            
        });

        Activities.userActivities(bp.user_data._id).then(function (response) {
           
            bp.activities = response.data;

        });

        Helper.sectors().then(function (response) {
             
            bp.sectors = response.data

        });             

        Helper.states().then(function (response) {
             
            bp.liststates = response.data
        });

        Helper.languages().then(function (response) {
             
            bp.listlanguages = response.data

        });

        Helper.softwares().then(function (response) {
             
            bp.listsoftwares = response.data

        });

        Helper.skills().then(function (response) {
             
            bp.listskills = response.data

        });  

        Helper.professions().then(function (response) {
             
            bp.professions = response.data

        });

        Helper.areas().then(function (response) {
             
            bp.areas = response.data

        });   

        Helper.countries().then(function (response) {
             
            bp.listCountries = response.data

        });   

        Helper.companiesExperiences().then(function (response) {
             
            bp.listCompExperiences = response.data;

        });  

        Helper.institutions().then(function (response) {
             
            bp.listinstitutions = response.data

        }); 

    }

    bp.buscarPersonas = function() {

        var objParams = {}
        bp.candidates = [];
        
        if (typeof bp.nationality != 'undefined' && bp.nationality != "")
            objParams['nationality'] = bp.nationality;

        if (typeof bp.country != 'undefined' && bp.country != "")
            objParams['country'] = bp.country;

        if (typeof bp.state != 'undefined' && bp.state != "")
            objParams['state'] = bp.state;

        if (typeof bp.city != 'undefined' && bp.city != "")
            objParams['city'] = bp.city;

        if (typeof bp.companies != 'undefined' && bp.companies != "")
            objParams['companies'] = bp.companies;

        if (typeof bp.sector != 'undefined' && bp.sector != "")
            objParams['sector'] = bp.sector;

        if (typeof bp.area != 'undefined' && bp.area != "")
            objParams['area'] = bp.area;

        if (typeof bp.institute != 'undefined' && bp.institute != "")
            objParams['institute'] = bp.institute;

        if (typeof bp.profesion != 'undefined' && bp.profesion != "")
            objParams['profesion'] = bp.profesion;

        if (typeof bp.skills != 'undefined' && bp.skills != "")
            objParams['skills'] = bp.skills;

        if (typeof bp.langs != 'undefined' && bp.langs != "")
            objParams['langs'] = bp.langs;

        if (typeof bp.softwares != 'undefined' && bp.softwares != "")
            objParams['softwares'] = bp.softwares;

        if (typeof bp.cv_update != 'undefined' && bp.cv_update != "")
            objParams['cv_update'] = bp.cv_update;

        if (!angular.equals({}, objParams)) {
            bp.search = true;
            bp.showFilters = true;
            Person.search(objParams).then(function (response) {

                bp.search = false;             
                if(response.data.length > 0){
                    bp.candidates = response.data
                    bp.nofound = false
                }else{
                     bp.nofound = true
                }

            });
        }    

    }

    bp.fnStates = function(country){

        bp.state = '';
        bp.city = '';
        bp.listStates = [];
        Helper.statesBycountry(country._id).then(function (response) {
             
            bp.listStates = response.data

        });

    }

    bp.fnCities = function(estado){

        bp.city = '';
        bp.listCities =[];
        Helper.citiesByState(estado._id).then(function (response) {
            
            bp.listCities = response.data

        });

    }

    bp.removeFilter = function(filter){

        switch (filter) {
            case 'nacionalidad': 
                bp.nationality = ''; break;
            case 'país': 
                bp.country = ''; break;
            case 'estado': 
                bp.state = ''; break;
            case 'ciudad': 
                bp.city = ''; break;
            case 'empresas': 
                bp.companies = ''; break;
            case 'sector': 
                bp.sector = ''; break;
            case 'area': 
                bp.area = ''; break;
            case 'universidad': 
                bp.universidad = ''; break;
            case 'profesion': 
                bp.profesion = ''; break;
            case 'habilidades': 
                bp.skills = ''; break;
            case 'idiomas': 
                bp.langs = ''; break;
            case 'softwares': 
                bp.softwares = ''; break;
        }

      delete bp.customFilter[filter];
      if (!angular.equals({}, bp.customFilter))
        bp.buscarPersonas();
      else{
            bp.showFilters = false;
            bp.candidates = {};
      }    
    }

    bp.removeFilterTerm = function(index, term){

        var i = bp.customFilter[index].indexOf(term);
        bp.customFilter[index].splice(i, 1);
        if(bp.customFilter[index].length == 0)
            bp.removeFilter(index)

    }

    bp.addFilter = function(index, term){
        
        bp.customFilter[index] = term;
        bp.buscarPersonas();
    }    

    bp.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    bp.logout = function(){
        Session.logout('usuarios/empresas')
    }
    
}]);