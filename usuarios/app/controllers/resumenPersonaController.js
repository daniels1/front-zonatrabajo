'use strict';

app.controller('resumenPersonaController', ['$scope', '$http', 'Session', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper', 
                                    function($scope, $http, Session, Redir, Person, Company, Activities, Offer, Helper) {

    var rp = $scope;
        rp.editPicture=false
    	rp.user_data = JSON.parse(localStorage.getItem("user_data"));

    rp.cargaInicial = function(){

        if (!Session.check(rp.user_data, 'person')){
            Session.logout(); return;
        }

        Person.profile(rp.user_data._id).then(function (response) {
             
            rp.person = response.data.person;
            rp.user = response.data.user;
            rp.percent = response.data.percent;
            rp.member_since = new Date(response.data.user.member_since.date);

        });

        Activities.userActivities(rp.user_data._id).then(function (response) {
        	 
            rp.activities = response.data;

        });

    	Offer.mostrecent().then(function (response) {
        	
        	rp.ofertas = response.data.offers;

        });

    	Company.list('8').then(function (response) {
        	 
        	rp.companies = response.data.companies;

        });

    }

    rp.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    rp.changePic = function(){
        rp.editPicture=true
    }

    rp.uploadPic = function(){
        rp.editPicture=false
    }    

    rp.logout = function(){
        Session.logout('usuarios/personas')
    }
    
}]);