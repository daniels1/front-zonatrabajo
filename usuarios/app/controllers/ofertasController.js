'use strict';

app.controller('ofertasController', ['$scope', '$http', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper', 
                             function($scope, $http, Session, commons, Alertify, Upload, $timeout, Redir, Person, Company, Activities, Offer, Helper) {

    var ofc = $scope;
    ofc.user_data = JSON.parse(localStorage.getItem("user_data"));
    ofc.schedules = commons.schedules;
    ofc.contracts = commons.contracts;
    ofc.currencies = commons.currencies;
    ofc.levels = commons.levels;
    ofc.listStates = [];
    ofc.listCities = [];

    ofc.cargaInicial = function(){
        
        ofc.offer = (JSON.parse(localStorage.getItem("offer")) != null) ? JSON.parse(localStorage.getItem("offer")) : {};
        
        if (typeof ofc.offer._id !== 'undefined') {

          Helper.statesByName(ofc.offer.country).then(function (response) {
           
              ofc.listStates = response

          });

          Helper.citiesByName(ofc.offer.state).then(function (response) {
           
              ofc.listCities = response

          });

        }

        if (!Session.check(ofc.user_data, 'company')){
            Session.logout(); return;
        }

        Company.companydata(ofc.user_data._id).then(function(response) {
            
            ofc.company = response.data.companydata;
            ofc.company.email = response.data.user.email;
            ofc.company.member_since = new Date(response.data.user.member_since.date)
            
        });

        Activities.userActivities(ofc.user_data._id).then(function (response) {
           
            ofc.activities = response.data;

        });

        Company.offers(ofc.user_data._id).then(function(response) {
            
            ofc.ofertas = response.data.offers;
                
        });

        Helper.states().then(function (response) {
             
            ofc.liststates = response.data
        });

        Helper.languages().then(function (response) {
             
            ofc.listlanguages = response.data

        });

        Helper.softwares().then(function (response) {
             
            ofc.listsoftwares = response.data

        });

        Helper.skills().then(function (response) {
             
            ofc.listskills = response.data

        });  

        Helper.professions().then(function (response) {
             
            ofc.professions = response.data

        });

        Helper.areas().then(function (response) {
             
            ofc.listAreas = response.data

        });   

        Helper.countries().then(function (response) {
             
            ofc.listCountries = response.data

        });  

    }

    ofc.setOfferType =  function(type) {
        ofc.offer.type = type
    }

    ofc.saveOfferData = function() {
      
      var objOffer = {company_id: ofc.company._id,
                      title: ofc.offer.title,
                      description: ofc.offer.description,
                      area: ofc.offer.area,
                      city: ofc.offer.city,
                      state: ofc.offer.state,
                      country: ofc.offer.country,
                      showSalary: ofc.offer.showSalary,
                      salary: ofc.offer.salary,
                      salaryCurrency: ofc.offer.salaryCurrency,
                      typeSchedule: ofc.offer.typeSchedule,
                      typeContract: ofc.offer.typeContract,
                      jobsAvaiables: ofc.offer.jobsAvaiables,
                      educationLevel: ofc.offer.educationLevel,
                      experience: ofc.offer.experience,
                      favCarrers: ofc.offer.favCarrers,
                      carrers: ofc.offer.carrers,
                      skills: ofc.offer.skills,
                      languages: ofc.offer.languages,
                      softwares: ofc.offer.softwares,
                      avaiableTravel: ofc.offer.avaiableTravel,
                      avaiableChangeHome: ofc.offer.avaiableChangeHome,
                      usVisa: ofc.offer.usVisa,
                      eurVisa: ofc.offer.eurVisa,
                      type: ofc.offer.type};

      if (ofc.offer._id) {
        Offer.edit(ofc.offer._id, objOffer)
             .success(function(data) {
                  if (!data.success) { Alertify.success('Datos Actualizados Correctamente'); }
                  else{ Alertify.error('Error al guardar la información'); }
              });
      }else{
        Offer.create(objOffer)
             .success(function(data) {
                  if (!data.success) { 
                    Alertify.success('Datos Guardados Correctamente'); 
                    ofc.offer._id = data.offer._id;
                    ofc.offer.status = data.offer.status;
                  }else{ 
                    Alertify.error('Error al guardar la información'); 
                  }
              });
      }       

    }

    ofc.fnStates = function(country){

        ofc.offer.state = '';
        ofc.offer.city = '';
        ofc.listStates = [];
        Helper.statesBycountry(country._id).then(function (response) {
             
            ofc.listStates = response.data

        });

    }

    ofc.fnCities = function(estado){

        ofc.offer.city = '';
        ofc.listCities =[];
        Helper.citiesByState(estado._id).then(function (response) {
            
            ofc.listCities = response.data

        });

    }

    ofc.checkOfferCarrers = function(){
      if (ofc.offer.favCarrers) {
        ofc.offer.carrers = '';
      }  
    }

    ofc.pauseOffer = function(offer){

      Offer.pause(offer._id)
           .success(function(data) {
                if (!data.success) { 
                    offer.status = 'pause';
                    Alertify.success('Oferta Pausada'); 
                }else{ 
                  Alertify.error('Error al actualizar la información'); 
                }
            });
    }

    ofc.playOffer = function(offer){

      Offer.active(offer._id)      
           .success(function(data) {
                if (!data.success) { 
                    offer.status = 'active';
                    Alertify.success('Oferta Activada'); 
                }else{ 
                  Alertify.error('Error al actualizar la información'); 
                }
            });        
    }

    ofc.deleteOffer = function(offer){

      Alertify.confirm("Desear eliminar esta oferta de empleo?")
                   .then(function onOk() {
                        Offer.remove(offer._id)                        
                             .success(function(data) {
                                  if (!data.success) { 
                                      var index = _.findIndex(ofc.ofertas, { _id: offer._id });
                                      ofc.ofertas.splice(index, 1);
                                  }
                              });
                   }, function onCancel() { 
                      return;
                   });
    }

    ofc.newOffer = function(itemName, url){

        Redir.newItem(itemName, url);

    }

    ofc.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    ofc.logout = function(){
        Session.logout('usuarios/empresas')
    }
    
}]);