'use strict';

app.controller('detalleEmpresaController', ['$scope', '$http', 'Session', 'Redir', 'commons', 'Alertify', 'Upload', '$timeout', 'Person', 'Company', 'Activities', 
                                    function($scope, $http, Session, Redir, commons, Alertify, Upload, $timeout, Person, Company, Activities) {

    var de = $scope;
    de.user_data = JSON.parse(localStorage.getItem("user_data"));
    de.company = JSON.parse(localStorage.getItem("company"));

    de.cargaInicial = function(){

        if (!Session.check(de.user_data, 'person')){
            Session.logout('usuarios/personas'); return;
        }

        Person.profile(de.user_data._id).then(function (response) {
             
            de.person = response.data.person;
            de.user = response.data.user;
            de.member_since = new Date(response.data.user.member_since.date);

        });

        Activities.userActivities(de.user_data._id).then(function (response) {
             
            de.activities = response.data;

        });

        Company.companydata(de.company._id).then(function(response) {
            
            de.company = response.data.companydata;
            de.company.email = response.data.user.email;
            de.company.member_since = new Date(response.data.user.member_since.date)
            
        });        

        Company.offers(de.company._id).then(function(response) {
            
            de.ofertas = response.data.offers;
            de.offerStatus = response.data.status;
                
        });        

    }

    de.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    de.logout = function(){
        Session.logout('usuarios/personas')
    }
    
}]);