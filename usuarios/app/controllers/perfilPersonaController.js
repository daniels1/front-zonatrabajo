'use strict';

app.controller('perfilPersonaController', ['$scope', '$http', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Person', 'Company', 'Activities', 'Offer', 'Helper',
                                   function($scope, $http, Session, commons, Alertify, Upload, $timeout, Person, Company, Activities, Offer, Helper) {

    var pp = $scope;
    pp.nationalities = commons.nationalities;
    pp.months = commons.months;
    pp.user_data = JSON.parse(localStorage.getItem("user_data"));
    pp.editStudy = false
    pp.editExperience = false

    pp.cargaInicial = function(){

        if (!Session.check(pp.user_data, 'person')){
            Session.logout(); return;
        }

        Person.profile(pp.user_data._id).then(function (response) {
             
            pp.person = response.data.person;
            pp.user = response.data.user;
            pp.member_since = new Date(response.data.user.member_since.date);

            Helper.statesByName(pp.person.country).then(function (response) {
             
                pp.listStates = response

            });

            Helper.citiesByName(pp.person.state).then(function (response) {
             
                pp.listCities = response

            });

        });

        Activities.userActivities(pp.user_data._id).then(function (response) {
             
            pp.activities = response.data;

        });

        Helper.languages().then(function (response) {
             
            pp.listlanguages = response.data

        });

        Helper.institutions().then(function (response) {
             
            pp.listinstitutions = response.data

        }); 

        Helper.countries().then(function (response) {
             
            pp.listCountries = response.data

        });

        Helper.softwares().then(function (response) {
             
            pp.listsoftwares = response.data

        });

        Helper.skills().then(function (response) {
             
            pp.listskills = response.data

        }); 

        Helper.professions().then(function (response) {
             
            pp.professions = response.data

        });   

        Helper.positions().then(function (response) {
             
            pp.positions = response.data

        });   

        Helper.sectors().then(function (response) {
             
            pp.sectors = response.data

        });     

    	Helper.areas().then(function (response) {
        	 
        	pp.areas = response.data

        });    	

    }

    pp.savePersonData = function() {

      var objPersona = {first_name: pp.person.first_name,
                        last_name: pp.person.last_name,
                        birthday: pp.person.birthday,
                        nationality: pp.person.nationality,
                        title: pp.person.title,
                        resume: pp.person.resume,
                        address: pp.person.address,
                        city: pp.person.city,
                        state: pp.person.state,
                        country: pp.person.country,
                        interest: pp.person.interest,
                        mobile: pp.person.mobile,
                        phone: pp.person.phone,
                        studies: pp.person.studies,
                        experiences: pp.person.experiences,
                        skills: pp.person.skills,
                        languages: pp.person.languages,
                        softwares: pp.person.softwares
                        };

      Person.edit(pp.user_data._id, objPersona)
            .success(function(data) {
                if (!data.success) { Alertify.success('Datos Actualizados Correctamente'); }
                else{ Alertify.error('Error al guardar la información'); }
            });

    }

    pp.fnHomeStates = function(country){

        Helper.statesBycountry(country._id).then(function (response) {
             
            pp.listStates = response.data

        });

    }

    pp.fnHomeCities = function(state){

        Helper.citiesByState(state._id).then(function (response) {
             
            pp.listCities = response.data

        });

    }

    pp.fnStudyStates = function(country){

        Helper.statesBycountry(country._id).then(function (response) {
             
            pp.listStates = response.data

        });

    }

    pp.fnStudyCities = function(state){

        Helper.citiesByState(state._id).then(function (response) {
             
            pp.listStudyCities = response.data

        });

    }

    pp.fnEditStudy = function(i) {
        pp.study = pp.person.studies[i];
        pp.editStudy = true;
    }

    pp.cancelEditStudy = function() {
        pp.study = {}
        pp.editStudy = false;
    }

    pp.addStudy = function () {
         pp.person.studies.push(pp.study);
         pp.study = {};
         $scope.frmStudy.$setPristine();
         pp.savePersonData();
    }

    pp.updateStudy = function (study) {
        var index = _.findIndex(pp.person.studies, { profesion: study.profesion.description });
        pp.person.studies[index] = study;
        pp.editStudy = false;
        $scope.frmStudy.$setPristine();
        pp.savePersonData();
    }

    pp.removeStudy = function (study) {

        Alertify.confirm('Desea Eliminar?')
        .then(
            function onOk() {
                var index = _.findIndex(pp.person.studies, { profesion: study.profesion.description });
                pp.person.studies.splice(index, 1);
                pp.savePersonData();
            }, 
            function onCancel() {
                return
            }
        );
    }

    pp.fnEditExperience = function(i) {
        pp.exp = pp.person.experiences[i];
        pp.editExperience = true;
    }

    pp.cancelEditExperience = function() {
        pp.exp = {}
        pp.editExperience = false;
    }

    pp.addExperience = function () {
         pp.person.experiences.push(pp.exp);
         pp.exp = {};
         pp.frmExperience.$setPristine();
         pp.savePersonData();
    }

    pp.updateExperience = function (exp) {
        var index = _.findIndex(pp.person.experiences, { position: exp.position });
        pp.person.experiences[index] = exp;
        pp.editExperience = false;
        $scope.frmExperience.$setPristine();
        pp.savePersonData();
    }

    pp.removeExperience = function (exp) {

        Alertify.confirm('Desea Eliminar?')
        .then(
            function onOk() {
                var index = _.findIndex(pp.person.experiences, { position: exp.position });
                pp.person.experiences.splice(index, 1);
                pp.savePersonData();
            }, 
            function onCancel() {
                return
            }
        );
    }

    pp.logout = function(){
        Session.logout('usuarios/personas')
    }

    pp.$on('$locationChangeStart', function(event) {

        if ($scope.frmPerfilPersona.$dirty) {
           Alertify.confirm("Desear Abandonar la página? Se perderan los datos no guardados")
                   .then(function onOk() {}, function onCancel() { 
                      event.preventDefault();
                    });
        }
    });
    
    pp.$watch('file', function () {
        pp.upload(pp.file);
    });

    pp.upload = function (file) {

          if (file){
            if (!file.$error) {
                Upload.upload({
                url: ServerDestino + 'users/avatar',
                data: {
                  id: pp.user._id,
                  file: file  
                }
                }).then(function (resp) {

                    $timeout(function(){ 
                        pp.user.avatar = resp.data.nameImage 
                        Alertify.success('Foto de Perfil Actualizada!!!');
                    });

                }, null, function (evt) {
                     var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                })
            }
          }
    };        
    
    pp.$watch('cv_file', function () {
        pp.uploadCV(pp.cv_file);
    });

    pp.uploadCV = function (file) {

          if (file){
            if (!file.$error) {
                Upload.upload({
                    url: ServerDestino + 'persons/cv',
                    data: {
                      id: pp.person._id,
                      file: file  
                    }
                }).then(function (resp) {

                    $timeout(function(){ 
                        pp.user.avatar = resp.data.nameImage 
                        Alertify.success('Hoja de Vida Actualizada Correctamente!!!');
                    });

                }, null, function (evt) {})
            }
          }
    };        
    
}]);