'use strict';

app.controller('buscarOfertasController', ['$scope', '$http', 'Session', 'commons', 'Alertify', 'Upload', '$timeout', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper', 
                             function($scope, $http, Session, commons, Alertify, Upload, $timeout, Redir, Person, Company, Activities, Offer, Helper) {

    var bo = $scope;
    bo.user_data = JSON.parse(localStorage.getItem("user_data"));
    bo.nationalities = commons.nationalities;
    bo.cv_updates = commons.cv_updates;
    bo.listStates = [];
    bo.listCities = [];
    bo.candidates = {};
    bo.search = true;
    bo.nofound = false;
    bo.showFilters = false;
    bo.offerFilter = {};

    bo.cargaInicial = function(){
        
        if (!Session.check(bo.user_data, 'person')){
            Session.logout(); return;
        }

        Person.profile(bo.user_data._id).then(function (response) {
             
            bo.person = response.data.person;
            bo.user = response.data.user;
            bo.member_since = new Date(response.data.user.member_since.date);

            Helper.statesByName(bo.person.country).then(function (response) {
             
                bo.listStates = response

            });

            Helper.citiesByName(bo.person.state).then(function (response) {
             
                bo.listCities = response

            });

        });

        Activities.userActivities(bo.user_data._id).then(function (response) {
             
            bo.activities = response.data;

        });

        Offer.listAll().then(function (response) {
          
          bo.ofertas = response.data.offers;
          bo.search = false;
          
        });

        Offer.filterBy("cities").then(function (response) {
          
          bo.offerCities = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("state").then(function (response) {
          
          bo.offerStates = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("country").then(function (response) {
          
          bo.offerCountry = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("education").then(function (response) {
          
          bo.offerLevels = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("contract").then(function (response) {
          
          bo.offerContratcs = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("schedule").then(function (response) {
          
          bo.offerSchedules = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("area").then(function (response) {
          
          bo.offerAreas = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

        Offer.filterBy("currency").then(function (response) {
          
          bo.offerCurrencies = ('result' in response.data.offer) ? response.data.offer.result : response.data.offer;

        });

    }

    bo.buscarPersonas = function() {

        var objParams = {}
        bo.candidates = [];
        
        if (typeof bo.nationality != 'undefined' && bo.nationality != "")
            objParams['nationality'] = bo.nationality;

        if (typeof bo.country != 'undefined' && bo.country != "")
            objParams['country'] = bo.country;

        if (typeof bo.state != 'undefined' && bo.state != "")
            objParams['state'] = bo.state;

        if (typeof bo.city != 'undefined' && bo.city != "")
            objParams['city'] = bo.city;

        if (typeof bo.companies != 'undefined' && bo.companies != "")
            objParams['companies'] = bo.companies;

        if (typeof bo.sector != 'undefined' && bo.sector != "")
            objParams['sector'] = bo.sector;

        if (typeof bo.area != 'undefined' && bo.area != "")
            objParams['area'] = bo.area;

        if (typeof bo.institute != 'undefined' && bo.institute != "")
            objParams['institute'] = bo.institute;

        if (typeof bo.profesion != 'undefined' && bo.profesion != "")
            objParams['profesion'] = bo.profesion;

        if (typeof bo.skills != 'undefined' && bo.skills != "")
            objParams['skills'] = bo.skills;

        if (typeof bo.langs != 'undefined' && bo.langs != "")
            objParams['langs'] = bo.langs;

        if (typeof bo.softwares != 'undefined' && bo.softwares != "")
            objParams['softwares'] = bo.softwares;

        if (typeof bo.cv_update != 'undefined' && bo.cv_update != "")
            objParams['cv_update'] = bo.cv_update;

        if (!angular.equals({}, objParams)) {
            bo.search = true;
            bo.showFilters = true;
            Person.search(objParams).then(function (response) {

                bo.search = false;             
                if(response.data.length > 0){
                    bo.candidates = response.data
                    bo.nofound = false
                }else{
                     bo.nofound = true
                }

            });
        }    

    }

    bo.fnStates = function(country){

        bo.state = '';
        bo.city = '';
        bo.listStates = [];
        Helper.statesBycountry(country._id).then(function (response) {
             
            bo.listStates = response.data

        });

    }

    bo.fnCities = function(estado){

        bo.city = '';
        bo.listCities =[];
        Helper.citiesByState(estado._id).then(function (response) {
            
            bo.listCities = response.data

        });

    }

    bo.removeFilter = function(filter){

        switch (filter) {
            case 'nacionalidad': 
                bo.nationality = ''; break;
            case 'país': 
                bo.country = ''; break;
            case 'estado': 
                bo.state = ''; break;
            case 'ciudad': 
                bo.city = ''; break;
            case 'empresas': 
                bo.companies = ''; break;
            case 'sector': 
                bo.sector = ''; break;
            case 'area': 
                bo.area = ''; break;
            case 'universidad': 
                bo.universidad = ''; break;
            case 'profesion': 
                bo.profesion = ''; break;
            case 'habilidades': 
                bo.skills = ''; break;
            case 'idiomas': 
                bo.langs = ''; break;
            case 'softwares': 
                bo.softwares = ''; break;
        }

      delete bo.offerFilter[filter];
      if (!angular.equals({}, bo.offerFilter))
        bo.buscarPersonas();
      else{
            bo.showFilters = false;
            bo.candidates = {};
      }    
    }

    bo.removeFilterTerm = function(index, term){

        var i = bo.offerFilter[index].indexOf(term);
        bo.offerFilter[index].splice(i, 1);
        if(bo.offerFilter[index].length == 0){
            bo.removeFilter(index)
            bo.showFilters = false;
        }

    }

    bo.addFilter = function(index, term){
        
        bo.offerFilter[index] = term;
        bo.showFilters = true;
    }    

    bo.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    bo.logout = function(){
        Session.logout('usuarios/empresas')
    }
    
}]);