'use strict';

app.controller('listEmpresasController', ['$scope', '$http', 'Session', 'Redir', 'Person', 'Company', 'Activities', function($scope, $http, Session, Redir, Person, Company, Activities) {

    var le = $scope;
    	le.user_data = JSON.parse(localStorage.getItem("user_data"));

    le.cargaInicial = function(){

        if (!Session.check(le.user_data, 'person')){
            Session.logout(); return;
        }

        Person.profile(le.user_data._id).then(function (response) {
             
            le.person = response.data.person;
            le.user = response.data.user;
            le.member_since = new Date(response.data.user.member_since.date);

        });

    	Activities.userActivities(le.user_data._id).then(function (response) {
        	 
            le.activities = response.data;

        });

    	Company.listAll().then(function (response) {
        	 
        	le.companies = response.data.data;

        });

    }

    le.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    } 

    le.logout = function(){
        Session.logout('usuarios/personas')
    }
    
}]);