'use strict';

app.controller('postulacionesController', ['$scope', '$http', 'Session', 'Redir', 'Person', 'Company', 'Activities', 'Offer', 'Helper', 
                                   function($scope, $http, Session, Redir, Person, Company, Activities, Offer, Helper) {

    var rp = $scope;
        rp.editPicture=false
    	rp.user_data = JSON.parse(localStorage.getItem("user_data"));

    rp.cargaInicial = function(){

        if (!Session.check(rp.user_data, 'person')){
            Session.logout(); return;
        }

        Person.profile(rp.user_data._id).then(function (response) {
             
            rp.person = response.data.person;
            rp.postulations = Object.keys(rp.person.postulations).map(function (key) {if (key != 'resumeStatus') {return  rp.person.postulations[key] } });

            rp.user = response.data.user;
            rp.member_since = new Date(response.data.user.member_since.date);

        });

        Activities.userActivities(rp.user_data._id).then(function (response) {
        	 
            rp.activities = response.data;

        });

    }

    rp.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }  

    rp.logout = function(){
        Session.logout('usuarios/personas')
    }
    
}]);