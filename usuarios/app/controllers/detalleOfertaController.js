'use strict';

app.controller('detalleOfertaController', ['$scope', '$http', 'Session', 'commons' , 'Alertify', 'Upload', '$timeout', 'Redir', 'Person', 'Company', 'Activities', 'Offer',
                                   function($scope, $http, Session, commons, Alertify, Upload, $timeout, Redir, Person, Company, Activities, Offer) {

    var dofc = $scope;
    dofc.user_data = JSON.parse(localStorage.getItem("user_data"));
    dofc.postulateSuccess = false;
    dofc.postulateDuplicate = false;

    dofc.cargaInicial = function(){
        
        dofc.offer = (JSON.parse(localStorage.getItem("offer")) != null) ? JSON.parse(localStorage.getItem("offer")) : {};

        if (!Session.check(dofc.user_data, 'person')){
            Session.logout('usuarios/personas'); return;
        }

        Company.companydata(dofc.offer.company_id).then(function(response) {
            
            dofc.company = response.data.companydata;
            dofc.company.email = response.data.user.email;
            dofc.company.member_since = new Date(response.data.user.member_since.date)
            
        });

        Activities.userActivities(dofc.user_data._id).then(function (response) {
             
            dofc.activities = response.data;

        });

        Person.profile(dofc.user_data._id).then(function (response) {
           
            dofc.person = response.data.person;
            dofc.user = response.data.user;
            dofc.member_since = new Date(response.data.user.member_since.date);

        });

        Offer.similars(dofc.offer.area, dofc.offer._id).then(function(response) {
            
            dofc.ofertas = response.data.offers;
                
        });

        Person.checkPostulations(dofc.offer._id, dofc.user_data._id).then(function(response) {
            
            dofc.postulateDuplicate = response.data.duplicate;
                
        });            

    }    

    dofc.viewDetail = function(item, itemName, url){

      Redir.detail(item, itemName, url);

    }

    dofc.postulateOffer = function() {
      
      var objPostulate = {offer_id: dofc.offer._id,
                          person_id: dofc.user_data._id};

        Person.postulate(objPostulate)
              .success(function(data) {
                  if (data.message == 'OK') { 
                    Alertify.success('Te has postulado correctamente'); 
                    dofc.postulateSuccess = true;
                    dofc.postulateDuplicate = false;
                  }else if (data.message == 'ERROR') { 
                    Alertify.log('Error al guardar la información'); 
                    dofc.postulateDuplicate = true;
                    dofc.postulateSuccess = false;
                  }
              });

    }    

    dofc.logout = function(){
        Session.logout('usuarios/personas')
    }
    
}]);