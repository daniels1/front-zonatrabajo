	$(function(){

	if (typeof(Storage) !== "undefined") {
	
		var user_data = JSON.parse(localStorage.getItem("user_data"));

		if (typeof(user_data) !== "undefined" && user_data !== null)
				window.location = '/usuarios/empresa';

	}

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

	$("#frmLogin").validate({
		rules:{
			email:{
				required:true,
				email:true
			},
			password:{
				required:true
			}
		},
		messages: {
			email: {
				required: "Ingrese su Correo Electr&oacute;nico",
				email: "Correo Electr&oacute;nico Inv&aacute;lido"
				},
			password: {
				required: "Ingrese su Contrase&ntilde;a"
				}
		}
	})

	$('#email, #password').keypress(function(e) {
		if (e.keyCode==13) {
			if($('#email').val()!=''&&$('#password').val()!=''){
				$("#btnLogIn").click()
			}
		};
	});


	$("#btnLogIn").click(function() {

		if($("#frmLogin").valid()){

			$(this).attr('value','Entrando...').attr('disabled', 'disabled');

			$.ajax({
				url: ServerDestino+'api/authenticate',
				type: 'POST',
				data: $("#frmLogin").serialize()+"&type_user=company",
				dataType:'json',
				success: function(resp){
					switch(resp.message){ 
						case 'logged': 
								var user_data = {_id: resp.company[0]._id, 
						                 		 name: resp.company[0].name, 
								                 token: resp.token,
								             	 type_user: resp.user.type_user}

								localStorage.setItem("user_data", JSON.stringify(user_data));
								window.location=resp.redirectTo; break;
						case 'bad_entry':  var redir = resp.redirectTo;
										   alertify.notify('Debe Acceder por el Lugar que le corresponde', 'error', 5, function(){
															window.location=redir;
										                   });
										  break;
						case 'wrong_status': alertify.notify(resp.MSGwrong_status, 'error'); break;
					}
					$("#btnLogIn").attr('value','Entrar').removeAttr('disabled');
				},
				error: function(error){
					switch (error.responseJSON.error) {
						case 'invalid_credentials':
							alertify.notify("Los datos ingresados son incorrectos", 'error'); break;
						break;						
					}
					$("#btnLogIn").attr('value','Entrar').removeAttr('disabled');
				}
			})

		}

	});

$.getJSON(ServerDestino+'/countries/all', function(result) {
    var country = $("#country");
    country.removeAttr('disabled');
    $.each(result, function(key, val) {
        country.append($("<option />").val(val.name).text(val.name));
    });
});

$("#country").on('change', function(event) {
	event.preventDefault();
	$("#stateLoading").show();
	$.getJSON(ServerDestino+'/states/bycountryname/'+$(this).val(), function(result) {
	    var state = $("#state");
	    state.removeAttr('disabled');
	    state.empty()
	    $("#stateLoading").hide();
	    $.each(result, function(key, val) {
	        state.append($("<option />").val(val.name).text(val.name));
	    });
	});
	
});

$("#state").on('change', function(event) {
	event.preventDefault();
	$("#citiesLoading").show();
	$.getJSON(ServerDestino+'/cities/bystatename/'+$(this).val(), function(result) {
	    var city = $("#city");
	    city.removeAttr('disabled');
	    city.empty()
		$("#citiesLoading").hide();
	    $.each(result, function(key, val) {
	        city.append($("<option />").val(val.city_name).text(val.city_name));
	    });
	});
	
});

$('#nro_doc').mask('00000000-0');
$('#phone, #contact_phone').mask('(0000) 000-0000');
$('#email').alphanumeric({allow:"@.-_"})
$('#company_name').alphanumeric({allow:".- "})
$('#contact_name').alpha({allow:" "});

$("#frmRegCompany").validate({
	rules:{
		company_name:{
			required:true
		},
		social_name:{
			required:true
		},
		nro_doc:{
			required:true
		},
		address:{
			required:true
		},
		phone:{
			required:true
		},
		contact_name:{
			required:true
		},
		position:{
			required:true
		},
		contact_email:{
			required:true,
			email:true
		},
		contact_phone:{
			required:true,
		},
		email:{
			required:true,
			email:true
		},
		password:{
			required:true,
			minlength: 6 
		},
		repassword:{
			required:true,
			equalTo: '#password'
		},
		terms:{
			required:true
		}
	},
	messages:{
		company_name:{
			required:"Ingrese el Nombre de la Empresa"
		},
		social_name:{
			required:"Ingrese la razón social de la Empresa"
		},
		nro_doc:{
			required:"Ingrese el nro. de Identificación"
		},
		address:{
			required:"Ingrese la dirección fiscal de la empresa"
		},
		phone:{
			required:"Ingrese el nro. de Teléfono"
		},
		contact_name:{
			required:"Ingrese el nombre de la persona de contacto"
		},
		position:{
			required:"Ingrese el cargo de la persona de contacto"
		},
		contact_email:{
			required: "Ingrese su Correo Electr&oacute;nico",
			email: "Correo Electr&oacute;nico Inv&aacute;lido"
		},
		contact_phone:{
			required:"Ingrese el nro. de Teléfono"
		},
		email:{
			required: "Ingrese su Correo Electr&oacute;nico",
			email: "Correo Electr&oacute;nico Inv&aacute;lido"
		},
		password:{
			required: "Ingrese su contrase&ntilde;a",
			minlength: "Su Contrase&ntilde;a debe tener al menos 6 caracteres"
		},
		repassword:{
			required: "Confirme su contrase&ntilde;a",
			equalTo: "La contrase&ntilde;as no coinciden"
		},
		terms:{
			required:" "
		}
	}
})

$("#btnRegCompany").click(function() {

	if($("#frmRegCompany").valid()){

		var notif = alertify.notify('Registrando, por favor espere...', 'info');

		$("#btnRegCompany").attr('value','Registrando').attr('disabled', 'disabled');

		$.ajax({
			url: ServerDestino+'users',
			type: 'POST',
			data: $("#frmRegCompany").serialize(),
			dataType:'json',
			success: function(resp){

				$("#btnRegCompany").attr('value','Registrar').removeAttr('disabled');
				notif.dismiss();

				switch(resp.message){

					case 'OK':

						var user_data = {_id: resp.company._id, 
				                 		 name: resp.company.name, 
						                 token: resp.token,
						             	 type_user: resp.user.type_user}

						localStorage.setItem("user_data", JSON.stringify(user_data));
						var redirectTo =resp.redirectTo;


						alertify.notify('Bienvenido a <strong>Zonatrabajo.com</strong>', 'success', 3, function(){  window.location = redirectTo });

					break;
					case 'USER_EXIST':
						alertify.notify('Ya se encuentra registrado en nuestro sistema', 'error', 3, function(){  window.location = myUrl+'usuarios/empresas' });

					break;
				}

			}
		})

	}

});

})