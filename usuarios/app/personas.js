$(function(){	

	if (typeof(Storage) !== "undefined") {
	
		var user_data = JSON.parse(localStorage.getItem("user_data"));

		if (typeof(user_data) !== "undefined" && user_data !== null)
				window.location = '/usuarios/persona';

	}

	var keyCodes = [61, 107, 173, 109, 187, 189];

	$(document).keydown(function(event) {   
	   if (event.ctrlKey==true && (keyCodes.indexOf(event.which) != -1)) {
	    event.preventDefault();
	    }
	});

	$(window).bind('mousewheel DOMMouseScroll', function (event) {
	   if (event.ctrlKey == true) {
	     event.preventDefault();
	   }
	});

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

	$("#frmLogin").validate({
		rules:{
			email:{
				required:true,
				email:true
			},
			password:{
				required:true
			}
		},
		messages: {
			email: {
				required: "Ingrese su Correo Electr&oacute;nico",
				email: "Correo Electr&oacute;nico Inv&aacute;lido"
				},
			password: {
				required: "Ingrese su Contrase&ntilde;a"
				}
		}
	})

	$('#email, #password').keypress(function(e) {
		if (e.keyCode==13) {
			if($('#email').val()!=''&&$('#password').val()!=''){
				$("#btnLogIn").click()
			}
		};
	});


	$("#btnLogIn").click(function() {

		if($("#frmLogin").valid()){

			$(this).attr('value','Entrando...').attr('disabled', 'disabled');

			$.ajax({
				url: ServerDestino+'api/authenticate',
				type: 'POST',
				data: $("#frmLogin").serialize()+"&type_user=person",
				dataType:'json',
				success: function(resp){
					switch(resp.message){ 
						case 'logged':

							if ('person' in resp) {

							    var user_data = {_id: resp.person[0]._id, 
								                 first_name: resp.person[0].first_name, 
								                 last_name: resp.person[0].last_name, 
								                 token: resp.token,
								             	 type_user: resp.user.type_user}

								localStorage.setItem("user_data", JSON.stringify(user_data));
							 	window.location=resp.redirectTo; 
							}else if('company' in resp){

								var user_data = {_id: resp.company[0]._id, 
						                 		 name: resp.company[0].name, 
								                 token: resp.token,
								             	 type_user: resp.user.type_user}

								localStorage.setItem("user_data", JSON.stringify(user_data));
								window.location=resp.redirectTo; break;

							} 	
						break;
						case 'bad_entry':  var redir = resp.redirectTo;
										   alertify.notify('Debe Acceder por el Lugar que le corresponde', 'error', 5, function(){
															if (typeof redir !== 'undefined') { window.location=redir; }															
									                   });
						break;
					}
					$("#btnLogIn").attr('value','Entrar').removeAttr('disabled');
				},
				error: function(error){
					switch (error.responseJSON.error) {
						case 'invalid_credentials':
							alertify.notify("Los datos ingresados son incorrectos", 'error'); break;
						break;						
					}
					$("#btnLogIn").attr('value','Entrar').removeAttr('disabled');
				}
			})

		}

	});

$('#email').alphanumeric({allow:"@.-_ "})
$('#first_name, #last_name').alpha();

$("#frmRegPerson").validate({
	rules:{
		first_name:{
			required:true
		},
		last_name:{
			required:true
		},
		phone:{
			required:true
		},
		email:{
			required:true,
			email:true,
			// remote: ServerDestino+'password/email'
		},
		password:{
			required:true,
			minlength: 6 
		},
		repass:{
			required:true,
			equalTo: '#password'
		},
		terms:{
			required:true
		}
	},
	messages:{
		first_name:{
			required:"Ingrese su Nombre"
		},
		last_name:{
			required:"Ingrese sus Apellidos"
		},
		phone:{
			required:"Ingrese su tel&eacute;fono"
		},
		email:{
			required: "Ingrese su Correo Electr&oacute;nico",
			email: "Correo Electr&oacute;nico Inv&aacute;lido",
		},
		password:{
			required: "Ingrese su contrase&ntilde;a",
			minlength: "Su Contrase&ntilde;a debe tener al menos 6 caracteres"
		},
		repass:{
			required: "Confirme su contrase&ntilde;a",
			equalTo: "La contrase&ntilde;as no coinciden"
		},
		terms:{
			required:"Error"
		}
	}
})

var profOpt = {
	url: ServerDestino+"/professions/all",

	getValue: "description",

	list: {
		match: {
			enabled: true
		}
	}
};

$("#profesion").easyAutocomplete(profOpt);

var countOpt = {
	url: ServerDestino+"/countries/all",

	getValue: "name",

	list: {
		match: {
			enabled: true
		}
	}
};

$("#country").easyAutocomplete(countOpt);

$('#phone').mask('(0000) 000-0000');

$("#btnRegPerson").click(function() {

	if($("#frmRegPerson").valid()){

		var notif = alertify.notify('Registrando, por favor espere...', 'info');

		$("#btnRegPerson").attr('value','Registrando').attr('disabled', 'disabled');

		$.ajax({
			url: ServerDestino+'users',
			type: 'POST',
			data: $("#frmRegPerson").serialize(),
			dataType:'json',
			success: function(resp){

				$("#btnRegPerson").attr('value','Registrar').removeAttr('disabled');
				notif.dismiss();

				switch(resp.message){

					case 'OK':

						var user_data = {_id: resp.person._id, 
						                 first_name: resp.person.first_name, 
						                 last_name: resp.person.last_name, 
						                 token: resp.token,
						             	 type_user: resp.user.type_user}

							localStorage.setItem("user_data", JSON.stringify(user_data));
							var redirectTo = resp.redirectTo;

						alertify.notify('Bienvenido a <strong>Zonatrabajo.com</strong>', 'success', 3, function(){  window.location = redirectTo });

					break;
					case 'USER_EXIST':
						alertify.notify('Ya se encuentra registrado en nuestro sistema', 'error', 3, function(){  window.location = myUrl+'usuarios/personas' });

					break;
				}

			}
		})

	}

});

})