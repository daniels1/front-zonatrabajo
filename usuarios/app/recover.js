$(function(){	

	if (typeof(Storage) !== "undefined") {
	
		var user_data = JSON.parse(localStorage.getItem("user_data"));

		if (typeof(user_data) !== "undefined" && user_data !== null)
				window.location = '/usuarios/persona';

	}

	var keyCodes = [61, 107, 173, 109, 187, 189];

	$(document).keydown(function(event) {   
	   if (event.ctrlKey==true && (keyCodes.indexOf(event.which) != -1)) {
	    event.preventDefault();
	    }
	});

	$(window).bind('mousewheel DOMMouseScroll', function (event) {
	   if (event.ctrlKey == true) {
	     event.preventDefault();
	   }
	});	

	$("#frmRecover").validate({
		rules:{
			email:{
				required:true,
				email:true
			}
		},
		messages: {
			email: {
				required: "Ingrese su Correo Electr&oacute;nico",
				email: "Correo Electr&oacute;nico Inv&aacute;lido"
			}
		}
	})

	$("#frmReset").validate({
		rules:{
			password:{
				required:true,
			},
			password_confirmation:{
				required:true,
				equalTo: "#password"
			}
		},
		messages: {
			password: {
				required: "Ingrese su nueva Contraseña"
			},
			password_confirmation:{
				required: "Ingrese nuevamente su nueva Contraseña",
				equalTo: "Las contraseñas no son iguales"
			}
		}
	})

	$("#btnRecover").click(function() {

		if($("#frmRecover").valid()){

			$(this).attr('value','Enviando...').attr('disabled', 'disabled');

			$.ajax({
				url: ServerDestino+'password/email',
				type: 'POST',
				data: {email: $('#email').val(), link: "https://zonatrabajo.com/usuarios/reset?email="+$('#email').val()},
				dataType:'json',
				success: function(resp){
					switch(resp.message){ 
						case 'OK':
							  var redir = resp.redirectTo;
								  alertify.notify('Se ha enviado un email para reestablecer contraseña en zonatrabajo.com', 'success', 5, function(){
													if (typeof redir !== 'undefined') { window.location=redir; }															
							                   	});
						break;
						case 'INVALID_USER':
							  var redir = resp.redirectTo;
								  alertify.notify('Debe Acceder por el Lugar que le corresponde', 'error', 5, function(){
													if (typeof redir !== 'undefined') { window.location=redir; }															
							                   	});
						break;
					}
					$("#btnRecover").attr('value','Recuperar').removeAttr('disabled');
				},
				error: function(error){
					switch (error.responseJSON.error) {
						case 'invalid_credentials':
							alertify.notify("Los datos ingresados son incorrectos", 'error'); break;
						break;						
					}
					$("#btnRecover").attr('value','Recuperar').removeAttr('disabled');
				}
			})

		}

	});

	$("#btnReset").click(function() {

		if($("#frmReset").valid()){

			$(this).attr('value','Enviando...').attr('disabled', 'disabled');

			$.ajax({
				url: ServerDestino+'password/reset',
				type: 'POST',
				data: $("#frmReset").serialize(),
				dataType:'json',
				success: function(resp){
					switch(resp.message){ 
						case 'OK':
							  var redir = resp.redirectTo;
								  alertify.notify('Se ha reestablecido su contraseña de acceso a zonatrabajo.com', 'success', 5, function(){
													if (typeof redir !== 'undefined') { window.location=redir; }															
							                   	});
						break;
						case 'INVALID_USER':
							  var redir = resp.redirectTo;
								  alertify.notify('Usuario no registrado en zonatrabajo.com', 'error', 5, function(){
													if (typeof redir !== 'undefined') { window.location=redir; }															
							                   	});
						break;
					}
					$("#btnReset").attr('value','Recuperar').removeAttr('disabled');
				},
				error: function(error){
					if ('error' in error.responseJSON) {
						alertify.notify("Los datos ingresados son incorrectos", 'error');
					}
					$("#btnReset").attr('value','Recuperar').removeAttr('disabled');
				}
			})

		}

	});

$('#email').alphanumeric({allow:"@.-_ "})

})