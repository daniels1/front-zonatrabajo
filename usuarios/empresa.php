<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Empresas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/resumenEmpresaController.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="resumenEmpresaController" data-ng-init="cargaInicial()">

      <?
        include('top_empresa.php');
        include('menu_empresa.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Resumen            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="empresa"><i class="fa fa-dashboard"></i> Empresa</a></li>
            <li><a href="empresa">Resumen</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>
          <!-- Info boxes -->

            <div class="row"> 
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>Ver</h3>
                  <p>Mi Resumen</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-home-outline"></i>
                </div>
                <a href="empresa" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>Ver</h3>
                  <p>Mis Ofertas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-list-outline"></i>
                </div>
                <a href="ofertas" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-blue">
                <div class="inner">
                  <h3>Ver</h3>
                  <p>Candidatos</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-contact"></i>
                </div>
                <a href="candidatos" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-purple">
                <div class="inner">
                  <h3>Buscar</h3>
                  <p>Personas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-search"></i>
                </div>
                <a href="buscar_personas" class="small-box-footer">
                  Ver más <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div><!-- ./col -->
          </div>

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">

              <div class="row">
                <div class="col-md-6">

              <!-- Widget: user widget style 1 -->
              <div class="box box-primary box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="pull-right no-padding">
                  <a class="btn btn-info btn-xs" href="perfil_empresa"><i class="fa fa-pencil"></i> Editar Perfil</a>
                </div>                
                <div class="widget-user-header bg-light-blue">
                  <div class="widget-user-image">
                  <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                  <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username">{{company.name}} 
                        <i ng-show="company.status == 'verified'" class="fa fa-check text-info"></i>
                  </h3> 
                  <h5 class="widget-user-desc">{{(company.description) ? company.description : '-----'}}</h5>
                </div>
                <div class="box-footer no-padding" ng-show="ofertas.length > 0">
                  <ul class="nav nav-stacked">
                  <li><a href="#"><strong>Perfil completado al</strong>

                          <span ng-class="{'bg-green': percent >= 90, 'bg-yellow': percent >= 60 && percent < 90,  'bg-red': percent < 60}" class="pull-right badge ng-binding">{{percent | number : 2}}%</span>
                      
                      </a></li>
                    <li><a href="#"><strong>Estado de tus Ofertas</strong></a></li>
                    <li><a href="#">Activas <span class="pull-right badge bg-green">{{offerStatus.active}}</span></a></li>
                    <li><a href="#">Pausadas <span class="pull-right badge bg-yellow">{{offerStatus.pause}}</span></a></li>
                    <li><a href="#">Finalizadas <span class="pull-right badge bg-red">{{offerStatus.finish}}</span></a></li>
                  </ul>
                </div>
              </div><!-- /.widget-user -->

                </div><!-- /.col -->

                <div class="col-md-6">
                
                  <div class="alert alert-info alert-dismissable" ng-show="company.status == 'pending'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      Su empresa esta en proceso de Verificación, una vez la información sea verificada procederemos a informarle y podrá usar todos nuestros servicios.
                  </div>

                  <div class="alert alert-warning alert-dismissable" ng-show="company.status == 'no_verified'">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-exclamation-triangle"></i> Aviso</h4>
                      Su empresa <b>NO ESTA VERIFICADA</b>, dirijase a <a href="perfil_empresa"><b>Perfil de Empresa</b></a>, en la pestaña de Validación y adjunte los documentos solicitados. Mientrás no suministre los documentos requeridos no podrá publicar ofertas ni contactar candidatos.
                  </div>

                  <div class="box box-info box-solid" ng-show="company.status == 'verified' && ofertas.length > 0">
                    <div class="box-header with-border">
                      <h3 class="box-title">Candidatos Postulados</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div id="candidatos" class="box-body no-padding">
                      <ul class="users-list clearfix">
                        <li class="candidate" data-ng-repeat="candidate in candidates" ng-click="viewDetail(candidate, 'person', 'detalle_persona')" ng-if="candidate.postulation.status[candidate.postulation.status.length - 1].status != 'descartado'">
                          <img data-ng-if="candidate.user.avatar" class="img-circle" data-ng-src="https://api.zonatrabajo.com/profile/{{candidate.user.avatar}}" alt="{{person.first_name}} {{person.last_name}}">
                          <img data-ng-if="!candidate.user.avatar" data-ng-src="dist/img/avatar.png" class="img-circle" alt="{{candidate.person.first_name}} {{candidate.person.last_name}}">
                          <a class="users-list-name" href="#">{{candidate.person.first_name}} {{candidate.person.last_name}}</a>
                        </li>                        
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                      <a href="candidatos" class="uppercase">Ver Candidatos</a>
                    </div><!-- /.box-footer -->
                  </div>
                </div><!-- /.col -->


              </div><!-- /.row -->

            </div><!-- /.col -->

            <div class="col-md-4">

              <div class="alert alert-info alert-dismissable" ng-show="ofertas.length == 0 && company.status == 'verified'">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Aviso</h4>
                  En este momento no tienes ofertas cargadas en nuestro sistema. Crea una Oferta haciendo Click <a href="nueva_oferta"><b>AQUÍ</b></a>
              </div>              

              <!-- PRODUCT LIST -->
              <div class="box box-success box-solid" ng-show="ofertas.length > 0">
                <div class="box-header with-border">
                  <h3 class="box-title">Mis Ofertas</h3>
                  <div class="box-tools pull-right">
                    <a ng-disabled="company.status != 'verified'" ng-click="newOffer('offer', 'oferta')" class="btn btn-default btn-xs btn-box-tool"><i class="fa fa-plus"></i> Crear Nueva</a>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div id="ofertas" class="box-body no-padding">
                  <ul class="products-list product-list-in-box">
                    <li class="item" data-ng-repeat="offer in ofertas" ng-click="viewDetail(offer, 'offer', 'oferta')">
                      <div class="product-img">
                        <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                        <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                      </div>
                      <div class="product-info">
                        <a class="product-title">{{offer.title}} </a>
                        <div class="pull-right">
                          <span class="label label-info" ng-show="offer.showSalary"> {{offer.salary | currency : '' : 2}} {{offer.salaryCurrency}}</span>
                          <span class="label label-info" ng-show="!offer.showSalary"> A Convenir </span>
                          <p class="text-center" ng-class="{'text-success' : offer.status == 'active'}" ng-show="offer.status == 'active'"><b> Activa <i class="fa fa-check"></i></b></p>
                          <p class="text-center" ng-class="{'text-warning' : offer.status == 'pause'}" ng-show="offer.status == 'pause'"><b> Pausada <i class="fa fa-pause"></i></b></p>
                          <p class="text-center" ng-class="{'text-muted' : offer.status == 'finish'}" ng-show="offer.status == 'finish'"><b> Finalizada <i class="fa fa-ban"></i></b></p>
                        </div>
                        <span class="product-description">
                          {{offer.description | limitTo: 50}}
                        </span>
                        {{company.name}} - {{offer.city}} 
                      </div>
                    </li><!-- /.item -->
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="ofertas" class="uppercase">Ver Todas las Ofertas</a>
                </div><!-- /.box-footer -->
              </div><!-- /.box -->



            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include('copy.php');
         include('control_bar_empresa.php'); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <!-- ChartJS 1.0.1 -->
    <script src="plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script>
      $(function(){
          $('#candidatos, #ofertas').slimScroll({
              height: '280px'
          });
      });
    </script>

  </body>
</html>
