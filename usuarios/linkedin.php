<? include('footer.php'); ?>
<script src="app/functions.js"></script>
<script>
$(function(){

	if (typeof(Storage) !== "undefined") {
    	
		if (getUrlVars()["status"] == 'success') {

			var user_data = {_id: getUrlVars()["user_id"], 
			                 first_name: getUrlVars()["first_name"], 
			                 last_name: getUrlVars()["last_name"], 
			                 token: getUrlVars()["token"],
			             	 type_user:'person'}

			localStorage.setItem("user_data", JSON.stringify(user_data));

			window.location = '/usuarios/persona';

		}else if(getUrlVars()["status"] == 'error') {

			window.location = myUrl+'usuarios/personas';

		}

	}

})
</script>