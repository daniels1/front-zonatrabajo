      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel" ng-cloak>
            <div class="pull-left image">
              <img ng-if="user.avatar != ''" ng-src="https://api.zonatrabajo.com/profile/{{user.avatar}}" class="img-circle" alt="{{person.first_name}} {{person.last_name}}">
              <img ng-if="user.avatar == ''" ng-src="dist/img/avatar.png" class="img-circle" alt="{{person.first_name}} {{person.last_name}}">
            </div>
            <div class="pull-left info">
              <p>{{person.first_name}} {{person.last_name}}</p>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Menú</li>

            <li>
              <a href="persona">
                <i class="fa fa-home"></i> <span>Resumen</span>
              </a>
            </li>
            <li>
              <a href="postulaciones">
                <i class="fa fa-file-text"></i> <span>Postulaciones</span>
              </a>
            </li>
            <li>
              <a href="perfil_persona">
                <i class="fa fa-user"></i> <span>Currículum</span>
              </a>
            </li>
            <li>
              <a href="buscar_ofertas">
                <i class="fa fa-search"></i> <span>Ofertas</span>
              </a>
            </li>
            <li>
              <a data-ng-click="logout()">
                <i class="fa fa-sign-out"></i> <span>Salir</span>
              </a>
            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>