    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="../assets/js/alertify/alertify.js"></script>
    <link rel="stylesheet" href="../assets/css/alertify/alertify.css"/>
    <link rel="stylesheet" href="../assets/css/alertify/themes/bootstrap.css"/>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="plugins/iCheck/icheck.js"></script>
    <script src="plugins/easyAutocomplete/jquery.easy-autocomplete.min.js"></script>
    <script src="../assets/js/alphanumeric/jquery.alphanumeric.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../assets/js/jquery.mask.min.js"></script> 
    <script src="../app/server.js"></script>