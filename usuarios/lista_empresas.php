<!DOCTYPE html>
<html>
  <head>
    <title>Zonatrabajo.com | Personas</title>
    <? include('header.php'); ?>
    <script src="app/controllers/listEmpresasController.js"></script>

  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper" data-ng-app="zonaTrabajoApp" data-ng-controller="listEmpresasController" data-ng-init="cargaInicial()">

      <?
        include('top_persona.php');
        include('menu_persona.php');
      ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Empresas Registradas            
          </h1>      
          <ol class="breadcrumb">
            <li><a href="persona"><i class="fa fa-dashboard"></i> Persona</a></li>
            <li><a href="persona">Empresas Registradas</a></li>
          </ol>              
        </section>

        <!-- Main content -->
        <section class="content" ng-cloak>
          
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div class="box-tools-list-candidates">
                    <div class="form-group">
                      <div class="input-group-list-offers">
                        <input type="text" class="form-control" ng-model="companyFilter" placeholder="Buscar" ng-disabled="companies.length == 0">                        
                      </div>
                    </div>  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <div class="alert alert-info alert-dismissable" ng-show="companies.length == 0">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Aviso</h4>
                      En este momento no hay empresas registradas en nuestro sistema.</a>
                  </div>

                      <div masonry>
                        <div class="masonry-brick col-xs-3" ng-repeat="company in companies | filter: companyFilter">

                          <div class="box-candidates box-widget widget-user">
                            
                            <div class="widget-user-header bg-purple">
                              <h4>{{company.name}}</h4>
                              <h5>{{company.slogan}}</h5>
                            </div>
                            <div class="widget-user-image">
                                <img ng-if="company.logo" ng-src="https://api.zonatrabajo.com/profile/companies/logos/{{company.logo}}" class="img-circle" alt="{{company.name}}">
                                <img ng-if="!company.logo" ng-src="dist/img/company.png" class="img-circle" alt="{{company.name}}">
                            </div>
                            <div class="box-footer">
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="description-block-company">
                                    <h5 class="description-header">{{company.offers.length}}</h5>
                                    <span class="description-text">Ofertas</span>
                                  </div>
                                </div><!-- /.col -->                                    
                                <div class="col-sm-6">
                                  <div class="description-block">
                                    <span class="description-text"><a class="btn btn-xs btn-info" ng-click="viewDetail(company, 'company', 'detalle_empresa')""> <i class="fa fa-users"></i> Ver Perfil</a></span>
                                  </div><!-- /.description-block -->
                                </div><!-- /.col -->
                              </div><!-- /.row -->
                            </div>
                          </div><!-- /.widget-user -->
                        </div>                            
                      </div><!-- /.row -->  

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <? include('copy.php');
         include('control_bar_persona.php'); ?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>